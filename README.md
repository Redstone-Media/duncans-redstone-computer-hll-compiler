This is the location of the 'Duncan's Redstone Computer HLL Compiler' (or DRCHLL-C) repository! The source files of this project will be located here.

The purpose of this project is to be able to write programs in a proprietary language called 'AGuyWhoIsBored's Redstone Computer Instruction Set Specification', or ARCISS, and then be able to run those programs on the redstone computer(s) that I have built in Minecraft. 

* Redstone Computer v3.0 (Ultimate) Showcase: https://www.youtube.com/watch?v=V3ycmN0rSU8
* Redstone Computer v4.0 Showcase: https://www.youtube.com/watch?v=SPaI5BJxs5M
* Duncan's Redstone Computer HLL Compiler (DRCHLL-C) Showcase: https://www.youtube.com/watch?v=vKNb2Fblrxw
* DRCHLL-C Repository Showcase: https://www.youtube.com/watch?v=isQ-W871m2A

Here's a list of the features of the latest version(s) of the DRCHLL-C project!

* Compiles and supports ARCISS v1.0, a custom programming language designed for these redstone computers 
* Compiler can compile ARCISS-HLL (High Level Language) to ARCISS-3L (Low Level Language / Assembly)
* Compiler can compile ARCISS-3L to ARCISS Machine Code
  * Machine code can be run on redstone computer targets
* Compiler contains built-in syntax checker at compilation
* Compiler includes some optimizations to help optimize code that you have written (will improve in the future)
* A separate uploader program exists to be able to upload compiled programs (in ARCISS Machine Code) to redstone computer targets
* The Windows Edition has a GUI that makes it more user-friendly (WIP)

More features will be coming soon!

Planned features:
(More features may be coming per user requests!)

* Function support!
* Extension (library) support!
* More advanced optimizations
* More advanced syntax checker
* Support for custom language extensions
* Multiplatform support

**NOTE** This is still a package of programs that are very much in development! Which means there will be bugs and such! If you find a bug, please file a bug report in the Issues section on how to reproduce it! Thank you very much!

Downloads for the DRCHLL-C are provided in the Downloads repository! 
Documentation for the compiler will be provided in the Wiki section as time comes!
Updates will come as time comes!
Thank you all very much for the support you all have given me! It really means a lot :) 