﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DRCHLLC_Core_Compiler
{
    class machineCodeParser
    {
        /*
         * This class will contain all functions and information needed for parsing code from
         * ARCIS assembly to native machine code on specific targets.
         * 
         * Created 9/5/2016 by AGuyWhoIsBored
         * 
         */

        // machine code array
        public static List<string> machineCodeList = new List<string>();

        public static void generateMachineCode()
        {
            // WILL CHECK FOR FUNCTIONS / INSTRUCTIONS THAT HAVE NOT BEEN CREATED YET!
            // ramw / ramr - RAM write / RAM read
            // cacw / cacr - cache read / cache write

            logging.logDebug("Core generating for: " + Program.compileCoreTarget, logging.loggingSenderID.machineCodeParser);

            // this is where we will generate our machine code!
            // length of word on RC3.0 - 41 bits
            // length of word on RC4.0 - 84 bits

            #region RC3.0 machine code generation
            if (Program.compileTarget == "1")
            {
                #region RC3.0 IS Bit Layouts
                // instruction set bit layout for RC3.0 core 1 (facing north)
                // [0 0 0 0 0 ] [0 0 0 0 0 ] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0  0 0 0 0] [0 0] [0 0 0] 
                // [1 2 4 8 16] [1 2 4 8 16] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [16 8 4 2 1] [1 2] [1 2 4]
                // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]

                // instruction set bit layout for RC3.0 core 2 (facing north)
                // [0 0 0 0 0 ] [0 0 0 0 0 ] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0 0 0 0 0 ] [0 0] [0 0 0]
                // [1 2 4 8 16] [1 2 4 8 16] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [1 2 4 8 16] [1 2] [1 2 4]
                // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]

                // instruction set bit layout for RC3.0 core 3 (facing north)
                // [0  0 0 0 0] [0  0 0 0 0] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0 0 0 0 0 ] [0 0] [0 0 0]
                // [16 8 4 2 1] [16 8 4 2 1] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [1 2 4 8 16] [1 2] [1 2 4]
                // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]

                // instruction set bit layout for RC3.0 core 4 (facing north)
                // [0  0 0 0 0] [0  0 0 0 0] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0 0 0 0 0 ] [0 0] [0 0 0]
                // [16 8 4 2 1] [16 8 4 2 1] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [1 2 4 8 16] [1 2] [1 2 4]
                // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]
                #endregion

                #region All Language Opcodes
                //ARCISS COMPILER ALL PARSED GENERATED FUNCTIONS:

                //NOT IMPLEMENTED YET:
                //cacw
                //cacr(?)

                //TEMPS:
                //memw
                //memr

                //GLOBAL:
                //ramw
                //ramr
                //malloc
                //jalloc [not actual opcode; generated and then deleted by compiler]
                //test
                //exit
                //flush
                //output
                //input
                //jump
                //bsr
                //bsl
                //ad1
                //sb1
                //not
                //add
                //sub

                //RC4.0:
                //gpuencode
                //gpuclr
                //gpudraw
                //gpuerase
                //gpugu
                //updspd

                #endregion

                // var for incrementing line of code location if none provided (default)
                int fJumpDefault = 2; // on the first line of code, we jump to line 2
                bool generate = true; // if this is true, we will generated a line of code; if it's not (i.e. malloc for values returning 0), will not generate!

                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {

                    // init
                    #region Init vars
                    string currentInstructionWord = null;

                    // will break down machine code into different segments
                    string mImmediate = "00000000";
                    string mGFalse = "00000";
                    string mGTrue = "00000";
                    string mCond = "00000";
                    string mRamW = "0000";
                    string mRamR = "0000";
                    string mOpcode = "00000";
                    string mDualReadToggle = "00";
                    string mExtension = "000";
                    #endregion

                    // now find what function or thing we need to do
                    #region malloc opcode
                    if (compiler.parsedAssemblyList[i].Contains("malloc"))
                    {
                        // grab values
                        int memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("malloc") + 7));
                        int value = acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].LastIndexOf("$") + 1));
                        // update machine code
                        if (generate == true)
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(memAddr, true, 4);
                                mImmediate = functionLibrary.convertNumberToBinary(value, false, 8);
                                // only jump that we should see with a malloc
                                if (compiler.parsedAssemblyList[i].Contains("fjump"))
                                {
                                    // grab fjump location and convert it to binary
                                    mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("fjump")) + 5), true, 5);
                                }
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(memAddr, false, 4);
                                mImmediate = functionLibrary.convertNumberToBinary(value, true, 8);
                                // only jump that we should see with a malloc
                                if (compiler.parsedAssemblyList[i].Contains("fjump"))
                                {
                                    // grab fjump location and convert it to binary
                                    mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("fjump")) + 5), true, 5);
                                }
                            }
                        }
                    }
                    #endregion

                    #region exit opcode
                    if (compiler.parsedAssemblyList[i].Contains("exit"))
                    {
                        // generate machine code; this opcode is easy!
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") // cores 1, 3 & 4's instruction set bit layouts are identical
                        {
                            mOpcode = "01011";
                        }
                        else if (Program.compileCoreTarget == "2")
                        {
                            mOpcode = "11010";
                        }
                    }
                    #endregion

                    #region flush opcode
                    if (compiler.parsedAssemblyList[i].Contains("flush"))
                    {
                        // grab flush reg
                        int regToFlush = acquireMemoryAddress(compiler.parsedAssemblyList[i], 5);
                        // convert regtoflush from value in code to value required in ext.
                        regToFlush++;

                        // build instruction word

                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mOpcode = "00111";
                        }
                        else if (Program.compileCoreTarget == "2")
                        {
                            mOpcode = "11100";
                        }
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        {
                            mExtension = functionLibrary.convertNumberToBinary(regToFlush, true, 3);
                        }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mExtension = functionLibrary.convertNumberToBinary(regToFlush, false, 3);
                        }
                    }
                    #endregion

                    #region output opcode
                    // syntax one
                    if (compiler.parsedAssemblyList[i].Contains("output1"))
                    {
                        // grab var address
                        int memAddr = -1;
                        int extension = -1;
                        int immediate = -1;

                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("$")));
                        }
                        else
                        {
                            // grab second number, will need to run interesting function here
                            int IDIndexCounter = 1;
                        TopOfFunction:
                            try
                            {
                                Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(7, IDIndexCounter));
                                IDIndexCounter++;
                                goto TopOfFunction;
                            }
                            catch (FormatException)
                            {
                                // do nothing
                            }
                            int IDIndexCounter2 = 1;
                        TopOfFunction2:
                            try
                            {
                                Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(7 + IDIndexCounter, IDIndexCounter2));
                                IDIndexCounter2++;
                                goto TopOfFunction2;
                            }
                            catch (FormatException)
                            {
                                // do nothing
                            }
                            immediate = acquireMemoryAddress(compiler.parsedAssemblyList[i], 7 + IDIndexCounter + (IDIndexCounter2 - IDIndexCounter - 1)); // will need to test this; quick tests show this works
                        }
                        extension = acquireMemoryAddress(compiler.parsedAssemblyList[i], 7);
                        if (Program.compileCoreTarget == "1")
                        {
                            if (memAddr == -1) { mRamR = "0000"; }
                            else { mRamR = functionLibrary.convertNumberToBinary(memAddr, true, 4); }
                            mOpcode = "01001";

                            if (immediate != -1)
                            {
                                mImmediate = functionLibrary.convertNumberToBinary(immediate, false, 8);
                            }

                        }
                        else if (Program.compileCoreTarget == "2")
                        {
                            if (memAddr == -1) { mRamR = "0000"; }
                            else { mRamR = functionLibrary.convertNumberToBinary(memAddr, true, 4); }
                            mOpcode = "10010";

                            if (immediate != -1)
                            {
                                mImmediate = functionLibrary.convertNumberToBinary(immediate, false, 8);
                            }
                        }
                        else if (Program.compileCoreTarget == "3" | Program.compileCoreTarget == "4")
                        {
                            if (memAddr == -1) { mRamR = "0000"; }
                            else { mRamR = functionLibrary.convertNumberToBinary(memAddr, true, 4); }
                            mOpcode = "01001";

                            if (immediate != -1)
                            {
                                mImmediate = functionLibrary.convertNumberToBinary(immediate, true, 8);
                            }
                        }

                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        {
                            mExtension = functionLibrary.convertNumberToBinary(extension + 1, true, 3);
                        }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mExtension = functionLibrary.convertNumberToBinary(extension + 1, false, 3);
                        }
                    }

                    // syntax two
                    if (compiler.parsedAssemblyList[i].Contains("output2"))
                    {
                        mExtension = functionLibrary.convertNumberToBinary((acquireMemoryAddress(compiler.parsedAssemblyList[i], 7)) + 1, true, 3);
                        if (Program.compileCoreTarget == "1") { mOpcode = "01001"; }
                        else if (Program.compileCoreTarget == "2" | Program.compileCoreTarget == "3" | Program.compileCoreTarget == "4") { mOpcode = "10010"; }
                    }
                    #endregion

                    #region input opcode
                    if (compiler.parsedAssemblyList[i].Contains("input"))
                    {
                        mOpcode = "01010";
                        // by default data will be pushed onto 
                        // gather data
                        int inputSocket = acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("input")) + 5);
                        int memAddr = 0;
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("$")));
                        }
                        // modify input socket based on hardware on RC 3.0
                        if (inputSocket == 6) { inputSocket = 1; }
                        else if (inputSocket == 5) { inputSocket = 3; }
                        else if (inputSocket == 4) { inputSocket = 2; }


                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        {
                            mExtension = functionLibrary.convertNumberToBinary(inputSocket, true, 3);
                        }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mExtension = functionLibrary.convertNumberToBinary(inputSocket, false, 3);
                        }
                        if (memAddr != 0)
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(memAddr, true, 4);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(memAddr, false, 4);
                            }
                        }
                    }

                    #endregion

                    #region bsr opcode
                    if (compiler.parsedAssemblyList[i].Contains("bsr"))
                    {
                        // generate opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mOpcode = "00110";
                        }
                        else if (Program.compileCoreTarget == "2")
                        {
                            mOpcode = "01100";
                        }

                        // grab address to modify
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        {
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("$"))), true, 4);
                        }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("$"))), false, 4);
                        }
                    }
                    #endregion

                    #region bsl opcode
                    if (compiler.parsedAssemblyList[i].Contains("bsl"))
                    {
                        // generate opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mOpcode = "00101";
                        }
                        else if (Program.compileCoreTarget == "2")
                        {
                            mOpcode = "10100";
                        }

                        // grab address to modify
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        {
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("$"))), true, 4);
                        }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("$"))), false, 4);
                        }
                    }
                    #endregion

                    #region ad1 opcode
                    if (compiler.parsedAssemblyList[i].Contains("ad1"))
                    {
                        // ad1 instruction
                        // opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00001"; }
                        else if (Program.compileCoreTarget == "2") { mOpcode = "10000"; }
                        // mem addr
                        // we have this statement here because ad1 temp[x] temp[x] instructions can be generated
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4);
                            }
                        }
                        // immediate value
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        {
                            mImmediate = "00000001";
                        }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mImmediate = "10000000";
                        }
                    }
                    #endregion

                    #region sb1 opcode
                    if (compiler.parsedAssemblyList[i].Contains("sb1"))
                    {
                        // opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00010"; }
                        else if (Program.compileCoreTarget == "2") { mOpcode = "01000"; }
                        // mem addr
                        // we have this statement here because sb1 temp[x] temp[x] instructions can be generated
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4);
                            }
                        }
                        // immediate value
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        {
                            mImmediate = "00000001";
                        }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mImmediate = "10000000";
                        }
                    }
                    #endregion

                    #region not opcode
                    if (compiler.parsedAssemblyList[i].Contains("not"))
                    {
                        // opcode
                        mOpcode = "00100";
                        // mem addr
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        {
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4);
                        }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4);
                        }
                    }
                    #endregion

                    #region add opcode
                    if (compiler.parsedAssemblyList[i].Contains("add"))
                    {
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("add"), 4);
                        string block1 = null;
                        string block2 = null;
                        string block3 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block3 += currentChar;
                        while (readLoc != temp.Length)
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block3 += currentChar;
                        }

                        // opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00001"; }
                        if (Program.compileCoreTarget == "2") { mOpcode = "10000"; }

                        // actual add instruction
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        {
                            mDualReadToggle = "01";
                        }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mDualReadToggle = "10";
                        }

                        // because of the RC3.0 architecture, we'll need to detect if either block is an
                        // immediate int. If it is, then we have to set that as immediate value, and have
                        // the other value become mramr
                        // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                        // checker already checks that and takes care of that for us (although the error produced 
                        // is a bit misleading)
                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8);
                            }
                            // then set other block to mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4);
                            }
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8);
                            }
                            // then set other block to mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4);
                            }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4);
                            }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4);
                            }
                        }

                    }
                    #endregion

                    #region subtract opcode
                    // pretty much pasted from add opcode, too lazy to do otherwise
                    if (compiler.parsedAssemblyList[i].Contains("sub"))
                    {
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("sub"), 4);
                        string block1 = null;
                        string block2 = null;
                        string block3 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block3 += currentChar;
                        while (readLoc != temp.Length)
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block3 += currentChar;
                        }

                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00010"; }
                        if (Program.compileCoreTarget == "2") { mOpcode = "01000"; }

                        // actual sub instruction
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        {
                            mDualReadToggle = "01";
                        }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mDualReadToggle = "10";
                        }

                        // because of the RC3.0 architecture, we'll need to detect if either block is an
                        // immediate int. If it is, then we have to set that as immediate value, and have
                        // the other value become mramr
                        // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                        // checker already checks that and takes care of that for us (although the error produced 
                        // is a bit misleading)
                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8);
                            }
                            // then set other block to mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4);
                            }
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8);
                            }
                            // then set other block to mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4);
                            }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4);
                            }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4);
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4);
                            }
                        }
                    }
                    #endregion

                    #region test instruction
                    if (compiler.parsedAssemblyList[i].Contains("test"))
                    {
                        // grab what we're testing
                        // list of things we can test:
                        // SO - shift overflow
                        // SU - shift underflow
                        // <, >, ==
                        // user input 1, 2, 3, 4
                        if (compiler.parsedAssemblyList[i].Contains("SO"))
                        {
                            // test ID
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mCond = "10000";
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mCond = "00001";
                            }
                            // all we need to do! extremely easy! :D (hopefully)
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("SU"))
                        {
                            // test ID
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mCond = "01000";
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mCond = "00010";
                            }
                            // all we need to do! extremely easy! :D (hopefully)
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("=="))
                        {
                            // test ID
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mCond = "11000";
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mCond = "00011";
                            }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mOpcode = "00011";
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mOpcode = "11000";
                            }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mDualReadToggle = "01";
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mDualReadToggle = "10";
                            }
                            // load two numbers in
                            // will have to grab blocks
                            string temp = compiler.parsedAssemblyList[i];
                            if (temp.IndexOf("test") != 0)
                            {
                                // remove text before
                                temp = temp.Remove(0, compiler.parsedAssemblyList[i].IndexOf("test"));
                            }
                            temp = temp.Remove(temp.IndexOf("test"), 8);
                            string block1 = null;
                            string block2 = null;
                            int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block1 += currentChar;
                            }
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block2 += currentChar;
                            }
                            // because of the RC3.0 architecture, we'll need to detect if either block is an
                            // immediate int. If it is, then we have to set that as immediate value, and have
                            // the other value become mramr
                            // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                            // checker already checks that and takes care of that for us (although the error produced 
                            // is a bit misleading)
                            if (!block1.Contains("$"))
                            {
                                // block1 becomes immediate value
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8);
                                }
                                // then set other block to mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4);
                                }
                            }
                            else if (!block2.Contains("$"))
                            {
                                // block2 becomes immediate value
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8);
                                }
                                // then set other block to mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4);
                                }
                            }
                            else
                            {
                                // convert both blocks to mramr and mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4);
                                }
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4);
                                }
                            }
                        }
                        else if (compiler.parsedAssemblyList[i].Contains(">"))
                        {
                            // test ID
                            mCond = "00100";
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mOpcode = "00011";
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mOpcode = "11000";
                            }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mDualReadToggle = "01";
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mDualReadToggle = "10";
                            }
                            // because of the way the RC3.0 architecture works, we'll need to test both blocks
                            // and see if one of them is an immediate number - if it is, that block will be loaded
                            // into the immediate part of the instruction word (which on the RC3.0 is piped into cache b)
                            // we will also need to check if the immediate number is on the righthand side of our assembly code,
                            // because if it's not, we'll need to invert the sign (as we can't change where the immediate value
                            // will be piped into) to be able to get accurate results

                            // load two numbers in
                            // will have to grab blocks
                            // for temp string, will have to remove any test BEFORE test!
                            string temp = compiler.parsedAssemblyList[i];
                            if (temp.IndexOf("test") != 0)
                            {
                                // remove text before
                                temp = temp.Remove(0, compiler.parsedAssemblyList[i].IndexOf("test"));
                            }
                            temp = temp.Remove(temp.IndexOf("test"), 7);
                            string block1 = null;
                            string block2 = null;
                            int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block1 += currentChar;
                            }
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block2 += currentChar;
                            }

                            if (!block1.Contains("$"))
                            {
                                // block1 becomes immediate value
                                // because block1 is on the lefthand side of our operator, switch operator
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8);
                                }
                                compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace('>', '<');
                                // then set other block to mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4);
                                }
                            }
                            else if (!block2.Contains("$"))
                            {
                                // block2 becomes immediate value
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8);
                                }
                                // then set other block to mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4);
                                }
                            }
                            else
                            {
                                // convert both blocks to mramr and mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4);
                                }
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4);
                                }
                            }
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("<"))
                        {
                            // test ID
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mCond = "10100";
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mCond = "00101";
                            }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mOpcode = "00011";
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mOpcode = "11000";
                            }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mDualReadToggle = "01";
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mDualReadToggle = "10";
                            }
                            // because of the way the RC3.0 architecture works, we'll need to test both blocks
                            // and see if one of them is an immediate number - if it is, that block will be loaded
                            // into the immediate part of the instruction word (which on the RC3.0 is piped into cache b)
                            // we will also need to check if the immediate number is on the righthand side of our assembly code,
                            // because if it's not, we'll need to invert the sign (as we can't change where the immediate value
                            // will be piped into) to be able to get accurate results

                            // load two numbers in
                            // will have to grab blocks
                            string temp = compiler.parsedAssemblyList[i];
                            if (temp.IndexOf("test") != 0)
                            {
                                // remove text before
                                temp = temp.Remove(0, compiler.parsedAssemblyList[i].IndexOf("test"));
                            }
                            temp = temp.Remove(temp.IndexOf("test"), 7);
                            string block1 = null;
                            string block2 = null;
                            int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block1 += currentChar;
                            }
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block2 += currentChar;
                            }

                            if (!block1.Contains("$"))
                            {
                                // block1 becomes immediate value
                                // because block1 is on the lefthand side of our operator, switch operator
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8);
                                }
                                compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace('<', '>');
                                // then set other block to mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4);
                                }
                            }
                            else if (!block2.Contains("$"))
                            {
                                // block2 becomes immediate value
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8);
                                }
                                // then set other block to mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4);
                                }
                            }
                            else
                            {
                                // convert both blocks to mramr and mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4);
                                }
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4);
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4);
                                }
                            }
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("ui"))
                        {
                            // collect which one it is
                            if (compiler.parsedAssemblyList[i].Contains("ui0"))
                            {
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mCond = "01100";
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mCond = "00110";
                                }
                                // easy! :D
                            }
                            else if (compiler.parsedAssemblyList[i].Contains("ui1"))
                            {
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mCond = "11100";
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mCond = "00111";
                                }
                                // easy! :D
                            }
                            else if (compiler.parsedAssemblyList[i].Contains("ui2"))
                            {
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mCond = "00010";
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mCond = "01000";
                                }
                                // easy! :D
                            }
                            else if (compiler.parsedAssemblyList[i].Contains("ui3"))
                            {
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                {
                                    mCond = "10010";
                                }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                {
                                    mCond = "01001";
                                }
                                // easy! :D
                            }
                        }
                    }
                    #endregion

                    #region ramw (ONLY) component
                    if (compiler.parsedAssemblyList[i].Contains("ramw") && !compiler.parsedAssemblyList[i].Contains("ramr"))
                    {
                        // will assume that [ramw $address$ temp] is only case of this, so we will keep it that way
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        {
                            mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4);
                        }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4);
                        }
                    }
                    #endregion

                    #region ramr + ramw component (for copying values)
                    // can be optimized
                    if (compiler.parsedAssemblyList[i].Contains("ramr") && compiler.parsedAssemblyList[i].Contains("ramw"))
                    {
                        // grab two memory addresses needed
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        {
                            mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4);
                        }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4);
                        }
                        int nextStartLoc = compiler.parsedAssemblyList[i].IndexOf("$");
                        nextStartLoc = compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1);
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        {
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1)), true, 4);
                        }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1)), false, 4);
                        }
                    }
                    #endregion

                    #region fjump component
                    if (compiler.parsedAssemblyList[i].Contains("fjump"))
                    {
                        // all four cores share identical mgfalse bit layout! 
                        mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("fjump")) + 5), true, 5);
                    }
                    else
                    {
                        if (mGFalse == "00000" && generate == true)
                        {
                            mGFalse = functionLibrary.convertNumberToBinary(fJumpDefault, true, 5);
                        }
                    }
                    #endregion

                    #region tjump component
                    if (compiler.parsedAssemblyList[i].Contains("tjump"))
                    {
                        // all four cores share identical mgtrue bit layout!
                        mGTrue = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("tjump")) + 5), true, 5);
                    }
                    #endregion

                    if (generate == true)
                    {
                        currentInstructionWord = mGFalse + " " + mGTrue + " " + mCond + " " + mImmediate + " " + mRamW + " " + mRamR + " " + mOpcode + " " + mDualReadToggle + " " + mExtension;
                        addToMachineCodeList(currentInstructionWord, i + 1);
                    }
                    fJumpDefault++;
                }
                // after machine code generation is complete, remove last line of machine code (as it's EOF and has all 0's written)
                machineCodeList.RemoveAt(machineCodeList.Count - 1);
                logging.logDebug("Machine code generation complete!", logging.loggingSenderID.machineCodeParser);
                logging.logDebug("Dumping generated machine code ...", logging.loggingSenderID.machineCodeParser);
                // remove all whitespaces in machine code first!
                for (int i = 0; i < machineCodeList.Count; i++)
                {
                    machineCodeList[i] = functionLibrary.RemoveWhitespace(machineCodeList[i]);
                }
                for (int i = 0; i < machineCodeList.Count; i++)
                {
                    logging.logDebug("[" + (i + 1).ToString() + "]: " + machineCodeList[i] + " | len " + machineCodeList[i].Length.ToString(), logging.loggingSenderID.machineCodeParser);
                }
            }
            #endregion
            #region RC4.0 machine code generation
            else if (Program.compileTarget == "2")
            {
                // instruction set bit layout for RC4.0 cores 1 - 4 (facing south)
                // [0  0  0  0  0  0  0 0 0 0 0 0 0 0 0 | 0 0 0 0 0 0 0 0 0  0  0  0  0  0  0] [0 0 0 0] [0 0 0 0  0  0] [0 0 0 0  0  0] [0 0 0 0] [0   0  0  0  0 0 0 0] [0 ] [0  0 0 0 0] [0  0 0 0 0] [0 0 0 0] [0] [0 0 0 0] [0 0 0 0] [0 0]
                // [15 14 13 12 11 10 9 8 7 6 5 4 3 2 1] [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15] [E 4 2 1] [1 2 4 8 16 32] [1 2 4 8 16 32] [1 2 4 8] [128 64 32 16 8 4 2 1] [E ] [16 8 4 2 1] [16 8 4 2 1] [1 2 4 8] [E] [1 2 4 8] [E 1 2 4] [2 1] (E = toggle bit, this bit will be used to toggle things)
                // [GPU GRANULAR X                     ] [GPU GRANULAR Y                     ] [CLOCKID] [GOTO TRUE    ] [GOTO FALSE   ] [TEST ID] [IMMEDIATE           ] [DR] [RAM READ  ] [RAM WRITE ] [OPCODE ] [M] [EXT.   ] [CACHE C] [ADR]
                // CLOCKID is ID of new clock speed if you want to change it; TEST ID is CONDITION; DR is dual-read toggle; M is mode toggle; CACHE C is cache control; ADR is cache address selector (0 based, so 0 is 1, 3 is 4)

                #region All Language Opcodes
                //ARCISS COMPILER ALL PARSED GENERATED FUNCTIONS:

                //NOT IMPLEMENTED YET:
                //cacw
                //cacr(?)

                //TEMPS:
                //memw
                //memr

                //GLOBAL:
                //ramw
                //ramr
                //malloc
                //jalloc [not actual opcode; generated and then deleted by compiler]
                //test
                //exit
                //flush
                //output
                //input
                //jump
                //bsr
                //bsl
                //ad1
                //sb1
                //not
                //add
                //sub

                //RC4.0:
                //gpuencode
                //gpuclr
                //gpudraw
                //gpuerase
                //gpugu
                //updspd

                #endregion

                // var for incrementing line of code location if none provided (default)
                int fJumpDefault = 1; // on the first line of code, we jump to line 2
                bool generate = true; // if this is true, we will generated a line of code; if it's not (i.e. malloc for values returning 0), will not generate!

                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {
                    fJumpDefault++;

                    // init
                    #region Init vars
                    string currentInstructionWord = null;

                    // will break down machine code into different segments
                    string mGPUGX = "000000000000000";
                    string mGPUGY = "000000000000000";
                    string mClockID = "0000";
                    string mMode = "0";
                    string mCacheC = "0000";
                    string mCacheAddr = "00";
                    string mGFalse = "000000";
                    string mGTrue = "000000";
                    string mCond = "0000";
                    string mImmediate = "00000000";
                    string mRamW = "00000";
                    string mRamR = "00000";
                    string mOpcode = "0000";
                    string mDualReadToggle = "0";
                    string mExtension = "0000";
                    #endregion

                    // now find what function or thing we need to do
                    #region malloc opcode
                    if (compiler.parsedAssemblyList[i].Contains("malloc"))
                    {
                        // grab values
                        int memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("malloc") + 7));
                        int value = acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].LastIndexOf("$") + 1));
                        // update machine code
                        if (generate == true)
                        {
                            mRamW = functionLibrary.convertNumberToBinary(memAddr, false, 5);
                            mImmediate = functionLibrary.convertNumberToBinary(value, false, 8);
                            // only jump that we should see with a malloc
                            if (compiler.parsedAssemblyList[i].Contains("fjump"))
                            {
                                // grab fjump location and convert it to binary
                                mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("fjump")) + 5), true, 6);
                            }
                        }
                    }
                    #endregion

                    #region exit opcode
                    if (compiler.parsedAssemblyList[i].Contains("exit"))
                    {
                        // this opcode is easy!
                        mOpcode = "1001";
                    }
                    #endregion

                    #region flush opcode
                    if (compiler.parsedAssemblyList[i].Contains("flush"))
                    {
                        // grab flush reg
                        int regToFlush = acquireMemoryAddress(compiler.parsedAssemblyList[i], 5);
                        // convert regtoflush from value in code to value required in ext.
                        regToFlush += 2;

                        // build instruction word
                        mOpcode = "0011";

                        mExtension = functionLibrary.convertNumberToBinary(regToFlush, true, 4);
                    }
                    #endregion

                    #region output opcode
                    // syntax one
                    if (compiler.parsedAssemblyList[i].Contains("output1"))
                    {
                        // set opcode
                        mOpcode = "0101";

                        // grab var address
                        int memAddr = -1;
                        int extension = -1;
                        int immediate = -1;

                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("$")));
                        }
                        else
                        {
                            // grab second number, will need to run interesting function here
                            int IDIndexCounter = 1;
                        TopOfFunction:
                            try
                            {
                                Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(7, IDIndexCounter));
                                IDIndexCounter++;
                                goto TopOfFunction;
                            }
                            catch (FormatException)
                            {
                                // do nothing
                            }
                            int IDIndexCounter2 = 1;
                        TopOfFunction2:
                            try
                            {
                                Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(7 + IDIndexCounter, IDIndexCounter2));
                                IDIndexCounter2++;
                                goto TopOfFunction2;
                            }
                            catch (FormatException)
                            {
                                // do nothing
                            }
                            immediate = acquireMemoryAddress(compiler.parsedAssemblyList[i], 7 + IDIndexCounter + (IDIndexCounter2 - IDIndexCounter - 1)); // will need to test this; quick tests show this works
                        }
                        extension = acquireMemoryAddress(compiler.parsedAssemblyList[i], 7);
                        if (memAddr == -1) { mRamR = "00000"; }
                        else { mRamR = functionLibrary.convertNumberToBinary(memAddr, false, 5); }

                        if (immediate != -1)
                        {
                            mImmediate = functionLibrary.convertNumberToBinary(immediate, false, 8);
                        }
                        mExtension = functionLibrary.convertNumberToBinary(extension + 1, true, 4);
                    }

                    // syntax two
                    if (compiler.parsedAssemblyList[i].Contains("output2"))
                    {
                        mOpcode = "0101";
                        mExtension = functionLibrary.convertNumberToBinary((acquireMemoryAddress(compiler.parsedAssemblyList[i], 7)) + 1, true, 4);
                    }
                    #endregion

                    #region input opcode
                    if (compiler.parsedAssemblyList[i].Contains("input"))
                    {
                        mOpcode = "1101";
                        // by default data will be pushed onto 
                        // gather data
                        int inputSocket = acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("input")) + 5);
                        int memAddr = 0;
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("$")));
                        }

                        // will have to update inputsocket for this RC's IS (instruction set)
                        //logging.logDebug("inputsocket is " + inputSocket.ToString());
                        if (inputSocket == 6) { inputSocket = 5; }
                        else if (inputSocket == 5) { inputSocket = 7; }
                        else if (inputSocket == 4) { inputSocket = 6; }
                        mExtension = functionLibrary.convertNumberToBinary(inputSocket, true, 4);
                        if (memAddr != 0)
                        {
                            mRamW = functionLibrary.convertNumberToBinary(memAddr, false, 5);
                        }
                    }

                    #endregion

                    #region bsr opcode
                    if (compiler.parsedAssemblyList[i].Contains("bsr"))
                    {
                        // generate opcode
                        mOpcode = "1010";
                        mDualReadToggle = "1";

                        // grab address to modify
                        // since this is bsr, and we have an architectual limit where we only can access bsr on cache b, change this to read cache b! (mramw)
                        mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("$"))), false, 5);
                    }
                    #endregion

                    #region bsl opcode
                    if (compiler.parsedAssemblyList[i].Contains("bsl"))
                    {
                        // generate opcode
                        mOpcode = "0010";

                        // grab address to modify
                        mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("$"))), false, 5);
                    }
                    #endregion

                    #region ad1 opcode
                    if (compiler.parsedAssemblyList[i].Contains("ad1"))
                    {
                        // ad1 instruction
                        // opcode
                        mOpcode = "1000";

                        // mem addr
                        // we have this statement here because ad1 temp[x] temp[x] instructions can be generated
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                        }
                        // immediate value
                        mImmediate = "00000001";
                    }
                    #endregion

                    #region sb1 opcode
                    if (compiler.parsedAssemblyList[i].Contains("sb1"))
                    {
                        // opcode
                        mOpcode = "0100";
                        // mem addr
                        // we have this statement here because sb1 temp[x] temp[x] instructions can be generated
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                        }
                        // immediate value
                        mImmediate = "00000001";
                    }
                    #endregion

                    #region not opcode
                    if (compiler.parsedAssemblyList[i].Contains("not"))
                    {
                        // opcode
                        mOpcode = "1100";
                        // mem addr
                        mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                    }
                    #endregion

                    #region add opcode
                    if (compiler.parsedAssemblyList[i].Contains("add"))
                    {
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("add"), 4);
                        string block1 = null;
                        string block2 = null;
                        string block3 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block3 += currentChar;
                        while (readLoc != temp.Length)
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block3 += currentChar;
                        }
                        // WILL ALSO NEED TO GENERATE MACHINE CODE FOR WRITEBACK IF APPROPRIATE! [DEPRECATED!]
                        // only case where it would not be appropriate if a test instruction testing SO or SU is next

                        // opcode
                        mOpcode = "1000";

                        // actual add instruction
                        mDualReadToggle = "1";

                        // because of the RC4.0 architecture, we'll need to detect if either block is an
                        // immediate int. If it is, then we have to set that as immediate value, and have
                        // the other value become mramr
                        // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                        // checker already checks that and takes care of that for us (although the error produced 
                        // is a bit misleading)
                        // also b/c of the RC4.0 architecture and the way it handles immediate values
                        // (piped into cache b instead of cache a like the RC3.0), we will have to change this slightly
                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);

                            // then set other block to mramr
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                            // then set other block to mramr
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                            mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                        }

                    }
                    #endregion

                    #region subtract opcode
                    // pretty much pasted from add opcode, too lazy to do otherwise
                    if (compiler.parsedAssemblyList[i].Contains("sub"))
                    {
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("sub"), 4);
                        string block1 = null;
                        string block2 = null;
                        string block3 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block3 += currentChar;
                        while (readLoc != temp.Length)
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block3 += currentChar;
                        }
                        // WILL ALSO NEED TO GENERATE MACHINE CODE FOR WRITEBACK IF APPROPRIATE! [DEPRECATED!]
                        // only case where it would not be appropriate if a test instruction testing SO or SU is next

                        // opcode
                        mOpcode = "0100";

                        // actual add instruction
                        mDualReadToggle = "1";

                        // because of the RC4.0 architecture, we'll need to detect if either block is an
                        // immediate int. If it is, then we have to set that as immediate value, and have
                        // the other value become mramr
                        // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                        // checker already checks that and takes care of that for us (although the error produced 
                        // is a bit misleading)
                        // also b/c of the RC4.0 architecture and the way it handles immediate values
                        // (piped into cache b instead of cache a like the RC3.0), we will have to change this slightly
                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);

                            // then set other block to mramr
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                            // then set other block to mramr
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                            mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                        }

                        //mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, (block2.IndexOf("$"))), false, 5);
                        //mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, (block1.IndexOf("$"))), false, 5);

                    }
                    #endregion

                    #region RC4.0 specific functions

                    #region gpu encode opcode
                    if (compiler.parsedAssemblyList[i].Contains("gpuencode"))
                    {
                        // opcode
                        mOpcode = "0110";
                        mDualReadToggle = "1";

                        // load two numbers in
                        // will have to do it this way b/c we can have either $x$ #, # $x$, or $x$ $x$
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("gpuencode"), 10);
                        string block1 = null;
                        string block2 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (readLoc != temp.Length)
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        // block1 will be ramr, block2 will be ramw
                        if (block1.Contains("$"))
                        {
                            // grab address
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                        }
                        else
                        {
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);
                        }
                        if (block2.Contains("$"))
                        {
                            mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                        }
                        else
                        {
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                        }
                    }
                    #endregion

                    #region gpu clear opcode
                    if (compiler.parsedAssemblyList[i].Contains("gpuclr"))
                    {
                        // easy!
                        mOpcode = "1000";
                        mMode = "1";
                    }
                    #endregion

                    #region gpu draw opcode
                    if (compiler.parsedAssemblyList[i].Contains("gpudraw"))
                    {
                        mOpcode = "0100";
                        mMode = "1";
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            // we know it's a var syntax
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                        }
                        else
                        {
                            // we know it's 2 immediate numbers syntax
                            // load two numbers in
                            string num1 = null;
                            string num2 = null;
                            num1 = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], 7), true, 4);
                            int nextStartLoc = compiler.parsedAssemblyList[i].IndexOf(" ");
                            nextStartLoc = compiler.parsedAssemblyList[i].IndexOf(" ", nextStartLoc + 1);
                            num2 = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], nextStartLoc), true, 4);
                            mImmediate = num2 + num1;
                        }
                    }
                    #endregion

                    #region gpu erase opcode
                    if (compiler.parsedAssemblyList[i].Contains("gpuerase"))
                    {
                        mOpcode = "1100";
                        mMode = "1";
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            // we know it's a var syntax
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                        }
                        else
                        {
                            // we know it's 2 immediate numbers syntax
                            // load two numbers in
                            string num1 = null;
                            string num2 = null;
                            num1 = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], 8), true, 4);
                            int nextStartLoc = compiler.parsedAssemblyList[i].IndexOf(" ");
                            nextStartLoc = compiler.parsedAssemblyList[i].IndexOf(" ", nextStartLoc + 1);
                            num2 = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], nextStartLoc), true, 4);
                            mImmediate = num2 + num1;
                        }
                    }
                    #endregion

                    #region gpu granular update opcode
                    if (compiler.parsedAssemblyList[i].Contains("gpugu"))
                    {
                        mOpcode = "0010";
                        mMode = "1";

                        // this will be interesting
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("gpugu"), 6);
                        string block1 = null;
                        string block2 = null;
                        string block3 = null;
                        string block4 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block3 += currentChar;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block3 += currentChar;
                        }
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block4 += currentChar;
                        while (readLoc != temp.Length)
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block4 += currentChar;
                        }
                        // hex stuff
                        block1 = block1.Replace("0x", "");
                        block2 = block2.Replace("0x", "");
                        int hexToInt = int.Parse(block1, System.Globalization.NumberStyles.HexNumber);
                        mGPUGX = functionLibrary.convertNumberToBinary(hexToInt, false, 15);
                        hexToInt = int.Parse(block2, System.Globalization.NumberStyles.HexNumber);
                        mGPUGY = functionLibrary.convertNumberToBinary(hexToInt, false, 15);
                        // bool stuff
                        string drawPart = "01";
                        string fillX = "0";
                        string fillY = "0";
                        if (block3.Contains("1")) { fillX = "1"; }
                        if (block4.Contains("1")) { fillY = "1"; }
                        mExtension = drawPart + fillX + fillY;

                    }
                    #endregion

                    #region update clock speed opcode
                    if (compiler.parsedAssemblyList[i].Contains("updspd"))
                    {
                        // don't even need an opcode!
                        // grab clock id
                        string clockID = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], 6), false, 3);
                        mClockID = "1" + clockID;
                    }
                    #endregion

                    #endregion

                    // WILL ALSO NEED TO ADD IN A=0 / B=0 TESTS!
                    #region test instruction
                    if (compiler.parsedAssemblyList[i].Contains("test"))
                    {
                        // grab what we're testing
                        // list of things we can test:
                        // SO - shift overflow
                        // SU - shift underflow
                        // <, >, ==
                        // user input 1, 2, 3, 4
                        if (compiler.parsedAssemblyList[i].Contains("SO"))
                        {
                            // test ID
                            mCond = "1000";
                            // all we need to do! extremely easy! :D (hopefully)
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("SU"))
                        {
                            // test ID
                            mCond = "0100";
                            // all we need to do! extremely easy! :D (hopefully)
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("=="))
                        {
                            // test ID
                            mCond = "1100";
                            mOpcode = "0001";
                            mDualReadToggle = "1";
                            // load two numbers in
                            // will have to grab blocks
                            string temp = compiler.parsedAssemblyList[i];
                            if (temp.IndexOf("test") != 0)
                            {
                                // remove text before
                                temp = temp.Remove(0, compiler.parsedAssemblyList[i].IndexOf("test"));
                            }
                            temp = temp.Remove(temp.IndexOf("test"), 8);
                            string block1 = null;
                            string block2 = null;
                            int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block1 += currentChar;
                            }
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block2 += currentChar;
                            }
                            // because of the RC4.0 architecture, we'll need to detect if either block is an
                            // immediate int. If it is, then we have to set that as immediate value, and have
                            // the other value become mramr
                            // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                            // checker already checks that and takes care of that for us (although the error produced 
                            // is a bit misleading)
                            // also b/c of the RC4.0 architecture and the way it handles immediate values
                            // (piped into cache a instead of cache b like the RC3.0), we will have to change this slightly
                            if (!block1.Contains("$"))
                            {
                                // block1 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);

                                // then set other block to mramr
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                            }
                            else if (!block2.Contains("$"))
                            {
                                // block2 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                                // then set other block to mramr
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                            }
                            else
                            {
                                // convert both blocks to mramr and mramw
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                            }

                        }
                        else if (compiler.parsedAssemblyList[i].Contains(">"))
                        {
                            // test ID
                            mCond = "0010";
                            mOpcode = "0001";
                            mDualReadToggle = "1";
                            // will perform same checks as ==, check that method for more info
                            // load two numbers in
                            // will have to grab blocks
                            string temp = compiler.parsedAssemblyList[i];
                            if (temp.IndexOf("test") != 0)
                            {
                                // remove text before
                                temp = temp.Remove(0, compiler.parsedAssemblyList[i].IndexOf("test"));
                            }
                            temp = temp.Remove(temp.IndexOf("test"), 7);
                            string block1 = null;
                            string block2 = null;
                            int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block1 += currentChar;
                            }
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block2 += currentChar;
                            }
                            // because of the RC4.0 architecture, we'll need to detect if either block is an
                            // immediate int. If it is, then we have to set that as immediate value, and have
                            // the other value become mramr
                            // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                            // checker already checks that and takes care of that for us (although the error produced 
                            // is a bit misleading)
                            // also b/c of the RC4.0 architecture and the way it handles immediate values
                            // (piped into cache a instead of cache b like the RC3.0), we will have to change this slightly
                            if (!block1.Contains("$"))
                            {
                                // block1 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);

                                // then set other block to mramr
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                            }
                            else if (!block2.Contains("$"))
                            {
                                // block2 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                                // then set other block to mramr
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                            }
                            else
                            {
                                // convert both blocks to mramr and mramw
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                            }
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("<"))
                        {
                            // test ID
                            mCond = "1010";
                            mOpcode = "0001";
                            mDualReadToggle = "1";
                            // will perform same checks as ==, check that method for more info

                            // load two numbers in
                            // will have to grab blocks
                            string temp = compiler.parsedAssemblyList[i];
                            if (temp.IndexOf("test") != 0)
                            {
                                // remove text before
                                temp = temp.Remove(0, compiler.parsedAssemblyList[i].IndexOf("test"));
                            }
                            temp = temp.Remove(temp.IndexOf("test"), 7);
                            string block1 = null;
                            string block2 = null;
                            int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block1 += currentChar;
                            }
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block2 += currentChar;
                            }
                            // because of the RC4.0 architecture, we'll need to detect if either block is an
                            // immediate int. If it is, then we have to set that as immediate value, and have
                            // the other value become mramr
                            // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                            // checker already checks that and takes care of that for us (although the error produced 
                            // is a bit misleading)
                            // also b/c of the RC4.0 architecture and the way it handles immediate values
                            // (piped into cache a instead of cache b like the RC3.0), we will have to change this slightly
                            if (!block1.Contains("$"))
                            {
                                // block1 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);

                                // then set other block to mramr
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                            }
                            else if (!block2.Contains("$"))
                            {
                                // block2 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                                // then set other block to mramr
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                            }
                            else
                            {
                                // convert both blocks to mramr and mramw
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                            }
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("ui"))
                        {
                            // collect which one it is
                            if (compiler.parsedAssemblyList[i].Contains("ui0"))
                            {
                                mCond = "0001";
                                // easy! :D
                            }
                            else if (compiler.parsedAssemblyList[i].Contains("ui1"))
                            {
                                mCond = "1001";
                                // easy! :D
                            }
                            else if (compiler.parsedAssemblyList[i].Contains("ui2"))
                            {
                                mCond = "0101";
                                // easy! :D
                            }
                            else if (compiler.parsedAssemblyList[i].Contains("ui3"))
                            {
                                mCond = "1101";
                                // easy! :D
                            }
                        }
                    }
                    #endregion

                    #region ramw (ONLY) component
                    if (compiler.parsedAssemblyList[i].Contains("ramw") && !compiler.parsedAssemblyList[i].Contains("ramr"))
                    {
                        // will assume that [ramw $address$ temp] is only case of this, so we will keep it that way
                        mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                    }
                    #endregion

                    #region ramr + ramw component (for copying values)
                    // can be optimized
                    if (compiler.parsedAssemblyList[i].Contains("ramr") && compiler.parsedAssemblyList[i].Contains("ramw"))
                    {
                        // grab two memory addresses needed
                        mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                        int nextStartLoc = compiler.parsedAssemblyList[i].IndexOf("$");
                        nextStartLoc = compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1);
                        mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1)), false, 5);

                        // will also need to have an opcode to have value pass through ALU to get back to RAM; add will be sufficient
                        mOpcode = "1000";
                    }
                    #endregion

                    #region fjump component
                    if (compiler.parsedAssemblyList[i].Contains("fjump"))
                    {
                        mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("fjump")) + 5), true, 6);
                    }
                    else
                    {
                        if (mGFalse == "000000" && generate == true)
                        {
                            mGFalse = functionLibrary.convertNumberToBinary(fJumpDefault, true, 6);
                        }
                    }
                    #endregion

                    #region tjump component
                    if (compiler.parsedAssemblyList[i].Contains("tjump"))
                    {
                        mGTrue = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("tjump")) + 5), true, 6);
                    }
                    #endregion

                    if (generate == true)
                    {
                        currentInstructionWord = mGPUGX + " " + mGPUGY + " " + mClockID + " " + mGTrue + " " + mGFalse + " " + mCond + " " + mImmediate + " " + mDualReadToggle + " " + mRamR + " " + mRamW + " " + mOpcode + " " + mMode + " " + mExtension + " " + mCacheC + " " + mCacheAddr;
                        addToMachineCodeList(currentInstructionWord, i + 1);
                    }
                }
                // after machine code generation is complete, remove last line of machine code (as it's EOF and has all 0's written)
                machineCodeList.RemoveAt(machineCodeList.Count - 1);
                logging.logDebug("Machine code generation complete!", logging.loggingSenderID.machineCodeParser);
                logging.logDebug("Dumping generated machine code ...", logging.loggingSenderID.machineCodeParser);
                // remove all whitespaces in machine code first!
                for (int i = 0; i < machineCodeList.Count; i++)
                {
                    machineCodeList[i] = functionLibrary.RemoveWhitespace(machineCodeList[i]);
                }
                for (int i = 0; i < machineCodeList.Count; i++)
                {
                    logging.logDebug("[" + (i + 1).ToString() + "]: " + machineCodeList[i] + " | len " + machineCodeList[i].Length.ToString(), logging.loggingSenderID.machineCodeParser);
                }

            }
            #endregion
        }

        // related functions
        private static int acquireMemoryAddress(string codeToAcquireFrom, int lengthOfStringBefore)
        {
            int IDIndexCounter = 1;
            int memAddr = -1;
        // uncomment this if we need to debug internally!
        //logging.logDebug("acquireblockID stringbefore: " + codeToAcquireFrom.Substring(0, lengthOfStringBefore));
        //logging.logDebug("lengthofstringbefore: " + lengthOfStringBefore.ToString());
        TopOfFunction:
            try
            {
                //logging.logDebug(codeToAcquireFrom.Substring(lengthOfStringBefore + 1, IDIndexCounter));
                Convert.ToInt16(codeToAcquireFrom.Substring(lengthOfStringBefore + 1, IDIndexCounter));
                IDIndexCounter++;
                goto TopOfFunction;
            }
            catch (FormatException)
            {
                //logging.logDebug(codeToAcquireFrom.Substring(lengthOfStringBefore + 1, IDIndexCounter - 1));
                memAddr = Convert.ToInt16(codeToAcquireFrom.Substring(lengthOfStringBefore + 1, IDIndexCounter - 1));
            }
            catch (ArgumentOutOfRangeException)
            {
                memAddr = Convert.ToInt16(codeToAcquireFrom.Substring(lengthOfStringBefore + 1, IDIndexCounter - 1));
            }
            return memAddr;
        }
        private static void addToMachineCodeList(string currentInstructionWord, int lineOfCodeAt)
        {
            logging.logDebug("[" + (machineCodeList.Count + 1).ToString() + "]: word is " + currentInstructionWord + ", len " + currentInstructionWord.Length.ToString(), logging.loggingSenderID.machineCodeParser);
            machineCodeList.Add(currentInstructionWord);
        }
    }
}
