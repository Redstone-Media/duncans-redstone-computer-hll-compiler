﻿using System;
using System.Linq;
namespace DRCHLLC_Core_Compiler
{
    class tokens
    {
        /*
         * This class will contain all functions and data types about tokens
         * and token manipulation for the DRCHLL-C. 
         * 
         * Created 8/19/2016 by AGuyWhoIsBored
         * 
        */

        // tokenizer will read and parse from this table AFTER parsing through the internal core tokens
        // all extension tokens (even internal ones) will need to use this table in order to be parsed properly
        // INTERNAL: migrate RC40E to importedtokentable and RC40E internal specifics to new class
        public static string[] importedTokenTable = new string[0];
        public static string[] importedSyntaxTokenBindingTable = new string[0];
        public static int importedTokenTableCount = 0;
        public static int importedBindingTableCount = 0;

        public static int totalTokenCount = 41; // count native tokens first

        // core types of ARCISS language v1.0
        // all imported tokens (even internal extensions) will follow [className/extensionName][./_]types[] syntax
        // token ID's (#'s) have to follow 0< rule
        // see below for example
        // they will then be compiled into one big token list to be referenced from
        // compiler will first read from core types array (below), and then read from importedTokenTable

        public static string[] types = new string[41]
            { /*00-04*/ "invalid", "nulltype", "data_int", "data_hex", "variable",
              /*05-11*/ "operator_plus", "operator_minus", "operator_equals", "operator_greaterthan", "operator_lessthan", "operator_rshift", "operator_lshift",
              /*12-15*/ "operator_ifequals", "operator_increment", "operator_decrement", "operator_not",
              /*16-21*/ "identifier_if", "identifier_else", "identifier_causes", "identifier_jumploc", "identifier_new", "identifier_eof", // jumploc and EOF aren't in tokenizing methods - only conversion as they are special cases
              /*22-26*/ "end_of_line", "grouper_openparentheses", "grouper_closeparentheses", "grouper_openbracket", "grouper_closebracket",
              /*27-33*/ "func_exit", "func_flush", "func_output", "func_input", "func_jump", "sc_overflow", "sc_underflow",
              /*34-40*/ "ext_func", "ext_endfunc", "ext_data_all", "ext_data_1", "ext_data_2", "ext_enddata", "ext_func_leave" }; // may change how this works

        // use arrays for ID's
        public static int Tokenize(string stringToToken, int sender)
        {
            // sender 0 - compiler
            // sender 1 - importer

            // try core token types first
            // number
            try
            {
                Convert.ToInt32(stringToToken);
                return 2;
            }
            catch (FormatException)
            {
                // hex number
                if (stringToToken.Contains("0x"))
                {
                    return 3;
                }
                // operator
                switch (stringToToken)
                {
                    case "+":
                        return 5;
                    case "-":
                        return 6;
                    case "=":
                        return 7;
                    case "<":
                        return 8;
                    case ">":
                        return 9;
                    case ">>":
                        return 10;
                    case "<<":
                        return 11;
                    case "==":
                        return 12;
                    case "++":
                        return 13;
                    case "--":
                        return 14;
                    case "!!":
                        return 15;
                }
                // identifiers
                switch (stringToToken)
                {
                    case "if":
                        return 16;
                    case "else":
                        return 17;
                    case "causes":
                        return 18;
                    case "new":
                        return 20; // id_jumploc (19) is special token case and handled separately
                }
                // semicolon
                switch (stringToToken)
                {
                    case ";":
                        return 22; // id_eof (21) is special token case and handled separately
                }
                // data groupers
                switch (stringToToken)
                {
                    case "(":
                        return 23;
                    case ")":
                        return 24;
                    case "{":
                        return 25;
                    case "}":
                        return 26;
                }
                // functions
                switch (stringToToken)
                {
                    // go through all available functions
                    case "exit":
                        return 27;
                    case "flush":
                        return 28;
                    case "output":
                        return 29;
                    case "input":
                        return 30;
                    case "jump":
                        return 31;
                }

                // special conditions
                switch (stringToToken)
                {
                    case "SHIFT_OVERFLOW":
                        return 32;
                    case "SHIFT_UNDERFLOW":
                        return 33;
                }

                //extension stuff
                if (sender == 1)
                {
                    switch (stringToToken)
                    {
                        case "func":
                            return 34;
                        case "endfunc":
                            return 35;
                        case "data":
                            return 36;
                        case "data[1]":
                            return 37;
                        case "data[2]":
                            return 38;
                        case "enddata":
                            return 49;
                        case "int":
                            return 2; // return regular data_int
                        case "hex":
                            return 3; // return regular data_hex
                        case "leave":
                            return 40;
                    }
                }

                // imported token check [to work on later]
                // put imported token check code here
                for (int i = 0; i < importedTokenTable.Length; i++)
                {
                    if(stringToToken == importedTokenTable[i]){ return types.Length + (i + 1); }
                }


                // check if input passed into tokenizer is empty
                if (string.IsNullOrWhiteSpace(stringToToken) || stringToToken.Length == 0)
                { return 0; /* add discard marker to remove later */ }

                // assume everything else is a variable
                // run variable check syntax though
                // syntax: alphanumeric
                if (!stringToToken.All(char.IsLetterOrDigit))
                {
                    // return invalid
                    logging.logWarning("token '" + stringToToken + "' doesn't match variable syntax! Setting as invalid token! The program will most likely not compile because of this!", logging.loggingSenderID.tokenizer);
                    logging.logWarning("Did you forget to import external libraries?", logging.loggingSenderID.tokenizer);
                    return 0;
                } 
                else { return 4; }
            }
        }

        public static void addImportedToken(string importedToken)
        {
            // first check if value already exists in arrays - if it does, throw error
            // if it doesn't add into table and increment tokenCount

            Array.Resize(ref importedTokenTable, importedTokenTable.Length + 1);
            importedTokenTable[importedTokenTableCount] = importedToken;
            importedTokenTableCount++;
        }

        public static void bindSyntax(string syntaxTableID)
        {
            // works in tandem with tokens
            // to bind multiple syntaxes, use "-" (see rc40e class)
            // WILL BIND BASED OFF OF ARRAY INDEXES, SO ORDER MATTERS!
            Array.Resize(ref importedSyntaxTokenBindingTable, importedSyntaxTokenBindingTable.Length + 1);
            importedSyntaxTokenBindingTable[importedBindingTableCount] = syntaxTableID;
            importedBindingTableCount++;
        }

        #region conversion

        // str > int
        public static int convertInt(string token)
        {
            int converted = -1;

            try
            {
                // haven't tested yet
                converted = Convert.ToInt32(token);
            }
            catch (Exception)
            {
                switch (token)
                {
                    // human readable
                    case "invalid":
                        converted = 0;
                        break;
                    case "null":
                        converted = 1;
                        break;
                    case "data_int":
                        converted = 2;
                        break;
                    case "data_hex":
                        converted = 3;
                        break;
                    case "variable":
                        converted = 4;
                        break;
                    case "operator_plus":
                        converted = 5;
                        break;
                    case "operator_minus":
                        converted = 6;
                        break;
                    case "operator_equals":
                        converted = 7;
                        break;
                    case "operator_greaterthan":
                        converted = 8;
                        break;
                    case "operator_lessthan":
                        converted = 9;
                        break;
                    case "operator_rshift":
                        converted = 10;
                        break;
                    case "operator_lshift":
                        converted = 11;
                        break;
                    case "operator_ifequals":
                        converted = 12;
                        break;
                    case "operator_increment":
                        converted = 13;
                        break;
                    case "operator_decrement":
                        converted = 14;
                        break;
                    case "operator_not":
                        converted = 15;
                        break;
                    case "identifier_if":
                        converted = 16;
                        break;
                    case "identifier_else":
                        converted = 17;
                        break;
                    case "identifier_causes":
                        converted = 18;
                        break;
                    case "identifier_jumploc":
                        converted = 19;
                        break;
                    case "identifier_new":
                        converted = 20;
                        break;
                    case "identifier_eof":
                        converted = 21;
                        break;
                    case "end_of_line":
                        converted = 22;
                        break;
                    case "grouper_openparentheses":
                        converted = 23;
                        break;
                    case "grouper_closeparentheses":
                        converted = 24;
                        break;
                    case "grouper_openbracket":
                        converted = 25;
                        break;
                    case "grouper_closebracket":
                        converted = 26;
                        break;
                    case "func_exit":
                        converted = 27;
                        break;
                    case "func_flush":
                        converted = 28;
                        break;
                    case "func_output":
                        converted = 29;
                        break;
                    case "func_input":
                        converted = 30;
                        break;
                    case "func_jump":
                        converted = 31;
                        break;
                    case "sc_overflow":
                        converted = 32;
                        break;
                    case "sc_underflow":
                        converted = 33;
                        break;
                    case "ext_func":
                        converted = 34;
                        break;
                    case "ext_endfunc":
                        converted = 35;
                        break;
                    case "ext_data_all":
                        converted = 36;
                        break;
                    case "ext_data_1":
                        converted = 37;
                        break;
                    case "ext_data_2":
                        converted = 38;
                        break;
                    case "ext_enddata":
                        converted = 39;
                        break;
                    case "ext_func_leave":
                        converted = 40;
                        break;
                }
                // all imported tokens
                for (int i = 0; i < importedTokenTable.Length; i++)
                {
                    if (token == importedTokenTable[i]) { return types.Length + (i + 1); }
                }
            }
            return converted;
        }
        // int > str
        public static string convertStr(int token, bool humanReadable = false)
        {
            string converted = "";
            if (humanReadable)
            {
                switch (token)
                {
                    case 0:
                        converted = "invalid";
                        break;
                    case 1:
                        converted = "null";
                        break;
                    case 2:
                        converted = "data_int";
                        break;
                    case 3:
                        converted = "data_hex";
                        break;
                    case 4:
                        converted = "variable";
                        break;
                    case 5:
                        converted = "operator_plus";
                        break;
                    case 6:
                        converted = "operator_minus";
                        break;
                    case 7:
                        converted = "operator_equals";
                        break;
                    case 8:
                        converted = "operator_greaterthan";
                        break;
                    case 9:
                        converted = "operator_lessthan";
                        break;
                    case 10:
                        converted = "operator_rshift";
                        break;
                    case 11:
                        converted = "operator_lshift";
                        break;
                    case 12:
                        converted = "operator_ifequals";
                        break;
                    case 13:
                        converted = "operator_increment";
                        break;
                    case 14:
                        converted = "operator_decrement";
                        break;
                    case 15:
                        converted = "operator_not";
                        break;
                    case 16:
                        converted = "identifier_if";
                        break;
                    case 17:
                        converted = "identifier_else";
                        break;
                    case 18:
                        converted = "identifier_causes";
                        break;
                    case 19:
                        converted = "identifier_jumploc";
                        break;
                    case 20:
                        converted = "identifier_new";
                        break;
                    case 21:
                        converted = "identifier_eof";
                        break;
                    case 22:
                        converted = "end_of_line";
                        break;
                    case 23:
                        converted = "grouper_openparentheses";
                        break;
                    case 24:
                        converted = "grouper_closeparentheses";
                        break;
                    case 25:
                        converted = "grouper_openbracket";
                        break;
                    case 26:
                        converted = "grouper_closebracket";
                        break;
                    case 27:
                        converted = "func_exit";
                        break;
                    case 28:
                        converted = "func_flush";
                        break;
                    case 29:
                        converted = "func_output";
                        break;
                    case 30:
                        converted = "func_input";
                        break;
                    case 31:
                        converted = "func_jump";
                        break;
                    case 32:
                        converted = "sc_overflow";
                        break;
                    case 33:
                        converted = "sc_underflow";
                        break;
                    case 34:
                        converted = "ext_func";
                        break;
                    case 35:
                        converted = "ext_endfunc";
                        break;
                    case 36:
                        converted = "ext_data_all";
                        break;
                    case 37:
                        converted = "ext_data_1";
                        break;
                    case 38:
                        converted = "ext_data_2";
                        break;
                    case 39:
                        converted = "ext_enddata";
                        break;
                    case 40:
                        converted = "ext_func_leave";
                        break;
                }
                // for imported tokens
                try
                {
                    converted = importedTokenTable.ElementAt(token - (types.Length + 1));
                }
                catch (Exception) { /* do nothing */}
            }
            else
            {
                converted = Convert.ToString(token);
            }
            return converted;
        }

        #endregion
    }
}
