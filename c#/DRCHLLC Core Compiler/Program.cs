﻿using System;
using System.IO;

namespace DRCHLLC_Core_Compiler
{
    class Program
    {
        /*Duncan's Redstone Computer HLL Compiler [DRCHLL-C] [ARCISS v1.0]
         * -------------------------

          Created 6/21/2015. 
          Copyright © AGuyWhoIsBored. All rights reserved.  

          Project started on 6/21/15
          Build v1.0a pushed 7/22/16
          Build v1.1a pushed 7/25/16
          Build v1.2a pushed 7/25/16
          Build v1.3a pushed 7/31/16
          Build v1.4a pushed 12/26/16
          Build v1.5a pushed 8/8/17

          Changelog:
            v1.0a: initial release

            v1.1a:
                Removed all unnecessary and commented code (that I'm not using anymore) [DONE: 9356ln > 7820ln]
                Implement Fast Upload [DONE (for now)]
                    Fast Upload stats (on RC3.0):
                        Regular Upload Time (w/o clean) on RC_fibonacci_program (14 lines): 472.9 seconds
                        Regular Upload Time (w/ clean) on RC_fibonacci_program (14 lines): 1144.3 seconds
                        Fast Upload Time (w/o clean) on RC_fibonacci_program (14 lines): 61.7 seconds (7.6 times faster!)
                        Fast Upload Time (w/ clean) on RC_fibonacci_program (14 lines): 80.3 seconds (14.3 times faster!!!)
                Work on GUI Updates
                    Progressbar, Compiler Settings Window (and settings) [pushback to later revisions]

            v1.2a:
                Refine program in general
                    Fix reset b/c when we run multiple operations again and again it crashes / hangs [done, check known bugs]
                    Add ability to reset uploadThread [done]
                Optimize functions that compiler uses [done for now]
                    [7973ln > 7867ln]
                Update test machine code generator - see specific method for more details
                    implement / fix [value/#] == [value/#] [done for now]
                    fix [value/#] < [value/#] [done for now]
                        2nd fix (inverting sign) not needed as syntax checker prevents any case of that
                    fix [value/#] > [value/#] [done for now]
                        2nd fix (inverting sign) not needed as syntax checker prevents any case of that

            v1.3a:
                Implement first version of program optimizations
                    Having independent conditonal branching optimization disabled breaks machine code generator when we
                        read using blocks [fixed]
                Updated MAT generator to fix severe bug that caused program to crash / hang when certain
                    variable naming schemes were used
                Fixed bug in LexData function where line of code values get out-of-sync when removing invalid tokens
                Fixed bug in MAT generation where jumps from jump instructions are inaccurate
                Fixed bug in MAT updater where all instances of a variable would be replaced, not first instance, which caused bugs
                Fixed bug in machine code generator where ui condition code and ui input code wouldn't be generated correctly
                Updated machine code generator to match architecture changes on both targets and to fix a couple of bugs in machine code generation
                Added writeback instructions to function generator [mostly]

                **Consider splitting ALU comp / ALU accept data from test == / < / > instructions**

                [HARDWARE] Changed RC3.0 architecture to have immediate value pipe through cache A instead of cache B to allow [inst. var. [operation] immediate value], i.e. in sub]
                [HARDWARE] Changed RC4.0 architecture to have immediate value pipe through cache B instead of cache A to allow [inst. var. [operation] immediate value], i.e. in sub]
                [HARDWARE] There is an architectual limit on the RC4.0 using BSR / BSL functions:
                    bsl can ONLY be accessed on cache A / bsr can ONLY be accessed on cache B
                    Means we CANNOT bsl immediate values, as immediate values are only piped into cache B

                DEBUG COMMENT ON LINE 4392 'currentTokenIndex++;' in 2nd stage if parser generator! FUNCTION GENERATOR MAY NEED TO BE FIXED [IS WORKING FOR NOW]
                CREATING VARIABLES USING OTHER VARIABLES' VALUES IS NOT SUPPORTED IN CODE / LANGUAGE YET! (possibly change for v1.1 of language)
                CHECK MAT GENERATOR: if (something) == currentTokenID

                In parser function generator, NEED TO FIX! See bresenhamlinealgo2 - line(s) 20 & 21 / line 69 in program file

            v1.4a:
                Implemented source control (project repository added to bitbucket)
                    https://bitbucket.org/AGuyWhoIsBored/duncans-redstone-computer-hll-compiler
                [skipped] Port to c++ (for multiplatform support)
                    port will be in separate repo
                    https://bitbucket.org/AGuyWhoIsBored/duncans-redstone-computer-hll-compiler-multiplatform
                Organized code and distributed to multiple classes
                    This will help us make the transition to make the compiler extremely modular
                Began to add functionality to import libraries and extensions
                    Some functionality for extension support was added, but will add more in later revs
                    Needs to be implemented as now we don't have a way of natively importing RC40E
                Rewrite function generator, as VERY clunky / unorganized / inefficient / all three of those
                    Make sure we don't do what we did last time with the test condition, as there CAN be code where the test condition after
                        the function(s) is completely irrelevant to the function(s). 
                Updated syntax error msg to have line of code from original file / buffer - and maybe also display that line of code
                Added 'formatting-fixer' / rewrite compiler functions to support all types of formatting
                    as some people write code like this:
                        value1 = 5; value2 = 6; value3 = 7; if(value1 == 3) { value1 = 6; value5 = -1; } else { nothing here } - 
                            and this is technically still correct! So we need to update its formatting (maybe) to have it compatible with our compiler's functions
                            OR we could change the functions to fit any formatting
            v1.5a:
                Separated compiler, uploader, and GUI programs [mostly done, binaries integration w GUI still incomplete]
                    GUI will launch and manage arguments passed to compiler and uploader
                Redid uploader [done]
                    Implemented the same modular design that is present in the core compiler
                    Added ability to change start coordinates 
                    Added ability to upload straight from a file (from the .pb file generated from compiler)
                Add in var syntax checker [done]
                    WE HAVE A VAR NAME CONFLICT! IF USER INIT'S 'temp' AS VAR IN PROGRAM IT WILL INTERFERE!
                        OR we can have 'temp' be programmed in another way so that it won't be able to be tampered with. [DONE, issue #3 on repo]
            v1.6a [plans]:
                Implement support for RC4.0 cache in compiler
                    consider giving user granular control over RC4.0 cache w/ functions?
                Continue to improve and build on eventual library / extension support
                    Imported function functionality added
                        Includes custom syntax tables and custom function tokens
                Re-organize code for internal extensions (such as RC40E)
                    attempt to put it in its own class? And reorganize the way the compiler calls for specific data
                Complete GUI and full binary integration with GUI
                Implement more optimizations
                    Implement various peephole optimizations?
                Add auto-updater (to GUI)
                Rewrite syntax checker (?) 
                Rewrite function generator (?)
                    MASSIVELY INEFFICIENT / CLUTTERED!
                Optimize program
                Remove 'slow' upload options if determined not needed
                Clean up / organize code
                Check and test extensively for bugs


            Push out as b1 (beta 1)
            Push out as RC1 (release candidate)
            ...

            port to c++ to enable multiplatform support
                WILL BE REWRITING ENTIRE PROGRAM TO BE MORE OPTIMIZED!

        * --------------------------

          Current known bugs:
            -When doing [fast] upload, the inventory is somehow opened and that makes the uploading of the program 
                less accurate
                [PARTIAL FIX]: Had to add 200ms of delay after [compiler]: uploading program to core ... statement
                [added to repo issue tracker]
            -[HARDWARE] RC3.0 bsr/bsl hardware inefficient / outdated / broken / all three of those
            -[HARDWARE] There is an architectual limit on the RC4.0 using BSR / BSL functions:
                bsl can ONLY be accessed on cache A / bsr can ONLY be accessed on cache B
                Means we CANNOT bsl immediate values, as immediate values are only piped into cache B
            -Syntax checker may incorrectly identify erorrs 
                traces back to LexData as we don't have whitespace after last char in buffer so thus it doesn't register that char
                i.e. 'new value1 = 1' - should throw semicolon after new var declaration, but throws error 4.0
                to fix, user has to type 'new value1 = 1 ' so that whitespace triggers attempted tokenization
                [potential fix - add space after each line]

        * --------------------------

          To-do / want-to-do list for Duncan's Redstone Computer HLL Compiler / ARCISS:

          Add ability to actually import extensions (from a different file, and the compiler will read and load them)  

          ***OPTIMIZE ALL FUNCTIONS THAT COMPILER USES! MOST LIKELY EXTREMELY CLUNKY AND UNOPTIMIZED!***

          ARCISS v1.1 planned changes
            Make ARCIS Spec more flexible (in regards to groupings, parentheses, etc.)
            When functions require numbers, make it so that we can use variables, functions (that return integers), OR hex data! (as hex data is just encoded binary)
            Add function support
                Adding function support will enable support for importing extensions and libraries!
            Add logical operators to language
                Add AND (&) statements to language
                Add OR (or) statements to language
                Add <= and >= operators to language
                Add += and -= operators to language
                Add != operator to language
            Add support for [new var = oldvar] in language
            Add boolean data type into language 
                This will be converted to "0" and "1" in the machine code but for HLL add boolean
            Add support for defining multiple variables at one time in language
                i.e. "new var1, var2, var3 = 0;"
            Implement [extensionID].[functionName] syntax
                This will keep our code a lot more organized!

          Parser
            FIX PARSER - ((value1 + value2) + (value3 + value4)) DOES NOT PARSE CORRECTLY!
            Make data groupings more flexible
            Currently only supports (((value1+value2) + value3) + value4)
            DOES NOT Support (value1 + (value2 + value3 + (value4 + value5))) (2-tokened functions are on the left instead of the right)
            DOES NOT Support ((value1 + value2) + (value3 + value4)) and etc ... (doing operations on results from grouped functions)
                ^^ This will probably be a quirk of the language and maybe updated/changed with compiler revisions OR language revisions...
                ^^ THIS WAS CODED IN, BUT FOR SOME REASON DOESNT SEEM TO WORK!

          Syntax checker
            UPDATE SYNTAX ERROR MSG TO HAVE LINE OF CODE FROM ORIGINAL FILE / BUFFER! AND MAYBE ALSO DISPLAY THAT LINE OF CODE!
            When syntax checker throws error, either give line # or area of code that's incorrect [gives line #, give everything else]
                Make it so that syntax checker also dumps part of code it's throwing error for (maybe a buffer for tokens it's checking through?)
            Check to see if variables are used before they are declared
            Check to see if entire line of code / thing being syntax checked is on one line [no need]
            Add syntax check for RC40E extension, << and >> functions, and ++ and -- functions [done]
            Check to see if numbers <= 255 (at least for currently available compile targets, as these computers are 8-bit)
            Add syntax check for << and >> functions (or change way those functions syntax to fit into the syntax checker) [done]
            Add syntax check in if statements so that we use ==, NOT = in if statements! [done]
            When checking syntax, add another array for each line of code to see what type of syntax it had (if it has multiple types), so that the parser
                will be able to parse better [not really needed]
            Have syntax for most variable-operator functions be very flexible
            Add syntax check for if(((functions)++) ...) // if(((functions)--) ...) // if(((functions)>>) ...) // if(((functions)<<) ...) // if(((functions) !!) ...) [done]
                Currently throws 'Unrecognized check value in if statement!'
                Currently throws 'Unbalanced parentheses in data grouped function!'
            Make syntax checker more flexible when using data-grouped values to change variables
                value1 = (value1 + value2) works
                (value1 + value2) = value1 doesn't work
            Make syntax checker invalidate if statement grouping syntax if there is one operator and two parentheses groups [done]

          Optimizations
            Optimizations to implement:
                -Duplicate variable /instruction setting (i.e. 2 lines of var1 = 0) [redundant code removal]
                    -If compiler detects two or more duplicate variable / instructions right after another (i.e. var1 = 0; var1 = 0; var1 = 0), compiler will delete
                        two of those lines of code
                -Variable clock speed implementation
                    -Use clock speed dictionary to determine how fast each function can run, and then dynamically group each part of program and set clock speed
                        accordingly (only available on RC4.0)
                -Out of order code execution (?)
                    -If it can be more efficient to run certain lines of code before others (and if it's possible), run those lines of code first to make program
                        more optimized. Will need to reorder to save in program order, though (I think).
                -Independent Conditional Branching
                    -If line of code has a conditional branch attached to instruction, separate the two and have one line of code for instruction and one dedicated to
                        conditional branch. This (in theory) should make program A LOT FASTER, but will increase program line count. 
                -Removal of unreachable code [redundant code removal]
                    -If any code is detected at unreachable points (i.e. after exit(); function, or like 30 lines after entire program), remove those lines of code
                -Grouping of same block of code as function [automtaic function grouping]
                    -If we run the same block of code multiple times, only actually implement block of code for it once and have all instances of it point to that one
                        block of code
                -Automatic parallelization (?)
                    -If parts of program can be run on more than one core to make it more efficient, distribute program code between cores
                -Memory location optimizations [memory optimizations]
                    -In the RC4.0, if program variables can be stored in cache, enable it so that program uses cache instead of RAM for faster performance.
                    -In program, try to use all memory addresses closer to front end of RAM (so 1, 2, 3, etc ...) instead of scattered memory locations, as we can read
                        faster from addresses closer to front end of RAM / memory location. This should already have been performed by the MAT generator
                -Memory Cache Optimization [memory optimizations]
                    -If RC4.0 is selected as compile target, attempt to use cache for memory instead of RAM! This will improve performance (possibly) significantly!
                -Instruction Combination Optimization
                    -Some instructions (on both computers) can be combined w/ one another, to shrink program size! However, this may (or may not) hinder performance!
                -Conditional Determination-Result Optimization [automatic function grouping optimizations]
                    -This is poorly named!
                    -If we have a condition testing for something and the result if it's true does that same thing, do that thing only once before test and save result
                -Removal of var = 0 malloc instructions [redundant code removal]
                    -This optimization will remove any malloc statement setting the variable to 0. 
                -Jump Consolidation Optimization [redundant code removal]
                    -This optimization will remove line of code 'y' and have fjump y on test loop back onto itself:
                        [1] if(condition) tjump x fjump y
                        [y] fjump 1
                -Peephole optimizations
                    -This set of optimizations will optimize specific sets and situations of code!


        */

        // the actual compiler code will be here in this program
        // will ONLY BE COMPILER CODE! Uploader will be another program!

        // this program will include things such as:
        // - parsing flags
        // - loading resources
        // - etc...
        // - then jump straight into assigned compile job
        // will write results in temp file
        // args :
        // [program] -f "file" -t [targetID] [-o optimizationCollectiveID] -c [coreID] [-d (toggle debug)] [-r [resourceFolder]]

        // version stuff

        public static string compilerVersion = "1.6a";         // version of compiler program
        public static string compilerLanguageVersion = "1.0"; // version of ARCISS language compiler supports

        // arguments and settings
        public static string compileTarget = "-1";
        public static string optimizationCollectiveID = "-1";
        public static string compileCoreTarget = "-1";
        public static string sourceFileLocation = "";
        public static string resourceFolderLocation = "";
        public static bool debug = false;
        public static bool moreVerboseDebug = true; // eventual support for multiple levels of debugging
                                                    // performance improvements noted w/ less debugging
                                                    // "test-active-dev.pb" w/ -d + moreVerbose = 9.657s
                                                    // "test-active-dev.pb" w/ -d = 9.614
                                                    // "test-active-dev.pb" w/ no debug = 9.567
        private static string userCommand = null;

        static void Main(string[] args)
        {
            Console.Title = "DRCHLLC Core Compiler";
            Console.WriteLine("--------");
            Console.WriteLine("DRCHLL-C Core Compiler v" + compilerVersion);
            Console.WriteLine("Supports ARCISS spec v" + compilerLanguageVersion);
            Console.WriteLine("Written and developed by AGuyWhoIsBored");
            Console.WriteLine("--------");
            Console.WriteLine("");

            TopOfArgumentAsk:
            // load arguments
            if (args.Length == 0){ Console.WriteLine("Waiting for user input ..."); userCommand = Console.ReadLine(); }
            else
            {
                Console.WriteLine("Prepassed arguments found!");
                for (int i = 0; i < args.Length; i++)
                {
                    // for formatting
                    if (i == 0) { userCommand += args[i]; }
                    else { userCommand += (" " + args[i]); }
                }
            }
            Console.WriteLine("Beginning argument parsing ...");
            Console.WriteLine("Arguments: " + userCommand);
            #region argument / flag parsing
            int index = 0;
            int flagCollectID = 0;
            int quoteCount = 0;
            char currentChar;
            string bufferString = null;
            userCommand = userCommand + " ";
            StringReader sr = new StringReader(userCommand);
            while(index != userCommand.Length)
            {
                currentChar = (char)sr.Read();
                index++;
                bufferString += currentChar;
                switch(currentChar)
                {
                    case ' ':
                        if (bufferString == "-f ") { flagCollectID = 1; }
                        else if (bufferString == "-t ") { flagCollectID = 2; }
                        else if (bufferString == "-o ") { flagCollectID = 3; }
                        else if (bufferString == "-c ") { flagCollectID = 4; }
                        else if (bufferString == "-d ") { debug = true; Console.WriteLine("debug mode enabled!"); }
                        else if (bufferString == "-r ") { flagCollectID = 5; }
                        else
                        {
                            if (flagCollectID == 2) { compileTarget = bufferString; compileTarget = functionLibrary.RemoveWhitespace(compileTarget); flagCollectID = 0; }
                            else if (flagCollectID == 3) { optimizationCollectiveID = bufferString; optimizationCollectiveID = functionLibrary.RemoveWhitespace(optimizationCollectiveID); flagCollectID = 0; }
                            else if (flagCollectID == 4) { compileCoreTarget = bufferString; compileCoreTarget = functionLibrary.RemoveWhitespace(compileCoreTarget); flagCollectID = 0; }
                        }
                        if(quoteCount == 0) { bufferString = null; }
                        break;
                    case '"':
                        quoteCount++;
                        if(flagCollectID == 1 && quoteCount == 2) { sourceFileLocation = bufferString; sourceFileLocation = sourceFileLocation.Remove(sourceFileLocation.Length - 1); quoteCount = 0; flagCollectID = 0; }
                        else if(flagCollectID == 5 && quoteCount == 2) { resourceFolderLocation = bufferString; resourceFolderLocation = resourceFolderLocation.Remove(resourceFolderLocation.Length - 1); quoteCount = 0; flagCollectID = 0; }
                        bufferString = null;
                        break;
                }
            }
            #endregion

            // dumping parsed flag variables
            if(debug)
            {
                Console.WriteLine("compileTarget: " + compileTarget.ToString());
                Console.WriteLine("optimizationCollectiveID: " + optimizationCollectiveID.ToString());
                Console.WriteLine("compileCoreTarget: " + compileCoreTarget.ToString());
                Console.WriteLine("sourceFileLocation: " + sourceFileLocation);
                Console.WriteLine("resourceFolderLocation: " + resourceFolderLocation);
            }

            // not going to check advanced argument syntax because we're going to assume that the user knows that they're doing
            // especially if they have a program like this
            if (compileTarget == "-1"
                || compileCoreTarget == "-1"
                || sourceFileLocation == "")
            { Console.WriteLine("Core arguments are incomplete! Please check your arguments and try again!"); goto TopOfArgumentAsk; }

            #region setting compiler optimizations
            // [123456] < bits
            // 1: variableClockSpeedOptimization
            // 2: independentConditionalBranchingOptimization
            // 3: redundantCodeRemovalOptimization
            // 4: automaticFunctionGrouping
            // 5: memoryManagementOptimizations
            // 6: instructionMergingOptimizations
            if (optimizationCollectiveID != "-1")
            {
                Console.WriteLine("Optimizations enabled! Enabling selected optimizations ...");
                compiler.enableOptimizations = true;
                try
                {
                    optimizationCollectiveID = functionLibrary.convertNumberToBinary(Convert.ToInt16(optimizationCollectiveID), false, 6);
                    StringReader sr1 = new StringReader(optimizationCollectiveID);
                    char currentChar1;
                    int index1 = 0;
                    while (index1 != optimizationCollectiveID.Length)
                    {
                        currentChar1 = (char)sr1.Read();
                        index1++;
                        // 1
                        if (currentChar1 == '1' && index1 == 1) { compiler.enableVariableClockSpeedOptimization = true; if (debug) Console.WriteLine("Optimization 1 enabled!"); }
                        else if(currentChar1 == '1' && index1 == 2) { compiler.enableIndependentConditionalBranchingOptimization = true; if(debug) Console.WriteLine("Optimization 2 enabled!"); }
                        else if(currentChar1 == '1' && index1 == 3) { compiler.enableRedundantCodeRemovalOptimization = true; if(debug) Console.WriteLine("Optimization 3 enabled!"); }
                        else if(currentChar1 == '1' && index1 == 4) { compiler.enableAutomaticFunctionGrouping = true; if(debug) Console.WriteLine("Optimization 4 enabled!"); }
                        else if(currentChar1 == '1' && index1 == 5) { compiler.enableMemoryManagementOptimizations = true; if(debug) Console.WriteLine("Optimization 5 enabled!"); }
                        else if(currentChar1 == '1' && index1 == 6) { compiler.enableInstructionMergingOptimizations = true; if(debug) Console.WriteLine("Optimization 6 enabled!"); }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occured: " + e.Message);
                }
            }
            #endregion

            #region initializing compiler

            #region loading resources
            Console.WriteLine("Beginning resource load ...");
            if (resourceFolderLocation != "") { if (debug) { Console.WriteLine("Resource load ready to begin! Press [enter] to start resource import!"); Console.ReadLine(); } importer.initialize(); }
            else { Console.WriteLine("Resource folder not provided! Skipping resource load ..."); }
            #endregion

            // possibly add debug flag to dump all prepared tables?
            Console.WriteLine("Finalizing compiler initialization..."); // mainly dumping loaded tables
            compiler.initialize();
            #endregion

            Console.WriteLine("Compiler ready to start!");
            if (debug) { Console.WriteLine("Press [enter] to begin compile!"); Console.ReadLine(); }

            // begin main compile thread
            compiler.compile();
        }
    }
}
