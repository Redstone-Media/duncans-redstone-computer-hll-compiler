﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRCHLLC_Core_Compiler
{
    class rc40e
    {
        /*
         * This class will contain all functions and information needed for correctly enabling and 
         * handling all processes concering the internal RC40E extension. 
         * 
         * Created 8/26/2017 by AGuyWhoIsBored
         * 
         */

        public static bool enableCache = false;

        // this array is based off of the contents from the core types array
        public static string[] RC40E_types = new string[6]
        { /*1-6*/ "RC40E_GPU_encode", "RC40E_clear", "RC40E_GPU_drawPoint", "RC40E_GPU_erasePoint", "RC40E_GPU_granularUpdate", "RC40E_updateSpeed" };

        public static void checkExtensionStatus()
        {
            // RC40E loading code
            // this additional core extension will load if a 'RC40E.drchllc-enabler' file is present
            logging.logDebug("Looking for RC40E enabler file ...", logging.loggingSenderID.internalRC40E);
            if (!File.Exists(Program.resourceFolderLocation + "\\RC40E.drchllc-enabler")) { logging.logDebug("RC40E enabler file not found, not enabling RC40E internal extension.", logging.loggingSenderID.internalRC40E, true); }
            else
            {
                logging.logDebug("RC40E enabler file found! Enabling RC40E...", logging.loggingSenderID.internalRC40E, true);
                enableExtension();
                logging.logDebug("RC40E extension enabled!", logging.loggingSenderID.internalRC40E);
            }
        }

        static void enableExtension()
        {
            importer.ext_RC40E = true;
            // inject into loadedResourcesList
            importer.loadedResourcesList.Add(importer.resourceIDCounter, "ARCISS_RC40E");
            logging.logDebug("Interal RC40 extension assigned global resource identifier " + (importer.resourceIDCounter), logging.loggingSenderID.internalRC40E);
            importer.resourceIDCounter++;
            string RC40ETableKey = importer.loadedResourcesList.First(x => x.Value == "ARCISS_RC40E").Key.ToString();
            // inject tokens and bind syntax to tokens
            tokens.addImportedToken("GPU_encode"); tokens.bindSyntax("0.1");
            tokens.addImportedToken("GPU_clear"); tokens.bindSyntax("0.2");
            tokens.addImportedToken("GPU_drawPoint"); tokens.bindSyntax("0.3-0.4-");
            tokens.addImportedToken("GPU_erasePoint"); tokens.bindSyntax("0.5-0.6-");
            tokens.addImportedToken("GPU_granularUpdate"); tokens.bindSyntax("0.7");
            tokens.addImportedToken("RC40E_updateSpeed"); tokens.bindSyntax("0.8");
            // inject function syntax into syntax function table
            // ***FOR NOW*** NO NEED TO INCLUDE EOL TOKEN!
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".1", tokens.convertInt("GPU_encode").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("variable").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".2", tokens.convertInt("GPU_clear").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".3", tokens.convertInt("GPU_drawPoint").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".4", tokens.convertInt("GPU_drawPoint").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".5", tokens.convertInt("GPU_erasePoint").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".6", tokens.convertInt("GPU_erasePoint").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".7", tokens.convertInt("GPU_granularUpdate").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("data_hex").ToString()
                + "-" + tokens.convertInt("data_hex").ToString()
                // special case that we need to focus on here for the data_ints: 0<=x<=1
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".8", tokens.convertInt("RC40E_updateSpeed").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                // special case for data_int: 0<=x<=7
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            if (compiler.enableMemoryManagementOptimizations)
            {
                enableCache = true;
                logging.logDebug("RC40E caching optimizations enabled!", logging.loggingSenderID.internalRC40E);
            }
        }
    }
}
