﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace DRCHLLC_Core_Compiler
{
    class functionLibrary
    {
        /*
         * This class contains all miscellaneous functions needed to perform
         * compilations and other required tasks.
         * 
         * Created 12/28/2016 by AGuyWhoIsBored
         * 
         */

        public static bool checkForZero(string s)
        {
            if (string.IsNullOrWhiteSpace(s)) { return true; }
            else { return false; }
        }
        public static string calculateMD5Hash(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }
        public static string ReplaceFirst(string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }
        public static string[] RemoveElementFromArray(string[] IndicesArray, int RemoveAt)
        {
            string[] newIndicesArray = new string[IndicesArray.Length - 1];

            int i = 0;
            int j = 0;
            while (i < IndicesArray.Length)
            {
                if (i != RemoveAt)
                {
                    newIndicesArray[j] = IndicesArray[i];
                    j++;
                }

                i++;
            }

            return newIndicesArray;
        }
        public static string RemoveWhitespace(string input)
        {
            return new string(input.ToCharArray().Where(c => !char.IsWhiteSpace(c)).ToArray());
        }
        public static void AddToTokenArrays(string tokenedValue, string bufferString, string tokenLineOfCodeValue, int sender)
        {
            // sender 0 - compiler
            // sender 1 - importer
            // sender 2 - importer syntax
            if (sender == 0)
            {
                Array.Resize(ref compiler.tokenTokensArray, compiler.tokenTokensArray.Length + 1);
                Array.Resize(ref compiler.tokenValuesArray, compiler.tokenValuesArray.Length + 1);
                Array.Resize(ref compiler.tokenLineOfCodeValueArray, compiler.tokenLineOfCodeValueArray.Length + 1);
                compiler.tokenTokensArray[compiler.tokenValueCount] = tokenedValue;
                compiler.tokenValuesArray[compiler.tokenValueCount] = bufferString;
                compiler.tokenLineOfCodeValueArray[compiler.tokenValueCount] = tokenLineOfCodeValue;
                compiler.tokenValueCount++;
            }
            else if (sender == 1)
            {
                Array.Resize(ref importer.resourceTTokens, importer.resourceTTokens.Length + 1);
                Array.Resize(ref importer.resourceTValues, importer.resourceTValues.Length + 1);
                Array.Resize(ref importer.resourceTLineOfCodeValue, importer.resourceTLineOfCodeValue.Length + 1);
                importer.resourceTTokens[importer.resourceTCount] = tokenedValue;
                importer.resourceTValues[importer.resourceTCount] = bufferString;
                importer.resourceTLineOfCodeValue[importer.resourceTCount] = tokenLineOfCodeValue;
                importer.resourceTCount++;
            }
            //else if (sender == 2)
            //{
            //    Array.Resize(ref syntax.temptokenTokensArray, syntax.temptokenTokensArray.Length + 1);
            //    Array.Resize(ref syntax.temptokenValuesArray, syntax.temptokenValuesArray.Length + 1);
            //    Array.Resize(ref syntax.tempTokenLineOfCodeValueArray, syntax.tempTokenLineOfCodeValueArray.Length + 1);
            //    syntax.temptokenTokensArray[syntax.tempTokenArrayCount] = tokenedValue;
            //    syntax.temptokenValuesArray[syntax.tempTokenArrayCount] = bufferString;
            //    syntax.tempTokenLineOfCodeValueArray[syntax.tempTokenArrayCount] = tokenLineOfCodeValue;
            //    syntax.tempTokenArrayCount++;
            //}
        }
        public static int AcquireBlockID(string codeToParse, int lengthOfStringBefore)
        {
            // this function needs to exist b/c of possibilities that block ID's can go higher than single digit in complex programs
            int IDIndexCounter = 1;
            int blockID = -1;
        // uncomment this if we need to debug internally!
        //logging.logDebug("acquireblockID stringbefore: " + codeToParse.Substring(0, lengthOfStringBefore));
        TopOfFunction:
            try
            {
                Convert.ToInt16(codeToParse.Substring(lengthOfStringBefore, IDIndexCounter));
                IDIndexCounter++;
                goto TopOfFunction;
            }
            catch (FormatException)
            {
                blockID = Convert.ToInt16(codeToParse.Substring(lengthOfStringBefore, IDIndexCounter - 1));
            }
            return blockID;
        }
        public static string convertNumberToBinary(int pieceToConvert, bool flipBits, int padLength)
        {
            // this function will also pad our binary numbers with 0's to make it easier to just plug in
            string output = null;
            output = Convert.ToString(pieceToConvert, 2);
            if (flipBits == true)
            {
                output = new string(output.ToCharArray().Reverse().ToArray());
            }
            if (output.Length != padLength)
            {
                int increaseLength = padLength - (padLength - output.Length); // very important!
                if (flipBits == false)
                {
                    output = output.PadLeft((padLength - output.Length) + increaseLength, '0');
                }
                else
                {
                    output = output.PadRight((padLength - output.Length) + increaseLength, '0');
                }
            }

            return output;
        }
        public static void writeDebugLogOut()
        {
            if (Program.debug)
            {
                logging.logDebug("Writing debug log to file!", logging.loggingSenderID.compiler);
                logging.logDebug("Debug log located at '" + Directory.GetCurrentDirectory() + "\\debuglog.txt" + "'", logging.loggingSenderID.compiler);
                File.WriteAllLines(Directory.GetCurrentDirectory() + "\\debuglog.txt", new[] { logging.debugText });

            }
        }
    }
}
