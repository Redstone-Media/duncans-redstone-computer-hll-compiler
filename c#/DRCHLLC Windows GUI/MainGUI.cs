﻿using System;
using System.Windows.Forms;
using System.Diagnostics;

namespace DRCHLLC_Windows_GUI
{
    public partial class MainGUI : Form
    {
        // this program is a simple wrapper for the actual compiler and uploader command line binaries

        public string version = "1.0 (Windows edition)";

        // binary location variables
        string compilerBinaryLocation = "";
        string uploaderBinaryLocation = "";

        // compile variables - will be passed to compiler console app
        string resourceFolderLocation = "";

        // upload variables - will be passed to uploader console app
        string overrideStartCoordinates = "";
        string countdownInt = "-1";

        // collective variables - needed for both apps
        string compileTarget = "-1";
        string compileCoreTarget = "-1";
        string sourceFileLocation = "";
        string userCommand = null;


        public MainGUI() { InitializeComponent(); }

        private void MainGUI_Load(object sender, EventArgs e)
        {
            Text = "DRCHLLC GUI v" + version;

            // load all of the tooltip info
            ToolTip tt1 = new ToolTip(); ToolTip tt2 = new ToolTip(); ToolTip tt3 = new ToolTip(); ToolTip tt4 = new ToolTip(); ToolTip tt5 = new ToolTip(); ToolTip tt6 = new ToolTip();
            tt1.SetToolTip(loadSourceFileButton, "Select the source file you want to compile / upload.");
            tt2.SetToolTip(uploadButton, "Launch the uploader with the selected settings.");
            tt3.SetToolTip(compileButton, "Launch the compiler with the selected settings.");
            tt4.SetToolTip(label1, "Enter specific arguments here to pass to either the compiler or uploader.");
            tt5.SetToolTip(label3, "Select the location of the compiler resource folder if necessary here.");
            tt6.SetToolTip(compilerTargetListBox, "Select the computer target to which you want to upload or compile to.");
            tt6.SetToolTip(groupBox1, "Select the computer target to which you want to upload or compile to.");
        }

        private void compilerTargetListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // make sure to not change the index
            switch(compilerTargetListBox.SelectedIndex)
            {
                case 0:
                    compileTarget = "1"; compileCoreTarget = "1"; break;
                case 1:
                    compileTarget = "1"; compileCoreTarget = "2"; break;
                case 2:
                    compileTarget = "1"; compileCoreTarget = "3"; break;
                case 3:
                    compileTarget = "1"; compileCoreTarget = "4"; break;
                case 4:
                    compileTarget = "2"; compileCoreTarget = "1"; break;
                case 5:
                    compileTarget = "2"; compileCoreTarget = "2"; break;
                case 6:
                    compileTarget = "2"; compileCoreTarget = "3"; break;
                case 7:
                    compileTarget = "2"; compileCoreTarget = "4"; break;
                case 8:
                    compileTarget = "3"; compileCoreTarget = "1"; break;
                case 9:
                    compileTarget = "3"; compileCoreTarget = "2"; break;
                case 10:
                    compileTarget = "3"; compileCoreTarget = "3"; break;
                case 11:
                    compileTarget = "3"; compileCoreTarget = "4"; break;
            }

            compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Compile target changed to [" + compileTarget + "," + compileCoreTarget + "]" + Environment.NewLine;
        }

        private void selectResourceFolderButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.ShowDialog();
            if(fd.SelectedPath != null)
            {
                resourceFolderLocation = fd.SelectedPath;
                compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Resource folder changed to [" + resourceFolderLocation + "]" + Environment.NewLine;
                resourceFolderTextBox.Text = resourceFolderLocation;
            }
            fd.Dispose();
        }

        private void loadSourceFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog
            {
                Filter = "ARCISS Source Files (*.txt)|*.txt|ARCISS Binary Files (*.pb)|*.pb",
                Title = "Select file for Compiler/Uploader"
            };
            fd.ShowDialog();
            if (fd.FileName != null)
            {
                sourceFileLocation = fd.FileName;
                compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Source file changed to [" + sourceFileLocation + "]" + Environment.NewLine;
            }
            fd.Dispose();
        }

        private void uploadButton_Click(object sender, EventArgs e)
        {
            // build command
            string autoUserCommand = "";
            string uploadOptionCollectiveBinaryID = "";
            if (sourceFileLocation != "")
            {
                autoUserCommand += "-f \"\"\"" + sourceFileLocation + "\"\"\"";
                // serves for both target and core
                if (compileTarget != "-1")
                {
                    autoUserCommand += " -t " + compileTarget + " -c " + compileCoreTarget;
                    // **NOTE** Add in countdownInt, overrideStartCoordiantes, Alternate Upload method? 
                    // build uploadOptionCollectiveBinaryID
                    for (int i = 0; i < uploadOptionsListBox.Items.Count; i++)
                    {
                        if (uploadOptionsListBox.GetItemChecked(i)) { uploadOptionCollectiveBinaryID += "1"; }
                        else { uploadOptionCollectiveBinaryID += "0"; }
                    }
                    // use the same variable for efficiency
                    uploadOptionCollectiveBinaryID = Convert.ToString(Convert.ToInt32(uploadOptionCollectiveBinaryID, 2));
                    if (uploadOptionCollectiveBinaryID != "0") { autoUserCommand += " -o " + uploadOptionCollectiveBinaryID; }

                    // add extra CL arguments
                    if (userCommand != null) { autoUserCommand += " " + userCommand; }
                    if (autoUserCommand.Contains("-d")) { MessageBox.Show("Input Arguments: [" + autoUserCommand + "]"); }
                    // start
                    compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Job UPLOAD started" + Environment.NewLine;
                    if (uploaderBinaryLocation == "") MessageBox.Show("Uploader binary location required for upload!");
                    else
                    {
                        Process upro = new Process();
                        upro.StartInfo.FileName = uploaderBinaryLocation;
                        upro.StartInfo.Arguments = autoUserCommand;
                        upro.Start();
                        upro.WaitForExit();
                        compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Job UPLOAD finished!" + Environment.NewLine;
                    }
                }
                else { MessageBox.Show("Compile target is required for upload!"); }

            }
            else { MessageBox.Show("Source file is required for upload!"); }
        }

        private void compileButton_Click(object sender, EventArgs e)
        {
            // build command
            string autoUserCommand = "";
            string optimizationCollectiveBinaryID = "";
            if (sourceFileLocation != "")
            {
                autoUserCommand += "-f \"\"\"" + sourceFileLocation + "\"\"\"";
                if (resourceFolderLocation != "") { autoUserCommand += " -r \"\"\"" + resourceFolderLocation + "\"\"\""; }
                // serves for both target and core
                if (compileTarget != "-1")
                {
                    autoUserCommand += " -t " + compileTarget + " -c " + compileCoreTarget;
                    // build optimizationCollectiveID
                    for (int i = 0; i < optimizationConfigCheckedListBox.Items.Count; i++)
                    {
                        if (optimizationConfigCheckedListBox.GetItemChecked(i)) { optimizationCollectiveBinaryID += "1"; }
                        else { optimizationCollectiveBinaryID += "0"; }
                    }
                    // use the same variable for efficiency
                    optimizationCollectiveBinaryID = Convert.ToString(Convert.ToInt32(optimizationCollectiveBinaryID, 2));
                    if (optimizationCollectiveBinaryID != "0") { autoUserCommand += " -o " + optimizationCollectiveBinaryID; }

                    // add extra CL arguments
                    if (userCommand != null) { autoUserCommand += " " + userCommand; }
                    if (autoUserCommand.Contains("-d")) { MessageBox.Show("Input Arguments: " + autoUserCommand + "]"); }
                    // start
                    compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Job COMPILE started" + Environment.NewLine;
                    if (compilerBinaryLocation == "") MessageBox.Show("Compiler binary location required for compilation!");
                    else
                    {
                        Process cpro = new Process();
                        cpro.StartInfo.FileName = compilerBinaryLocation;
                        cpro.StartInfo.Arguments = autoUserCommand;
                        cpro.Start();
                        cpro.WaitForExit();
                        compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Job COMPILE finished!" + Environment.NewLine;
                    }
                }
                else { MessageBox.Show("Compile target is required for compilation!"); }
            }
            else { MessageBox.Show("Source file is required for compilation!"); }
        }

        private void CLArgumentsTextBox_TextChanged(object sender, EventArgs e) { userCommand = CLArgumentsTextBox.Text; }

        private void MainGUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            // close all running console apps
            foreach (Process p in Process.GetProcesses())
            {
                if (p.MainWindowTitle == "DRCHLLC Uploader" || p.MainWindowTitle == "DRCHLLC Core Compiler") p.Kill();
            }
        }

        private void loadCompilerBinaryLocationButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog
            {
                Filter = "DRCHLL-C Compiler Binary (*.exe)|*.exe",
                Title = "Select Compiler Binary"
            };
            fd.ShowDialog();
            if (fd.FileName != null)
            {
                compilerBinaryLocation = fd.FileName;
                compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Compiler binary location changed to [" + compilerBinaryLocation + "]" + Environment.NewLine;
            }
            fd.Dispose();
            compilerBinaryLocationTextBox.Text = compilerBinaryLocation;
        }

        private void uploaderBinaryLocationButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog
            {
                Filter = "DRCHLL-C Uploader Binary (*.exe)|*.exe",
                Title = "Select Uploader Binary"
            };
            fd.ShowDialog();
            if (fd.FileName != null)
            {
                uploaderBinaryLocation = fd.FileName;
                compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Uploader binary location changed to [" + uploaderBinaryLocation + "]" + Environment.NewLine;
            }
            fd.Dispose();
            uploaderBinaryLocationTextbox.Text = uploaderBinaryLocation;
        }
    }
}
