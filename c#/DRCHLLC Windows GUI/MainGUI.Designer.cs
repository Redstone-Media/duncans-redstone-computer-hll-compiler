﻿namespace DRCHLLC_Windows_GUI
{
    partial class MainGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainGUI));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.compilerTargetListBox = new System.Windows.Forms.ListBox();
            this.loadSourceFileButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.compilerOutputTextBox = new System.Windows.Forms.TextBox();
            this.compileButton = new System.Windows.Forms.Button();
            this.uploadButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.CLArgumentsTextBox = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.compileTab = new System.Windows.Forms.TabPage();
            this.selectResourceFolderButton = new System.Windows.Forms.Button();
            this.resourceFolderTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.optionsTab = new System.Windows.Forms.TabPage();
            this.binarySettingsGroupBox = new System.Windows.Forms.GroupBox();
            this.uploaderBinaryLocationButton = new System.Windows.Forms.Button();
            this.uploaderBinaryLocationTextbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.compilerBinaryLocationTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.loadCompilerBinaryLocationButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.uploadOptionsListBox = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.optimizationConfigCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.acquireOnlineCheckbox = new System.Windows.Forms.CheckBox();
            this.datapackUploadCheckbox = new System.Windows.Forms.CheckBox();
            this.autoMakeButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.compileTab.SuspendLayout();
            this.optionsTab.SuspendLayout();
            this.binarySettingsGroupBox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::DRCHLLC_Windows_GUI.Properties.Resources.photo;
            this.pictureBox1.Location = new System.Drawing.Point(9, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(119, 112);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.compilerTargetListBox);
            this.groupBox1.Location = new System.Drawing.Point(134, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(175, 112);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Compiler Target";
            // 
            // compilerTargetListBox
            // 
            this.compilerTargetListBox.FormattingEnabled = true;
            this.compilerTargetListBox.Items.AddRange(new object[] {
            "RC v3.0 [C1]",
            "RC v3.0 [C2]",
            "RC v3.0 [C3]",
            "RC v3.0 [C4]",
            "RC v4.0 [C1]",
            "RC v4.0 [C2]",
            "RC v4.0 [C3]",
            "RC v4.0 [C4]",
            "RC v5.0 [C1]",
            "RC v5.0 [C2]",
            "RC v5.0 [C3]",
            "RC v5.0 [C4]"});
            this.compilerTargetListBox.Location = new System.Drawing.Point(7, 20);
            this.compilerTargetListBox.Name = "compilerTargetListBox";
            this.compilerTargetListBox.Size = new System.Drawing.Size(162, 82);
            this.compilerTargetListBox.TabIndex = 0;
            this.compilerTargetListBox.SelectedIndexChanged += new System.EventHandler(this.compilerTargetListBox_SelectedIndexChanged);
            // 
            // loadSourceFileButton
            // 
            this.loadSourceFileButton.Location = new System.Drawing.Point(6, 124);
            this.loadSourceFileButton.Name = "loadSourceFileButton";
            this.loadSourceFileButton.Size = new System.Drawing.Size(97, 28);
            this.loadSourceFileButton.TabIndex = 2;
            this.loadSourceFileButton.Text = "Open Source File";
            this.loadSourceFileButton.UseVisualStyleBackColor = true;
            this.loadSourceFileButton.Click += new System.EventHandler(this.loadSourceFileButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.compilerOutputTextBox);
            this.groupBox2.Location = new System.Drawing.Point(313, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(229, 253);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Compiler Output";
            // 
            // compilerOutputTextBox
            // 
            this.compilerOutputTextBox.Location = new System.Drawing.Point(7, 20);
            this.compilerOutputTextBox.Multiline = true;
            this.compilerOutputTextBox.Name = "compilerOutputTextBox";
            this.compilerOutputTextBox.ReadOnly = true;
            this.compilerOutputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.compilerOutputTextBox.Size = new System.Drawing.Size(216, 227);
            this.compilerOutputTextBox.TabIndex = 0;
            this.compilerOutputTextBox.WordWrap = false;
            // 
            // compileButton
            // 
            this.compileButton.Location = new System.Drawing.Point(166, 124);
            this.compileButton.Name = "compileButton";
            this.compileButton.Size = new System.Drawing.Size(53, 28);
            this.compileButton.TabIndex = 4;
            this.compileButton.Text = "Compile";
            this.compileButton.UseVisualStyleBackColor = true;
            this.compileButton.Click += new System.EventHandler(this.compileButton_Click);
            // 
            // uploadButton
            // 
            this.uploadButton.Location = new System.Drawing.Point(109, 124);
            this.uploadButton.Name = "uploadButton";
            this.uploadButton.Size = new System.Drawing.Size(51, 28);
            this.uploadButton.TabIndex = 5;
            this.uploadButton.Text = "Upload";
            this.uploadButton.UseVisualStyleBackColor = true;
            this.uploadButton.Click += new System.EventHandler(this.uploadButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "CL Arguments:";
            // 
            // CLArgumentsTextBox
            // 
            this.CLArgumentsTextBox.Location = new System.Drawing.Point(88, 158);
            this.CLArgumentsTextBox.Name = "CLArgumentsTextBox";
            this.CLArgumentsTextBox.Size = new System.Drawing.Size(221, 20);
            this.CLArgumentsTextBox.TabIndex = 7;
            this.CLArgumentsTextBox.TextChanged += new System.EventHandler(this.CLArgumentsTextBox_TextChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.compileTab);
            this.tabControl1.Controls.Add(this.optionsTab);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(556, 291);
            this.tabControl1.TabIndex = 8;
            // 
            // compileTab
            // 
            this.compileTab.Controls.Add(this.autoMakeButton);
            this.compileTab.Controls.Add(this.selectResourceFolderButton);
            this.compileTab.Controls.Add(this.resourceFolderTextBox);
            this.compileTab.Controls.Add(this.label3);
            this.compileTab.Controls.Add(this.pictureBox1);
            this.compileTab.Controls.Add(this.compileButton);
            this.compileTab.Controls.Add(this.CLArgumentsTextBox);
            this.compileTab.Controls.Add(this.groupBox2);
            this.compileTab.Controls.Add(this.loadSourceFileButton);
            this.compileTab.Controls.Add(this.label1);
            this.compileTab.Controls.Add(this.uploadButton);
            this.compileTab.Controls.Add(this.groupBox1);
            this.compileTab.Location = new System.Drawing.Point(4, 22);
            this.compileTab.Name = "compileTab";
            this.compileTab.Padding = new System.Windows.Forms.Padding(3);
            this.compileTab.Size = new System.Drawing.Size(548, 265);
            this.compileTab.TabIndex = 0;
            this.compileTab.Text = "Main App";
            this.compileTab.UseVisualStyleBackColor = true;
            // 
            // selectResourceFolderButton
            // 
            this.selectResourceFolderButton.Location = new System.Drawing.Point(282, 184);
            this.selectResourceFolderButton.Name = "selectResourceFolderButton";
            this.selectResourceFolderButton.Size = new System.Drawing.Size(27, 20);
            this.selectResourceFolderButton.TabIndex = 10;
            this.selectResourceFolderButton.Text = "...";
            this.selectResourceFolderButton.UseVisualStyleBackColor = true;
            this.selectResourceFolderButton.Click += new System.EventHandler(this.selectResourceFolderButton_Click);
            // 
            // resourceFolderTextBox
            // 
            this.resourceFolderTextBox.Location = new System.Drawing.Point(95, 185);
            this.resourceFolderTextBox.Name = "resourceFolderTextBox";
            this.resourceFolderTextBox.ReadOnly = true;
            this.resourceFolderTextBox.Size = new System.Drawing.Size(181, 20);
            this.resourceFolderTextBox.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Resource Folder:";
            // 
            // optionsTab
            // 
            this.optionsTab.Controls.Add(this.binarySettingsGroupBox);
            this.optionsTab.Controls.Add(this.groupBox3);
            this.optionsTab.Controls.Add(this.label2);
            this.optionsTab.Controls.Add(this.groupBox5);
            this.optionsTab.Controls.Add(this.groupBox4);
            this.optionsTab.Location = new System.Drawing.Point(4, 22);
            this.optionsTab.Name = "optionsTab";
            this.optionsTab.Padding = new System.Windows.Forms.Padding(3);
            this.optionsTab.Size = new System.Drawing.Size(548, 265);
            this.optionsTab.TabIndex = 1;
            this.optionsTab.Text = "Configuration";
            this.optionsTab.UseVisualStyleBackColor = true;
            // 
            // binarySettingsGroupBox
            // 
            this.binarySettingsGroupBox.Controls.Add(this.acquireOnlineCheckbox);
            this.binarySettingsGroupBox.Controls.Add(this.uploaderBinaryLocationButton);
            this.binarySettingsGroupBox.Controls.Add(this.uploaderBinaryLocationTextbox);
            this.binarySettingsGroupBox.Controls.Add(this.label5);
            this.binarySettingsGroupBox.Controls.Add(this.compilerBinaryLocationTextBox);
            this.binarySettingsGroupBox.Controls.Add(this.label4);
            this.binarySettingsGroupBox.Controls.Add(this.loadCompilerBinaryLocationButton);
            this.binarySettingsGroupBox.Location = new System.Drawing.Point(224, 117);
            this.binarySettingsGroupBox.Name = "binarySettingsGroupBox";
            this.binarySettingsGroupBox.Size = new System.Drawing.Size(318, 140);
            this.binarySettingsGroupBox.TabIndex = 7;
            this.binarySettingsGroupBox.TabStop = false;
            this.binarySettingsGroupBox.Text = "Binaries Settings";
            // 
            // uploaderBinaryLocationButton
            // 
            this.uploaderBinaryLocationButton.Location = new System.Drawing.Point(244, 41);
            this.uploaderBinaryLocationButton.Name = "uploaderBinaryLocationButton";
            this.uploaderBinaryLocationButton.Size = new System.Drawing.Size(25, 23);
            this.uploaderBinaryLocationButton.TabIndex = 5;
            this.uploaderBinaryLocationButton.Text = "...";
            this.uploaderBinaryLocationButton.UseVisualStyleBackColor = true;
            this.uploaderBinaryLocationButton.Click += new System.EventHandler(this.uploaderBinaryLocationButton_Click);
            // 
            // uploaderBinaryLocationTextbox
            // 
            this.uploaderBinaryLocationTextbox.Location = new System.Drawing.Point(138, 43);
            this.uploaderBinaryLocationTextbox.Name = "uploaderBinaryLocationTextbox";
            this.uploaderBinaryLocationTextbox.ReadOnly = true;
            this.uploaderBinaryLocationTextbox.Size = new System.Drawing.Size(100, 20);
            this.uploaderBinaryLocationTextbox.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Uploader Binary Location:";
            // 
            // compilerBinaryLocationTextBox
            // 
            this.compilerBinaryLocationTextBox.Location = new System.Drawing.Point(138, 17);
            this.compilerBinaryLocationTextBox.Name = "compilerBinaryLocationTextBox";
            this.compilerBinaryLocationTextBox.ReadOnly = true;
            this.compilerBinaryLocationTextBox.Size = new System.Drawing.Size(100, 20);
            this.compilerBinaryLocationTextBox.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Compiler Binary Location:";
            // 
            // loadCompilerBinaryLocationButton
            // 
            this.loadCompilerBinaryLocationButton.Location = new System.Drawing.Point(244, 15);
            this.loadCompilerBinaryLocationButton.Name = "loadCompilerBinaryLocationButton";
            this.loadCompilerBinaryLocationButton.Size = new System.Drawing.Size(25, 23);
            this.loadCompilerBinaryLocationButton.TabIndex = 0;
            this.loadCompilerBinaryLocationButton.Text = "...";
            this.loadCompilerBinaryLocationButton.UseVisualStyleBackColor = true;
            this.loadCompilerBinaryLocationButton.Click += new System.EventHandler(this.loadCompilerBinaryLocationButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.datapackUploadCheckbox);
            this.groupBox3.Controls.Add(this.uploadOptionsListBox);
            this.groupBox3.Location = new System.Drawing.Point(224, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(318, 94);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Uploader Settings";
            // 
            // uploadOptionsListBox
            // 
            this.uploadOptionsListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.uploadOptionsListBox.FormattingEnabled = true;
            this.uploadOptionsListBox.Items.AddRange(new object[] {
            "Overwrite before upload",
            "Only Overwrite",
            "Fast Upload"});
            this.uploadOptionsListBox.Location = new System.Drawing.Point(7, 19);
            this.uploadOptionsListBox.Name = "uploadOptionsListBox";
            this.uploadOptionsListBox.Size = new System.Drawing.Size(140, 60);
            this.uploadOptionsListBox.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Optimization Settings";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.optimizationConfigCheckedListBox);
            this.groupBox5.Location = new System.Drawing.Point(6, 112);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(212, 145);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Optimization Configuration";
            // 
            // optimizationConfigCheckedListBox
            // 
            this.optimizationConfigCheckedListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.optimizationConfigCheckedListBox.FormattingEnabled = true;
            this.optimizationConfigCheckedListBox.Items.AddRange(new object[] {
            "Variable Clock Speed",
            "Independent Conditional Branching",
            "Redundant Code Removal",
            "Automatic Function Grouping",
            "Memory Management",
            "Instruction Merging"});
            this.optimizationConfigCheckedListBox.Location = new System.Drawing.Point(7, 20);
            this.optimizationConfigCheckedListBox.Name = "optimizationConfigCheckedListBox";
            this.optimizationConfigCheckedListBox.Size = new System.Drawing.Size(199, 120);
            this.optimizationConfigCheckedListBox.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radioButton2);
            this.groupBox4.Controls.Add(this.radioButton1);
            this.groupBox4.Location = new System.Drawing.Point(6, 31);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(212, 75);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Automatically Optimize For:";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 42);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(115, 17);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Small Program Size";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(137, 17);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Fast Program Execution";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // acquireOnlineCheckbox
            // 
            this.acquireOnlineCheckbox.AutoSize = true;
            this.acquireOnlineCheckbox.Checked = true;
            this.acquireOnlineCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.acquireOnlineCheckbox.Location = new System.Drawing.Point(9, 70);
            this.acquireOnlineCheckbox.Name = "acquireOnlineCheckbox";
            this.acquireOnlineCheckbox.Size = new System.Drawing.Size(210, 17);
            this.acquireOnlineCheckbox.TabIndex = 6;
            this.acquireOnlineCheckbox.Text = "Acquire Latest Binaries from Repository";
            this.acquireOnlineCheckbox.UseVisualStyleBackColor = true;
            // 
            // datapackUploadCheckbox
            // 
            this.datapackUploadCheckbox.AutoSize = true;
            this.datapackUploadCheckbox.Location = new System.Drawing.Point(8, 65);
            this.datapackUploadCheckbox.Name = "datapackUploadCheckbox";
            this.datapackUploadCheckbox.Size = new System.Drawing.Size(127, 17);
            this.datapackUploadCheckbox.TabIndex = 1;
            this.datapackUploadCheckbox.Text = "Upload via Datapack";
            this.datapackUploadCheckbox.UseVisualStyleBackColor = true;
            // 
            // autoMakeButton
            // 
            this.autoMakeButton.Location = new System.Drawing.Point(225, 124);
            this.autoMakeButton.Name = "autoMakeButton";
            this.autoMakeButton.Size = new System.Drawing.Size(60, 28);
            this.autoMakeButton.TabIndex = 11;
            this.autoMakeButton.Text = "EZ Make";
            this.autoMakeButton.UseVisualStyleBackColor = true;
            // 
            // MainGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 315);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainGUI";
            this.Text = "load";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainGUI_FormClosing);
            this.Load += new System.EventHandler(this.MainGUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.compileTab.ResumeLayout(false);
            this.compileTab.PerformLayout();
            this.optionsTab.ResumeLayout(false);
            this.optionsTab.PerformLayout();
            this.binarySettingsGroupBox.ResumeLayout(false);
            this.binarySettingsGroupBox.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox compilerTargetListBox;
        private System.Windows.Forms.Button loadSourceFileButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox compilerOutputTextBox;
        private System.Windows.Forms.Button compileButton;
        private System.Windows.Forms.Button uploadButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CLArgumentsTextBox;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage compileTab;
        private System.Windows.Forms.TabPage optionsTab;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckedListBox optimizationConfigCheckedListBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckedListBox uploadOptionsListBox;
        private System.Windows.Forms.TextBox resourceFolderTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button selectResourceFolderButton;
        private System.Windows.Forms.GroupBox binarySettingsGroupBox;
        private System.Windows.Forms.TextBox compilerBinaryLocationTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button loadCompilerBinaryLocationButton;
        private System.Windows.Forms.Button uploaderBinaryLocationButton;
        private System.Windows.Forms.TextBox uploaderBinaryLocationTextbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox acquireOnlineCheckbox;
        private System.Windows.Forms.CheckBox datapackUploadCheckbox;
        private System.Windows.Forms.Button autoMakeButton;
    }
}

