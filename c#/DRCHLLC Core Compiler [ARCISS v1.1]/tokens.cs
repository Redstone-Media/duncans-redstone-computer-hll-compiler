﻿using System;
using System.Linq;

namespace DRCHLLC_Core_Compiler__ARCISS_v1._1_
{
    class tokens
    {
        /*
         * This class will contain all functions and data types about tokens
         * and token manipulation for the DRCHLL-C. 
         * 
         * Created 8/19/2016 by AGuyWhoIsBored
         * 
        */

        // tokenizer will read and parse from this table AFTER parsing through the internal core tokens
        // all extension tokens (even internal ones) will need to use this table in order to be parsed properly
        // TODO: RENAME THIS - MISLEADING NAMES AS FUNCTIONS FROM THE ORIGINAL PROGRAM WILL ALSO BE PUT HERE
        public static string[] importedTokenTable = new string[0];
        public static string[] importedSyntaxTokenBindingTable = new string[0];
        public static int importedTokenTableCount = 0;
        public static int importedBindingTableCount = 0;
        public static string[] deletedImportedTokens = new string[0];

        // this is for linker specifically when removing functions that don't have target support
        // this is just a temporary workaround - MAKE SURE TO FULLY SOLVE THIS PROBLEM!
        //public static string[] removedTokenTable = new string[0];

        public static int addNewFunction = 0; // toggled when we run into func token
                                              // when we run the first time, addNewFunction = 1
                                              // if it's greater than 1, reset to 0

        // core types of ARCISS language v1.1
        // all imported tokens (even internal extensions) will follow [className/extensionName][./_]types[] syntax
        // token ID's (#'s) have to follow 0< rule
        // see below for example
        // they will then be compiled into one big token list to be referenced from
        // compiler will first read from core types array (below), and then read from importedTokenTable

        // consider making types an enum?
        public static string[] types = new string[45]
            { /*00-04*/ "invalid", "nulltype", "data_int", "data_hex", "variable",
              /*05-11*/ "operator_plus", "operator_minus", "operator_equals", "operator_increment", "operator_decrement", "operator_plusequals", "operator_minusequals",
              /*12-19*/ "operator_cond_greaterthan", "operator_cond_lessthan", "operator_cond_equals", "operator_cond_and", "operator_cond_or", "operator_cond_notequals", "operator_cond_gequals", "operator_cond_lequals",
              /*20-22*/ "operator_bitwise_not", "operator_bitwise_rshift", "operator_bitwise_lshift",
              /*23-29*/ "identifier_if", "identifier_else", "identifier_jumploc", "identifier_new", "identifier_eof", "identifier_func", "identifier_local", // jumploc and EOF aren't in tokenizing methods - only conversion as they are special cases
              /*30-34*/ "end_of_line", "grouper_openparentheses", "grouper_closeparentheses", "grouper_openbracket", "grouper_closebracket",
              /*35-40*/ "func_exit", "func_output", "func_input", "func_jump", "func_sleep", "func_return", // return function is really not a function, but grouped here
              /*41-42*/ "hcond_overflow", "hcond_underflow", 
              /*43-44*/ "ext_target", "ext_requires" // extension-specific tokens
            };

        public static int totalTokenCount = types.Length; // count native tokens first

        // use arrays for ID's
        public static int Tokenize(string stringToToken, int sender)
        {
            // sender 0 - compiler
            // sender 1 - importer

            if (addNewFunction != 0) addNewFunction++;
            if (addNewFunction > 2) addNewFunction = 0;

            // try core token types first
            // number
            try
            { Convert.ToInt32(stringToToken); return 2; }
            catch (FormatException)
            {
                // hex number
                if (stringToToken.Contains("0x")) { return 3; }
                // general operators
                switch (stringToToken)
                {
                    case "+": return 5;
                    case "-": return 6;
                    case "=": return 7;
                    case "++": return 8;
                    case "--": return 9;
                    case "+=": return 10;
                    case "-=": return 11;
                }
                // conditional operators
                switch (stringToToken)
                {
                    case ">": return 12;
                    case "<": return 13;
                    case "==": return 14;
                    case "&&": return 15;
                    case "||": return 16;
                    case "!=": return 17;
                    case ">=": return 18;
                    case "<=": return 19;
                }
                // bitwise operators (common)
                switch (stringToToken)
                {
                    case "!!": return 20;
                    case ">>": return 21;
                    case "<<": return 22;
                }
                // identifiers
                // id_jumploc is special token case and handled separately (in LexData)
                switch (stringToToken)
                {
                    case "if": return 23;
                    case "else": return 24;
                    case "new": return 26;
                    // special case - import custom token for next variable
                    case "func": addNewFunction++; return 28; 
                    case "local": return 29;
                }
                // semicolon
                // id_eof is special token case and handled separately (in LexData)
                switch (stringToToken)
                { case ";": return 30; }
                // data groupers
                switch (stringToToken)
                {
                    case "(": return 31;
                    case ")": return 32;
                    case "{": return 33;
                    case "}": return 34;
                }
                // functions
                switch (stringToToken)
                {
                    // go through all available functions
                    case "exit": return 35;
                    case "output": return 36;
                    case "input": return 37;
                    case "jump": return 38;
                    case "sleep": return 39;
                    case "return": return 40;
                }

                // hardware conditions
                switch (stringToToken)
                {
                    case "SHIFT_OVERFLOW": return 41;
                    case "SHIFT_UNDERFLOW": return 42;
                }

                //extension stuff
                if (sender == 1)
                {
                    switch (stringToToken)
                    {
                        case "target": return 43;
                        case "requires": return 44;
                    }
                }

                // imported token check [to work on later]
                // put imported token check code here
                // custom functions also run through here
                for (int i = 0; i < importedTokenTable.Length; i++)
                { if (stringToToken == importedTokenTable[i]) { return types.Length + i + 1; } }

                // check if input passed into tokenizer is empty
                if (string.IsNullOrWhiteSpace(stringToToken) || stringToToken.Length == 0)
                { return 0; /* add discard marker to remove later */ }

                // check to see if we're making a new custom function
                if (addNewFunction >= 1)
                {   // check for already existing tokens should be in addImportedToken
                    // import this variable as a token
                    addImportedToken(stringToToken);
                    logging.logDebug("Custom function '" + stringToToken + "' detected! Tokenizing as a new custom token ...", logging.loggingSenderID.tokenizer);
                    addNewFunction = 0;
                    return types.Length + importedTokenTable.Length;
                }

                // assume everything else is a variable
                // run variable check syntax though
                // syntax: alphanumeric
                if (!stringToToken.All(char.IsLetterOrDigit))
                {
                    // check for registers
                    if (stringToToken == "%gt_"
                        || stringToToken == "%spd"
                        || stringToToken == "%dp_"
                        || stringToToken == "%ui_"
                        || stringToToken == "%rng"
                        || stringToToken == "%dec"
                        || stringToToken == "%raw"
                        || stringToToken == "%out"
                        || stringToToken == "%stk"
                        || stringToToken == "%gex")
                    { logging.logDebug("Token is special variable [reg]", logging.loggingSenderID.tokenizer); return 4; }
                    else
                    {
                        // return invalid
                        logging.logWarning("token '" + stringToToken + "' doesn't match variable syntax! Setting as invalid token! The program will most likely not compile because of this!", logging.loggingSenderID.tokenizer);
                        logging.logWarning("Did you forget to import external libraries?", logging.loggingSenderID.tokenizer);
                        return 0;
                    }
                }

                else { return 4; }
            }
        }

        public static void addImportedToken(string importedToken)
        {
            // first check if value already exists in arrays - if it does, throw error
            // if it doesn't add into table and increment tokenCount

            if (importedTokenTable.Contains(importedToken)) throw new ArgumentException("token already exists! fix this internally!");
            Array.Resize(ref importedTokenTable, importedTokenTable.Length + 1);
            importedTokenTable[importedTokenTableCount] = importedToken;
            importedTokenTableCount++;
        }

        public static void removeImportedToken(string tokenID)
        {
            //importedTokenTable = functionLibrary.removeElementFromArray(importedTokenTable, Array.IndexOf(importedTokenTable, tokenID));
            //importedTokenTableCount--;
            Array.Resize(ref deletedImportedTokens, deletedImportedTokens.Length + 1);
            deletedImportedTokens[deletedImportedTokens.Length -  1] = tokenID;
        }

        public static void bindSyntax(string syntaxTableID)
        {
            // works in tandem with tokens
            // to bind multiple syntaxes, use "-" (see rc40e class)
            // WILL BIND BASED OFF OF ARRAY INDEXES, SO ORDER MATTERS!
            Array.Resize(ref importedSyntaxTokenBindingTable, importedSyntaxTokenBindingTable.Length + 1);
            importedSyntaxTokenBindingTable[importedBindingTableCount] = syntaxTableID;
            importedBindingTableCount++;
        }

        #region conversion

        // str > int
        public static int convertInt(string token)
        {
            int converted = -1;

            try
            {
                // haven't tested yet
                converted = Convert.ToInt32(token);
            }
            catch (Exception)
            {
                switch (token)
                {
                    // human readable
                    case "invalid":
                        converted = 0;
                        break;
                    case "null":
                        converted = 1;
                        break;
                    case "data_int":
                        converted = 2;
                        break;
                    case "data_hex":
                        converted = 3;
                        break;
                    case "variable":
                        converted = 4;
                        break;
                    case "operator_plus":
                        converted = 5;
                        break;
                    case "operator_minus":
                        converted = 6;
                        break;
                    case "operator_equals":
                        converted = 7;
                        break;
                    case "operator_increment":
                        converted = 8;
                        break;
                    case "operator_decrement":
                        converted = 9;
                        break;
                    case "operator_plusequals":
                        converted = 10;
                        break;
                    case "operator_minusequals":
                        converted = 11;
                        break;
                    case "operator_cond_greaterthan":
                        converted = 12;
                        break;
                    case "operator_cond_lessthan":
                        converted = 13;
                        break;
                    case "operator_cond_equals":
                        converted = 14;
                        break;
                    case "operator_cond_and":
                        converted = 15;
                        break;
                    case "operator_cond_or":
                        converted = 16;
                        break;
                    case "operator_cond_notequals":
                        converted = 17;
                        break;
                    case "operator_cond_gequals":
                        converted = 18;
                        break;
                    case "operator_cond_lequals":
                        converted = 19;
                        break;
                    case "operator_bitwise_not":
                        converted = 20;
                        break;
                    case "operator_bitwise_rshift":
                        converted = 21;
                        break;
                    case "operator_bitwise_lshift":
                        converted = 22;
                        break;
                    case "identifier_if":
                        converted = 23;
                        break;
                    case "identifier_else":
                        converted = 24;
                        break;
                    case "identifier_jumploc":
                        converted = 25;
                        break;
                    case "identifier_new":
                        converted = 26;
                        break;
                    case "identifier_eof":
                        converted = 27;
                        break;
                    case "identifier_func":
                        converted = 28;
                        break;
                    case "identifier_local":
                        converted = 29;
                        break;
                    case "end_of_line":
                        converted = 30;
                        break;
                    case "grouper_openparentheses":
                        converted = 31;
                        break;
                    case "grouper_closeparentheses":
                        converted = 32;
                        break;
                    case "grouper_openbracket":
                        converted = 33;
                        break;
                    case "grouper_closebracket":
                        converted = 34;
                        break;
                    case "func_exit":
                        converted = 35;
                        break;
                    case "func_output":
                        converted = 36;
                        break;
                    case "func_input":
                        converted = 37;
                        break;
                    case "func_jump":
                        converted = 38;
                        break;
                    case "func_sleep":
                        converted = 39;
                        break;
                    case "func_return":
                        converted = 40;
                        break;
                    case "hcond_overflow":
                        converted = 41;
                        break;
                    case "hcond_underflow":
                        converted = 42;
                        break;
                    case "ext_target":
                        converted = 43;
                        break;
                    case "ext_requires":
                        converted = 44;
                        break;
                }
                // all imported tokens
                for (int i = 0; i < importedTokenTable.Length; i++)
                {
                    if (token == importedTokenTable[i]) { return types.Length + (i + 1); }
                }
            }
            return converted;
        }
        // int > str
        public static string convertStr(int token, bool humanReadable = false)
        {
            string converted = "";
            if (humanReadable)
            {
                switch (token)
                {
                    case 0:
                        converted = "invalid";
                        break;
                    case 1:
                        converted = "null";
                        break;
                    case 2:
                        converted = "data_int";
                        break;
                    case 3:
                        converted = "data_hex";
                        break;
                    case 4:
                        converted = "variable";
                        break;
                    case 5:
                        converted = "operator_plus";
                        break;
                    case 6:
                        converted = "operator_minus";
                        break;
                    case 7:
                        converted = "operator_equals";
                        break;
                    case 8:
                        converted = "operator_increment";
                        break;
                    case 9:
                        converted = "operator_decrement";
                        break;
                    case 10:
                        converted = "operator_plusequals";
                        break;
                    case 11:
                        converted = "operator_minusequals";
                        break;
                    case 12:
                        converted = "operator_cond_greaterthan";
                        break;
                    case 13:
                        converted = "operator_cond_lessthan";
                        break;
                    case 14:
                        converted = "operator_cond_equals";
                        break;
                    case 15:
                        converted = "operator_cond_and";
                        break;
                    case 16:
                        converted = "operator_cond_or";
                        break;
                    case 17:
                        converted = "operator_cond_notequals";
                        break;
                    case 18:
                        converted = "operator_cond_gequals";
                        break;
                    case 19:
                        converted = "operator_cond_lequals";
                        break;
                    case 20:
                        converted = "operator_bitwise_not";
                        break;
                    case 21:
                        converted = "operator_bitwise_rshift";
                        break;
                    case 22:
                        converted = "operator_bitwise_lshift";
                        break;
                    case 23:
                        converted = "identifier_if";
                        break;
                    case 24:
                        converted = "identifier_else";
                        break;
                    case 25:
                        converted = "identifier_jumploc";
                        break;
                    case 26:
                        converted = "identifier_new";
                        break;
                    case 27:
                        converted = "identifier_eof";
                        break;
                    case 28:
                        converted = "identifier_func";
                        break;
                    case 29:
                        converted = "identifier_local";
                        break;
                    case 30:
                        converted = "end_of_line";
                        break;
                    case 31:
                        converted = "grouper_openparentheses";
                        break;
                    case 32:
                        converted = "grouper_closeparentheses";
                        break;
                    case 33:
                        converted = "grouper_openbracket";
                        break;
                    case 34:
                        converted = "grouper_closebracket";
                        break;
                    case 35:
                        converted = "func_exit";
                        break;
                    case 36:
                        converted = "func_output";
                        break;
                    case 37:
                        converted = "func_input";
                        break;
                    case 38:
                        converted = "func_jump";
                        break;
                    case 39:
                        converted = "func_sleep";
                        break;
                    case 40:
                        converted = "func_return";
                        break;
                    case 41:
                        converted = "hcond_overflow";
                        break;
                    case 42:
                        converted = "hcond_underflow";
                        break;
                    case 43:
                        converted = "ext_target";
                        break;
                    case 44:
                        converted = "ext_requires";
                        break;
                }
                // for imported tokens
                try { converted = importedTokenTable.ElementAt(token - (types.Length + 1)); }
                catch (Exception) { /* do nothing */}
            }
            else { converted = Convert.ToString(token); }
            return converted;
        }

        #endregion
    }
}
