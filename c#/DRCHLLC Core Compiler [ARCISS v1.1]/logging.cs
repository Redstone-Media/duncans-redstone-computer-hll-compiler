﻿using System;
using System.Diagnostics;

namespace DRCHLLC_Core_Compiler__ARCISS_v1._1_
{
    class logging
    {
        /*
         * This class will contain all functions and information needed to enable
         * versatile logging methods and capabilities with the compiler and debug 
         * information provided by the compiler
         * 
         * Created 8/20/2016 by AGuyWhoIsBored
         * 
         */

        public static string debugText = "";
        private static Stopwatch compileTimeMS = new Stopwatch();
        public enum loggingSenderID
        {
            // different IDs from senders so we can make debug messages more streamlined
            // one for each class
            none = 0,
            syntax = 1,
            compiler = 2,
            importer = 3,
            logging = 4,
            machineCodeParser = 5,
            internalRC40E = 6,
            tokenizer = 7,
            uploader = 8,
            assemblyParser = 9,
            optimizer = 10,
            internalRC50E = 11,
            linker = 12
        }

        ///<summary>
        ///<para>this will push info to our debugger window / array</para>
        ///will also push to main output log if needed
        /// </summary>
        public static void logDebug(string info, loggingSenderID senderID, bool showInMain = false)
        {
            UpdateDebugger(sIDToString(senderID) + info);
            if (showInMain && !Program.debug) { Console.WriteLine("[Info] " + sIDToString(senderID) + info); }
        }
        ///<summary>
        ///<para>this will push warning messages to debugger window / array and main output log</para>
        /// </summary>
        public static void logWarning(string info, loggingSenderID senderID)
        {
            UpdateDebugger(sIDToString(senderID) + info);
            Console.WriteLine("[WARNING] " + sIDToString(senderID) + info);
        }
        /// <summary>
        /// <para>this will push a fatal error message to debug window / array</para>
        /// <para>will also push simplified fatal error message to main output log </para>
        /// this WILL NOT handle thread / process control!
        /// </summary>
        /// <param name="errorCode">Error code to display</param>
        /// <param name="additionalInfo">Additional info to pass to debugger</param>
        /// <param name="senderID">ID of sender who called method</param>
        public static void logError(string errorCode, string additionalInfo, loggingSenderID senderID)
        {
            #region Error Code Dictionary
            /* 00.0 - target file not a valid file
             * 01.0 - unbalanced curly brackets
             * 02.0 - unknown function found
             * 03.0 - semicolon required after new variable declaration
             * 03.1 - semicolon required after variable manipulation
             * 03.2 - semicolon required after function
             * 03.3 - semicolon required after variable data groupings
             * 04.0 - number required after equals sign when creating new variable // CHANGE THIS IN FUTURE LANGUAGE REVISION
             * 05.0 - equals sign required after a variable
             * 06.0 - variable name required after NEW keyword
             * 07.0 - equals sign required after variable to be able to manipulate it
             * 08.0 - double parentheses required when calling exit function
             * 08.1 - double parentheses required when calling flush function
             * 08.2 - double parentheses required when calling input function
             * 08.3 - double parentheses required when calling output function
             * 08.4 - double parentheses required when calling jump function
             * 08.5 - double parentheses required when calling all functions
             * 09.0 - invalid value for flush function argument [registerID]
             * 09.1 - invalid value for output function argument [portID]
             * 09.2 - invalid type for output function argument [var1]
             * 09.3 - invalid value for input function argument [portID]
             * 09.4 - invalid type for input function argument [var1]
             * 09.5 - invalid type for input function argument [var1]
             * 09.6 - invalid value for input function argument [portID]
             * 09.7 - invalid argument type for jump function argument [location]
             * 09.8 - invalid value for function argument
             * 10.0 - invalid argument type
             * 11.0 - input(portID_userinput) overload of function is not supported in regular code (only if statements)
             * 12.0 - double parentheses required when calling if statement 
             * 12.1 - too many data groupings in if statement
             * 12.2 - double parentheses needed after == / < / > if operators
             * 12.3 - cannot have more than one if operator in if statement
             * 13.0 - unrecognized cause after causes operator
             * 14.0 - cannot have both if operators and causes statements in if statement
             * 15.0 - unbalanced parentheses in data grouped function
             * 16.0 - unrecognized check value in if statement
             * 17.0 - operator(s) required when using if statements
             * 18.0 - wrong operator type used in if statement
             * 19.0 - number larger than target capacity (when num > 255)
             * 20.0 - variable used before it was declared
             */
            #endregion

            string errorStringFull = "";
            string errorStringSimple = "";
            string errorString = "";

            // sent from syntax class
            if (senderID == loggingSenderID.syntax)
            {
                // add on syntax info
                errorString += "[SYNTAX ERROR] ";

                // update error stats
                syntax.errorCount++;
            }

            #region acquiring error code strings
            switch (errorCode)
            {
                case "0.0":
                    errorString += "[Code 0.0]: The target file is not a Redstone Computer target file!\n";
                    break;
                case "1.0":
                    errorString += "[Code 1.0]: Unbalanced curly brackets in program!\n";
                    break;
                case "2.0":
                    errorString += "[Code 2.0]: Unknown function found! Are you sure you've imported the right extensions?\n";
                    break;
                case "3.0":
                    errorString += "[Code 3.0]: Semicolon required after new variable declaration!\n";
                    break;
                case "3.1":
                    errorString += "[Code 3.1]: Semicolon required after variable manipulation!\n";
                    break;
                case "3.2":
                    errorString += "[Code 3.2]: Semicolon required after function!\n";
                    break;
                case "3.3":
                    errorString += "[Code 3.3]: Semicolon required after variable data groupings!\n";
                    break;
                case "4.0":
                    errorString += "[Code 4.0]: Number required after equals sign when creating new variable!\n";
                    break;
                case "5.0":
                    errorString += "[Code 5.0]: Equals sign required after a variable!\n";
                    break;
                case "6.0":
                    errorString += "[Code 6.0]: Variable name required after NEW keyword!\n";
                    break;
                case "7.0":
                    errorString += "[Code 7.0]: Equals sign required after variable to be able to manipulate it!\n";
                    break;
                case "8.0":
                    errorString += "[Code 8.0]: Double parentheses required when calling exit function!\n";
                    break;
                case "8.1":
                    errorString += "[Code 8.1]: Double parentheses required when calling flush function!\n";
                    break;
                case "8.2":
                    errorString += "[Code 8.2]: Double parentheses required when calling output function!\n";
                    break;
                case "8.3":
                    errorString += "[Code 8.3]: Double parentheses required when calling input function!\n";
                    break;
                case "8.4":
                    errorString += "[Code 8.4]: Double parentheses required when calling jump function!\n";
                    break;
                case "8.5":
                    errorString += "[Code 8.5]: Double parentheses required when calling all functions!\n";
                    break;
                case "9.0":
                    errorString += "[Code 9.0]: Invalid value for flush function argument [registerID]!\n";
                    break;
                case "9.1":
                    errorString += "[Code 9.1]: Invalid value for output function argument [portID]!\n";
                    break;
                case "9.2":
                    errorString += "[Code 9.2]: Invalid type for output function argument [var1]!\n";
                    break;
                case "9.3":
                    errorString += "[Code 9.3]: Invalid value for input function argument [portID]!\n";
                    break;
                case "9.4":
                    errorString += "[Code 9.4]: Invalid type for input function argument [var1]!\n";
                    break;
                case "9.5":
                    errorString += "[Code 9.5]: Invalid type for input function argument [var1]!\n";
                    break;
                case "9.6":
                    errorString += "[Code 9.6]: Invalid value for input function argument [portID]!\n";
                    break;
                case "9.7":
                    errorString += "[Code 9.7]: Invalid argument type for jump function argument [location]!\n";
                    break;
                case "9.8":
                    errorString += "[Code 9.8]: Invalid value for function argument!\n";
                    break;
                case "10.0":
                    errorString += "[Code 10.0]: Invalid argument type!\n";
                    break;
                case "11.0":
                    errorString += "[Code 11.0]: Input(portID_userinput) overload of function is not supported in regular code (only if statements)!\n";
                    break;
                case "12.0":
                    errorString += "[Code 12.0]: Double parentheses required when calling if statement!\n";
                    break;
                case "12.1":
                    errorString += "[Code 12.1]: Too many data groupings in if statement!\n";
                    break;
                case "12.2":
                    errorString += "[Code 12.2]: Double parentheses needed after == / < / > if operators!\n";
                    break;
                case "12.3":
                    errorString += "[Code 12.3]: Cannot have more than one if operator in if statement!\n";
                    break;
                case "13.0":
                    errorString += "[Code 13.0]: Unrecognized cause after causes operator!\n";
                    break;
                case "14.0":
                    errorString += "[Code 14.0]: Cannot have both if operators and causes keyword in if statement!\n";
                    break;
                case "15.0":
                    errorString += "[Code 15.0]: Unbalanced parentheses in data grouped function!\n";
                    break;
                case "16.0":
                    errorString += "[Code 16.0]: Unrecognized check value in if statement!\n";
                    break;
                case "17.0":
                    errorString += "[Code 17.0]: Operator(s) required when using if statements!\n";
                    break;
                case "18.0":
                    errorString += "[Code 18.0]: Wrong operator type used in if statement!\n";
                    break;
                case "19.0":
                    errorString += "[Code 19.0]: Number larger than target capacity!\n";
                    break;
                case "20.0":
                    errorString += "[Code 20.0]: Variable used before it was declared!\n";
                    break;
                default:
                    errorString += "[INVALID CODE!]: You shouldn't be seeing this message! Please submit a bug report if this shows up! [errorcode passed: '" + errorCode + "']\n";
                    break;
            }
            #endregion

            if (senderID == loggingSenderID.syntax)
            {
                // add on more syntax info
                // for importer syntax checking support
                if (syntax.tempTokensArray.Length != 0) { errorString += "[At line " + (Convert.ToInt32(syntax.tempLineOfCodeValueArray[syntax.currentTokenIndex]) - 1).ToString() + "] "; }
                else { errorString += "[At line " + (Convert.ToInt32(compiler.tokenLineOfCodeValueArray[syntax.currentTokenIndex]) - 1).ToString() + "] "; }
            }

            errorStringSimple = errorString;
            errorStringFull = errorString;

            if (senderID == loggingSenderID.syntax)
            {
                // for debug
                // grab line of code from line of code value
                try
                {
                    if (syntax.tempTokensArray.Length != 0)
                    {
                        // this is erroring out when we have linked programs because errored lines of code don't exist in sourceCodeBuffer! 
                        errorStringFull += "\"" + compiler.sourceCodeBuffer[Convert.ToInt32(syntax.tempLineOfCodeValueArray[syntax.currentTokenIndex]) - 1] + "\"\n"; // experiment with templineofcode - X 
                    }
                    else
                    { errorStringFull += "\"" + compiler.sourceCodeBuffer[Convert.ToInt32(compiler.tokenLineOfCodeValueArray[syntax.currentTokenIndex]) - 2] + "\"\n"; }
                }
                catch (Exception)
                { /*do nothing*/ }
                errorStringFull += "[Additional Info]: " + additionalInfo;
            }

            UpdateDebugger(errorStringFull);
            Console.WriteLine(errorStringSimple);

        }
        static string sIDToString(loggingSenderID senderID)
        {
            string value = null;
            if (senderID == loggingSenderID.none) { value = ""; }
            else if (senderID == loggingSenderID.syntax) { value = "[Syntax]: "; }
            else if (senderID == loggingSenderID.compiler) { value = "[Compiler]: "; }
            else if (senderID == loggingSenderID.importer) { value = "[Importer]: "; }
            else if (senderID == loggingSenderID.logging) { value = "[Logger]: "; }
            else if (senderID == loggingSenderID.machineCodeParser) { value = "[Machine Code Parser]: "; }
            else if (senderID == loggingSenderID.internalRC40E) { value = "[Internal RC40E]: "; }
            else if (senderID == loggingSenderID.tokenizer) { value = "[Tokenizer]: "; }
            else if (senderID == loggingSenderID.uploader) { value = "[Uploader]: "; }
            else if (senderID == loggingSenderID.assemblyParser) { value = "[Assembly Parser]: "; }
            else if (senderID == loggingSenderID.optimizer) { value = "[Optimizer]: "; }
            else if (senderID == loggingSenderID.internalRC50E) { value = "[Internal RC50E]: "; }
            else if (senderID == loggingSenderID.linker) { value = "[Linker]: "; }
            return value;
        }
        private static void UpdateDebugger(string text)
        {
            if (Program.debug) { Console.WriteLine("[" + compileTimeMS.ElapsedMilliseconds.ToString() + "]: " + text); }
            debugText += "[" + compileTimeMS.ElapsedMilliseconds.ToString() + "]: " + text + Environment.NewLine;
        }
        public static void startStopwatch() { compileTimeMS.Start(); }
        public static void resetStopwatch() { compileTimeMS.Reset(); }
        public static long getStopwatchTimeInMS() { return (compileTimeMS.ElapsedMilliseconds); }
    }
}
