﻿using System;
using System.IO;

namespace DRCHLLC_Core_Compiler__ARCISS_v1._1_
{
    class Program
    {
        /*Duncan's Redstone Computer HLL Compiler [DRCHLL-C] [ARCISS v1.1]
         * -------------------------

          Created 4/21/2018. 
          Copyright © AGuyWhoIsBored. All rights reserved.

          [ARCISS v1.0]
          Project started on 6/21/15
          Build v1.0a pushed 7/22/16
          Build v1.1a pushed 7/25/16
          Build v1.2a pushed 7/25/16
          Build v1.3a pushed 7/31/16
          Build v1.4a pushed 12/26/16
          Build v1.5a pushed 8/8/17
          See ARCISS v1.0 Compiler code for more changelog info
          [ARCISS v1.1]
          Development started 4/21/2018
          Build v1.6a pushed 

        * --------------------------

          Current known bugs:
            -When doing [fast] upload, the inventory is somehow opened and that makes the uploading of the program 
                less accurate
                [PARTIAL FIX]: Had to add 200ms of delay after [compiler]: uploading program to core ... statement
                [added to repo issue tracker]
                [see repo issue tracker for solutions]
                [FIX] rebuilt uploader from scratch using datapacks, which ensure 100% integrity of program! User will need 1.13+, however...
            -[HARDWARE] RC3.0 bsr/bsl hardware inefficient / outdated / broken / all three of those
            -[HARDWARE] There is an architectual limit on the RC4.0 using BSR / BSL functions:
                bsl can ONLY be accessed on cache A / bsr can ONLY be accessed on cache B
                Means we CANNOT bsl immediate values, as immediate values are only piped into cache B

        * --------------------------

          Current To-do / want-to-do list for Duncan's Redstone Computer HLL Compiler / ARCISS:

          Short-term:
            -Make sure compiler generated stuff (i.e. opcodes, compiler generated variables, etc.) DOES NOT clash with user-generated variables and definitions!
            -Finish initial function call support (this will most likely not involve variable scope)
            -Fix ffjump bug
            -Implement all caching for RC4.0 target (machine code)
                -criteria for RC4.0 caching
                -if two of the same variables have any ALU manipulation done to them, write var 1 to A and var 2 to B
                -if any operation of [sub globalvar localvar] (globalvar - localvar), write localvar to B
                    -if any sub case of this does not match initial one, set localvar to globalvar
                -

            
          Long-term:
            -Actually introduce ARCISS grammar and an actual AST into DRCHLLC
            -Introduce scope into language (v1.2?)
            -Optimize all functions that compiler uses
            -Rewrite entire compiler in c++

          ARCISS v1.1 planned changes
            [XX]Make ARCIS Spec MUCH more flexible (in regards to groupings, parentheses, etc.)
                Make it so that we can write code 'naturally' instead of using mass parentheses
                    var = var1 + var2; 
                    var = [function]; (ONLY ONE FUNCTION)
            [NO]Implement PEMDAS into langauge & specific groupings
                var = (var1 + var2) - var3 + (var4 + var5);
                [shouldn't be necessary if programmer arranges their operations correctly]
            [NO]When functions require numbers, make it so that we can use variables, Functions (that return integers), OR hex data! (as hex data is just encoded binary, and thus numbers)
                [not really necessary]
            [XX]Add function support
                Adding function support will enable support for importing extensions and libraries!
                Simply just move code within function(s) to the end of the program (or user specified location in advanced options) and have references to jump to that code
			[XX]Add extension support
				This is just importing functions from libraries!
            [XX]Add logical operators to language (see AND and OR)
            [XX]Add ALL bitwise operators to language
            [XX]Add multi-shifting for bsl and bsr
            [XX]Add AND (&&) statements to language
            [XX]Add OR (||) statements to language
            [XX]Add <= and >= operators to language
            [XX]Add += and -= operators to language
            [XX]Add != operator to language
                    This one is easy - just switch the fjump and tjump values
            [XX]Add support for [new var = old var] in language
                    Can perform new var1 = 5; new var2 = 0; var1 = var2;
            [NO]Add boolean data type into language 
                This will be converted to "0" and "1" in the machine code but for HLL add boolean
                [potentially implement in ARCISS v1.2?]
            [NO]Add support for defining multiple variables at one time in language
                i.e. "new var1, var2, var3 = 0;"
                [not really necessary, as the assembly code generated will be the same if we didn't have this anyways]
                people can simply use "new var1 = 0; new var2 = 0; ..."
            [XX]Implement [extensionID].[functionName] syntax
                This will keep our code a lot more organized!
			[XX]Implement sleep(cycle) function
				This function will essentially pass NOP opcodes into program for a certain amount of cycles
            [NO]When given the option to use a variable, enable the option to use a function and use the output of that function for the variable
                WILL HAVE TO ENABLE "RECURSIVENESS" - i.e. output(0, multiply(3, divide(20, 5)) - as an example
                can implement onion model to get this done!
                [potentially implement in ARCISS v1.2?]

          Syntax checker
            [  ]Have syntax for most variable-operator functions be very flexible
            [NO]Add syntax check for if(((functions)++) ...) // if(((functions)--) ...) // if(((functions)>>) ...) // if(((functions)<<) ...) // if(((functions) !!) ...) [done]
                Currently throws 'Unrecognized check value in if statement!'
                Currently throws 'Unbalanced parentheses in data grouped function!'
            [  ]Make syntax checker more flexible when using data-grouped values to change variables
                In ARCISS v1.1, variable modifications HAVE to be on the right-hand side!
            [  ]Make syntax checker invalidate if statement grouping syntax if there is one operator and two parentheses groups (CHECK THIS)
            [  ]Add option in compiler program to only report errors instead of errors and warnings?
            [  ]Add error/warning for brackets that don't have functions / if statements attached to them?
            [  ]Check to see if functions are being used on the right-hand-side of the equals sign - essentially dynamically allocate need to check for semicolons!

          Optimizations
            Optimizations to implement:
                [XX]Duplicate variable /instruction setting (i.e. 2 lines of var1 = 0) [redundant code removal]
                    -If compiler detects two or more duplicate variable / instructions right after another (i.e. var1 = 0; var1 = 0; var1 = 0), compiler will delete
                        all but one of those lines of code
                [??]Variable clock speed implementation
                    -Use clock speed dictionary to determine how fast each function can run, and then dynamically group each part of program and set clock speed
                        accordingly (only available on RC4.0)
                [XX]Independent Conditional Branching
                    -If line of code has a conditional branch attached to instruction, separate the two and have one line of code for instruction and one dedicated to
                        conditional branch. This (in theory) should make program A LOT FASTER, but will increase program line count. 
                [XX]Removal of unreachable code [redundant code removal]
                    -If any code is detected at unreachable points (i.e. after exit(); function, or like 30 lines after entire program), remove those lines of code
                [??]Grouping of same block of code as function [automtaic function grouping]
                    -If we run the same block of code multiple times, only actually implement block of code for it once and have all instances of it point to that one
                        block of code
                [XX]Memory location optimizations [memory optimizations]
                    -In the RC4.0, if program variables can be stored in cache, enable it so that program uses cache instead of RAM for faster performance.
                    -In program, try to use all memory addresses closer to front end of RAM (so 1, 2, 3, etc ...) instead of scattered memory locations, as we can read
                        faster from addresses closer to front end of RAM / memory location. This should already have been performed by the MAT generator
                [XX]Memory Cache Optimization [memory optimizations]
                    -If RC4.0 or RC5.0 is selected as compile target, attempt to use cache for memory instead of RAM! This will improve performance significantly!
                [XX]Instruction Combination Optimization
                    -Some instructions (on all targets) can be combined w/ one another, to shrink program size! However, this may (or may not) hinder performance!
                [NO]Conditional Determination-Result Optimization [automatic function grouping optimizations]
                    -This is poorly named!
                    -If we have a condition testing for something and the result if it's true does that same thing, do that thing only once before test and save result
                [XX]Removal of var = 0 malloc instructions [redundant code removal]
                    -This optimization will remove any malloc statement setting the variable to 0. 
                [  ]Jump Consolidation Optimization [redundant code removal]
                    -This optimization will remove line of code 'y' and have fjump y on test loop back onto itself:
                        [1] if(condition) tjump x fjump y
                        [y] fjump 1
                [  ]Peephole optimizations
                    -This set of optimizations will optimize specific sets and situations of code!


        */

        // the actual compiler code will be here in this program
        // will ONLY BE COMPILER CODE! Uploader will be another program!

        // this program will include things such as:
        // - parsing flags
        // - loading resources
        // - etc...
        // - then jump straight into assigned compile job
        // will write results in temp file
        // args:
        // [program] [-f "file"] -t [targetID] [-o optimizationCollectiveID] [-c coreID] [-d (toggle debug)] [-r [resourceFolder]] [-m (toggle ASMC)] [-dd (toggle [more] verbose debug)] [-s (perform syntax checking)] [-ao (compile only to ARCISS-3L)]
        // version stuff

        public static string compilerVersion = "1.6a";        // version of compiler program
        public static string compilerLanguageVersion = "1.1"; // version of ARCISS language compiler supports

        // arguments and settings
        public static string compileTarget = "-1";
        public static string optimizationCollectiveID = "-1";
        public static string compileCoreTarget = "-1";
        public static string sourceFileLocation = "";
        public static string resourceFolderLocation = "";
        public static bool debug = false;
        public static bool moreVerboseDebug = false; // eventual support for multiple levels of debugging
                                                     // performance improvements noted w/ less debugging
                                                     // "test-active-dev.pb" w/ -d + moreVerbose = 9.657s
                                                     // "test-active-dev.pb" w/ -d = 9.614
                                                     // "test-active-dev.pb" w/ no debug = 9.567
        public static bool autoSplitMultiCore = false; // this feature will detect if the program written is longer than a target's line of code capacity per core. 
                                                       // If it is, then the program will split up the code into separate programs to be uploaded onto separate cores.
                                                       // however, if there is core control code included in the program (RC5.0), ASMC may not work correctly due to that code being
                                                       // switched onto another core
        public static bool compileOnlyAssembly = false; // mostly used for function support
        private static string userCommand = null;

        static void Main(string[] args)
        {
            Console.Title = "DRCHLLC Core Compiler";
            Console.WriteLine("--------");
            Console.WriteLine("DRCHLL-C Core Compiler v" + compilerVersion);
            Console.WriteLine("Supports ARCISS spec v" + compilerLanguageVersion);
            Console.WriteLine("Written and developed by AGuyWhoIsBored");
            Console.WriteLine("--------");
            Console.WriteLine("");

            TopOfArgumentAsk:
            // load arguments
            if (args.Length == 0) { Console.WriteLine("Waiting for user input ..."); userCommand = Console.ReadLine(); }
            else
            {
                Console.WriteLine("Prepassed arguments found!");
                for (int i = 0; i < args.Length; i++)
                {
                    // for formatting
                    if (i == 0) { userCommand += args[i]; }
                    else { userCommand += " " + args[i]; }
                }
            }
            Console.WriteLine("Beginning argument parsing ...");
            Console.WriteLine("Arguments: " + userCommand);
            #region argument / flag parsing
            int index = 0;
            int flagCollectID = 0;
            int quoteCount = 0;
            char currentChar;
            string bufferString = null;
            userCommand = userCommand + " ";
            StringReader sr = new StringReader(userCommand);
            while (index != userCommand.Length)
            {
                currentChar = (char)sr.Read();
                index++;
                bufferString += currentChar;
                switch (currentChar)
                {
                    case ' ':
                        if (bufferString == "-f ") { flagCollectID = 1; }
                        else if (bufferString == "-t ") { flagCollectID = 2; }
                        else if (bufferString == "-o ") { flagCollectID = 3; }
                        else if (bufferString == "-c ") { flagCollectID = 4; }
                        else if (bufferString == "-r ") { flagCollectID = 5; }
                        else if (bufferString == "-d ") { debug = true; Console.WriteLine("Debug mode enabled!"); }
                        else if (bufferString == "-dd ") { debug = true; moreVerboseDebug = true; Console.WriteLine("More verbose debug more enabled!"); }
                        else if (bufferString == "-m ") { autoSplitMultiCore = true; Console.WriteLine("Auto Split Multi Core enabled!"); } // simply split program and for all other programs enable redundant code removal optimization to preserve variable locations
                        else if (bufferString == "-ao ") { compileOnlyAssembly = true; Console.WriteLine("ARCISS-3L compilation mode enabled!"); } // generates .pa files instead of .pb files (program assembly vs program binary)
                        else
                        {
                            if (flagCollectID == 2) { compileTarget = bufferString; compileTarget = functionLibrary.removeWhitespace(compileTarget); flagCollectID = 0; }
                            else if (flagCollectID == 3) { optimizationCollectiveID = bufferString; optimizationCollectiveID = functionLibrary.removeWhitespace(optimizationCollectiveID); flagCollectID = 0; }
                            else if (flagCollectID == 4) { compileCoreTarget = bufferString; compileCoreTarget = functionLibrary.removeWhitespace(compileCoreTarget); flagCollectID = 0; }
                        }
                        if (quoteCount == 0) { bufferString = null; }
                        break;
                    case '"':
                        quoteCount++;
                        if (flagCollectID == 1 && quoteCount == 2) { sourceFileLocation = bufferString; sourceFileLocation = sourceFileLocation.Remove(sourceFileLocation.Length - 1); quoteCount = 0; flagCollectID = 0; }
                        else if (flagCollectID == 5 && quoteCount == 2) { resourceFolderLocation = bufferString; resourceFolderLocation = resourceFolderLocation.Remove(resourceFolderLocation.Length - 1); quoteCount = 0; flagCollectID = 0; }
                        bufferString = null;
                        break;
                }
            }
            #endregion

            // dumping parsed flag variables
            if (debug)
            {
                Console.WriteLine("compileTarget: " + compileTarget.ToString());
                Console.WriteLine("optimizationCollectiveID: " + optimizationCollectiveID.ToString());
                Console.WriteLine("compileCoreTarget: " + compileCoreTarget.ToString());
                Console.WriteLine("sourceFileLocation: " + sourceFileLocation);
                Console.WriteLine("resourceFolderLocation: " + resourceFolderLocation);
            }

            // not going to check advanced argument syntax because we're going to assume that the user knows that they're doing
            // especially if they have a program like this
            if (compileTarget == "-1"
                || compileCoreTarget == "-1"
                || sourceFileLocation == "")
            { Console.WriteLine("Core arguments are incomplete! Please check your arguments and try again!"); goto TopOfArgumentAsk; }

            #region setting compiler optimizations
            // [1  2  3  4  5  6 ] < bits
            // [32 16 08 04 02 01]
            // 1: variableClockSpeedOptimization
            // 2: independentConditionalBranchingOptimization
            // 3: redundantCodeRemovalOptimization
            // 4: automaticFunctionGrouping
            // 5: memoryManagementOptimizations
            // 6: instructionMergingOptimizations
            if (optimizationCollectiveID != "-1")
            {
                Console.WriteLine("Optimizations enabled! Enabling selected optimizations ...");
                compiler.enableOptimizations = true;
                try
                {
                    optimizationCollectiveID = functionLibrary.convertNumberToBinary(Convert.ToInt16(optimizationCollectiveID), false, 6);
                    StringReader sr1 = new StringReader(optimizationCollectiveID);
                    char currentChar1;
                    int index1 = 0;
                    while (index1 != optimizationCollectiveID.Length)
                    {
                        currentChar1 = (char)sr1.Read();
                        index1++;
                        if (currentChar1 == '1' && index1 == 1) { compiler.enableVariableClockSpeedOptimization = true; if (debug) Console.WriteLine("Optimization 1 enabled!"); }
                        else if (currentChar1 == '1' && index1 == 2) { compiler.enableIndependentConditionalBranchingOptimization = true; if (debug) Console.WriteLine("Optimization 2 enabled!"); }
                        else if (currentChar1 == '1' && index1 == 3) { compiler.enableRedundantCodeRemovalOptimization = true; if (debug) Console.WriteLine("Optimization 3 enabled!"); }
                        else if (currentChar1 == '1' && index1 == 4) { compiler.enableAutomaticFunctionGrouping = true; if (debug) Console.WriteLine("Optimization 4 enabled!"); }
                        else if (currentChar1 == '1' && index1 == 5) { compiler.enableMemoryManagementOptimizations = true; if (debug) Console.WriteLine("Optimization 5 enabled!"); }
                        else if (currentChar1 == '1' && index1 == 6) { compiler.enableInstructionMergingOptimizations = true; if (debug) Console.WriteLine("Optimization 6 enabled!"); }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occured: " + e.Message);
                }
            }
            #endregion

            Console.WriteLine("Compiler ready to start!");
            if (debug) { Console.WriteLine("Press [enter] to begin compile!"); Console.ReadLine(); }

            // begin main compile thread
            compiler.compile();
        }
    }
}
