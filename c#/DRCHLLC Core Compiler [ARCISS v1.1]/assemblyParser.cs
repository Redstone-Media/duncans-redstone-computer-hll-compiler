﻿using System;
using System.IO;

namespace DRCHLLC_Core_Compiler__ARCISS_v1._1_
{
    class assemblyParser
    {
        /*
         * This class will contain all functions and information needed for parsing code from the
         * ARCIS HLL to ARCISS assembly (ARCISS-HLL to ARCISS-3L (Low Level Language)) 
         * 
         * Created 9/5/2016 by AGuyWhoIsBored
         * 
         */

        public static void parseStage1()
        {
            /* stage 1 will
             * -convert most of the functions and tokens into lines of assembly code
             * -generate writeback statements
             * -setup ALL functions
             *      -for regular functions, just 
             *  
             *  stage 1 will also add compiler notes into assembly output for
             *      -memory allocation requests
             *      -all branches, conditional or nonconditional
             *      -all code blocks - i.e ( ) / { }
             *      these will be taken care of in later stages (mainly stage 2)

            // instructions that need writeback instructions
            // add (if funcCount != 1; if it does, check for where the function group came from (if / var set) and set mem write to that
            // sub (if funcCount != 1; if it does, check for where the function group came from (if / var set) and set mem write to that
             bsr, bsl, ad1, sb1, 
            */

            string parsedAssemblyOutput = null;
            string currentTokenID = "";
            string currentTokenValue = "";
            int currentTokenIndex = -1; // 0-based index
            int lineOfCodeGeneratedAssembly = 0;
            int curlyBracketCount = 0;
            bool jumpAfterProcess = false;
            int jumpAfterProcessLoc = 0; // 1 - after var change from function
            string jumpAfterProcessLocData = "";

            // redundant but just want to get the local variable cache address checking working for now
            int MATAddressCounterLocal = 0;

            // MEMW SYNTAX: memw [location] [value] 

        // will use parse tree, which is very similar to syntax tree
        TopOfParseTree:

            if (jumpAfterProcessLoc == 1 && jumpAfterProcess)
            {
                lineOfCodeGeneratedAssembly++;
                // generate writeback for var change from function
                parsedAssemblyOutput += "memw " + jumpAfterProcessLocData + " ~temp~";
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                jumpAfterProcessLoc = 0; jumpAfterProcessLocData = ""; jumpAfterProcess = false;
                goto TopOfParseTree;
            } 
            // so parser generates function for function AND THEN generates writeback on next line
            if (jumpAfterProcessLoc == 1 && !jumpAfterProcess) jumpAfterProcess = true;

            currentTokenIndex++;
            currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
            currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];

            lineOfCodeGeneratedAssembly++;
            logging.logDebug("Hit top of parse tree: ", logging.loggingSenderID.assemblyParser);
            logging.logDebug("Parser info: " + "[ln:" + lineOfCodeGeneratedAssembly.ToString() + ",cti:" + currentTokenIndex.ToString() + ",ctid:" + currentTokenID + ",ctv:" + currentTokenValue + "]", logging.loggingSenderID.assemblyParser);

            // EOF check
            #region End of File Check
            if (currentTokenID == tokens.convertInt("identifier_eof").ToString()) goto EndOfParsingStage1;
            #endregion

            #region Compiler Requests & Info For Compiler
            if (currentTokenID == tokens.convertInt("identifier_new").ToString())
            {
                // gather data from additional tokens and add a compiler memory allocation request
                // local memory allocation request (cache)
                // only allow to run if memoryManagementOptimizations are enabled
                if (compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("identifier_local").ToString())
                {
                    currentTokenIndex++;
                    if (compiler.enableMemoryManagementOptimizations && Program.compileTarget == "1")
                    { logging.logDebug("Local keyword found but local variables not supported on target RC3.0! Defining local variable as a global variable...", logging.loggingSenderID.assemblyParser); parsedAssemblyOutput += "%compiler%: malloc"; }
                    if (compiler.enableMemoryManagementOptimizations && Program.compileTarget == "2" && MATAddressCounterLocal > 3)
                    { logging.logDebug("Local keyword found but cache addressing limit reached on target RC4.0! Defining local variable as a global variable ...", logging.loggingSenderID.assemblyParser); parsedAssemblyOutput += "%compiler%: malloc"; }
                    else if (compiler.enableMemoryManagementOptimizations && Program.compileTarget == "3" && MATAddressCounterLocal > 7)
                    { logging.logDebug("Local keyword found but cache addressing limit reached on target RC5.0! Defining local variable as a global variable ...", logging.loggingSenderID.assemblyParser); parsedAssemblyOutput += "%compiler%: malloc"; }
                    else if (compiler.enableMemoryManagementOptimizations) { parsedAssemblyOutput += "%compiler%: lmalloc"; MATAddressCounterLocal++; }
                    else { logging.logDebug("Local keyword found but memory management optimizations not enabled! Defining local variable as a global variable...", logging.loggingSenderID.assemblyParser); parsedAssemblyOutput += "%compiler%: malloc"; }
                } 
                else parsedAssemblyOutput += "%compiler%: malloc"; 
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex += 1];
                parsedAssemblyOutput += " " + currentTokenValue; 
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex += 2]; // skip the operator (=) to get straight to the value of the variable
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++; // to get to the semicolon; when we hit top of parse tree will read next token
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: [compiler request]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            if (currentTokenID == tokens.convertInt("identifier_jumploc").ToString())
            {
                // add a compiler code jump location request
                parsedAssemblyOutput += "%compiler%: jalloc";
                // get variable from location
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                currentTokenValue = currentTokenValue.Replace('#', ' ');
                currentTokenValue = functionLibrary.removeWhitespace(currentTokenValue);
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: [compiler request]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            if (currentTokenID == tokens.convertInt("identifier_func").ToString())
            {
                // add a compiler function allocation request
                parsedAssemblyOutput += "%compiler%: falloc";
                // get name of function
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex += 1];
                parsedAssemblyOutput += " " + currentTokenValue;
                // skip open parentheses
                currentTokenIndex += 2;
                // grab the amount of arguments it requires - do we need this?
                int argCount = 0;
                while (compiler.tokenTokensArray[currentTokenIndex] != tokens.convertInt("grouper_closeparentheses").ToString()) { currentTokenID = compiler.tokenTokensArray[currentTokenIndex += 1]; argCount++; }
                //parsedAssemblyOutput += " " + argCount.ToString();
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: [compiler request]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            if (currentTokenID == tokens.convertInt("grouper_openbracket").ToString())
            {
                // add a compiler code 'block' marker (start block)
                curlyBracketCount++;
                parsedAssemblyOutput += "%compiler%: sblock" + curlyBracketCount.ToString();
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: [compiler note]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            if (currentTokenID == tokens.convertInt("grouper_closebracket").ToString())
            {
                // add a compiler code 'block' marker (terminate block)
                parsedAssemblyOutput += "%compiler%: tblock" + curlyBracketCount.ToString();
                curlyBracketCount--;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: [compiler note]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion

            #region If Statement Parsing
            if (currentTokenID == tokens.convertInt("identifier_if").ToString())
            {
                // check for input(portID) syntax FIRST!
                if (compiler.tokenTokensArray[currentTokenIndex + 2] == tokens.convertInt("func_input").ToString())
                {
                    parsedAssemblyOutput += "test %input% " + compiler.tokenValuesArray[currentTokenIndex += 4];
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    currentTokenIndex += 2;
                    goto TopOfParseTree;
                }
                // then check if hcond is first to be tested
                if (compiler.tokenTokensArray[currentTokenIndex + 2] == tokens.convertInt("hcond_overflow").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex + 2] == tokens.convertInt("hcond_underflow").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex + 2] == tokens.convertInt("RC40E.RNG_ON").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex + 2] == tokens.convertInt("RC50E.DATA_RECEIVED").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex + 2] == tokens.convertInt("RC50E.STACK_OVERFLOW").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex + 2] == tokens.convertInt("RC50E.DATASTREAM_ACTIVE").ToString())
                {
                    if (compiler.tokenTokensArray[currentTokenIndex + 2] == tokens.convertInt("hcond_overflow").ToString()) parsedAssemblyOutput += "test %so% ~temp1~";
                    else if (compiler.tokenTokensArray[currentTokenIndex + 2] == tokens.convertInt("hcond_underflow").ToString()) parsedAssemblyOutput += "test %su% ~temp1~";
                    else if (compiler.tokenTokensArray[currentTokenIndex + 2] == tokens.convertInt("RC40E.RNG_ON").ToString()) parsedAssemblyOutput += "test %rng_on%";
                    else if (compiler.tokenTokensArray[currentTokenIndex + 2] == tokens.convertInt("RC50E.DATA_RECEIVED").ToString()) parsedAssemblyOutput += "test %data_received%";
                    else if (compiler.tokenTokensArray[currentTokenIndex + 2] == tokens.convertInt("RC50E.STACK_OVERFLOW").ToString()) parsedAssemblyOutput += "test %stack_overflow%";
                    else if (compiler.tokenTokensArray[currentTokenIndex + 2] == tokens.convertInt("RC50E.DATASTREAM_ACTIVE").ToString()) parsedAssemblyOutput += "test %datastream_active%";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                }

                    currentTokenIndex += 3; // skip initial openparentheses first variable
            TopOfIf:
                // FOR single case conditional checks, generate assembly for everything inside first, and then the test opcode
                // FOR multi case conditional checks (Loperators), repeat process for single case conditional checks INCLUDING Loperator logic!

                // for single case, single var conditional checks
                // skip openparentheses and first variable
                if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_greaterthan").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_lessthan").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_equals").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_notequals").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_gequals").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_lequals").ToString())
                {
                    // condition
                    if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_greaterthan").ToString()) parsedAssemblyOutput += "test > ";
                    else if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_lessthan").ToString()) parsedAssemblyOutput += "test < ";
                    else if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_equals").ToString()) parsedAssemblyOutput += "test == ";
                    else if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_notequals").ToString()) parsedAssemblyOutput += "test != ";
                    else if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_gequals").ToString()) parsedAssemblyOutput += "test >= ";
                    else if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_lequals").ToString()) parsedAssemblyOutput += "test <= ";

                    if (compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("variable").ToString()
                        || compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("data_int").ToString())
                    {
                        parsedAssemblyOutput += compiler.tokenValuesArray[currentTokenIndex - 1] + " " + compiler.tokenValuesArray[currentTokenIndex += 1];
                        logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                        compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                        parsedAssemblyOutput = null;
                        currentTokenIndex++;
                        if (compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("grouper_closeparentheses").ToString()) goto TopOfParseTree;
                        else goto LOperator;
                    }
                }
                // for single case, multi var conditional checks
                if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_plus").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_minus").ToString())
                {
                    int varOpCount = 0;
                TopOfVarOpVar:
                    if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_plus").ToString()) parsedAssemblyOutput += "add ";
                    else if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_minus").ToString()) parsedAssemblyOutput += "sub ";
                    else { parsedAssemblyOutput = null; goto condCheck; }
                    if (varOpCount != 0) parsedAssemblyOutput += "~temp1~ " + compiler.tokenValuesArray[currentTokenIndex += 1] + " ~temp1~";
                    else parsedAssemblyOutput += compiler.tokenValuesArray[currentTokenIndex - 1] + " " + compiler.tokenValuesArray[currentTokenIndex += 1] + " ~temp1~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    currentTokenIndex++; varOpCount++; lineOfCodeGeneratedAssembly++;
                    goto TopOfVarOpVar;
                }

            // should only hit this when we have [var] [op] [var]... [COND] scenario
            condCheck:
                if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_greaterthan").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_lessthan").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_equals").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_notequals").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_gequals").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_lequals").ToString())
                {
                    string condCode = "";
                    if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_greaterthan").ToString()) condCode = ">";
                    else if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_lessthan").ToString()) condCode = "<";
                    else if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_equals").ToString()) condCode = "==";
                    else if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_notequals").ToString()) condCode = "!=";
                    else if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_gequals").ToString()) condCode = ">=";
                    else if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_lequals").ToString()) condCode = "<=";

                    // jump to Loperator if necessary
                    if (compiler.tokenTokensArray[currentTokenIndex += 2] == tokens.convertInt("operator_cond_and").ToString()
                        || compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_or").ToString()) { goto LOperator; }

                    // ...[COND] [var] scenario
                    if (compiler.tokenTokensArray[currentTokenIndex] != tokens.convertInt("operator_plus").ToString()
                        && compiler.tokenTokensArray[currentTokenIndex] != tokens.convertInt("operator_minus").ToString())
                    {
                        parsedAssemblyOutput += "test " + condCode + " ~temp1~ " + compiler.tokenValuesArray[currentTokenIndex - 1];
                        logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                        compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                        parsedAssemblyOutput = null;
                        goto TopOfParseTree;
                    }

                    // ...[COND] [var] [op] [var]...scenario
                    // run other computations BEFORE the test instruction
                    else
                    {
                        int varOpCount = 0;
                    TopOfVarOpVar:
                        if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_plus").ToString()) parsedAssemblyOutput += "add ";
                        else if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_minus").ToString()) parsedAssemblyOutput += "sub ";
                        else
                        {
                            //logging.logDebug("hit end of +- 2", logging.loggingSenderID.assemblyParser);
                            parsedAssemblyOutput = null;
                            // assemble test instruction
                            parsedAssemblyOutput += "test " + condCode + " ~temp1~ ~temp2~";
                            logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null; 
                            goto LOperator;
                        }
                        if (varOpCount != 0) parsedAssemblyOutput += "~temp2~ " + compiler.tokenValuesArray[currentTokenIndex += 1] + " ~temp2~";
                        else parsedAssemblyOutput += compiler.tokenValuesArray[currentTokenIndex - 1] + " " + compiler.tokenValuesArray[currentTokenIndex += 1] + " ~temp2~";
                        logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                        compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                        parsedAssemblyOutput = null;
                        currentTokenIndex++; varOpCount++; lineOfCodeGeneratedAssembly++;
                        goto TopOfVarOpVar;
                    }
                }

            // Loperator check
            LOperator:
                if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_and").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_or").ToString())
                {
                    lineOfCodeGeneratedAssembly++;
                    if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_and").ToString()) parsedAssemblyOutput += "%compiler%: lop1";
                    if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_or").ToString()) parsedAssemblyOutput += "%compiler%: lop2";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "] [compiler note]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null; lineOfCodeGeneratedAssembly++;

                    // check for hcond
                    if (compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("hcond_overflow").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("hcond_underflow").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("RC40E.RNG_ON").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("RC50E.DATA_RECEIVED").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("RC50E.STACK_OVERFLOW").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("RC50E.DATASTREAM_ACTIVE").ToString())
                    {
                        if (compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("hcond_overflow").ToString()) parsedAssemblyOutput += "test %so% ~temp1~";
                        else if (compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("hcond_underflow").ToString()) parsedAssemblyOutput += "test %su% ~temp1~";
                        else if (compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("RC40E.RNG_ON").ToString()) parsedAssemblyOutput += "test %rng_on%";
                        else if (compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("RC50E.DATA_RECEIVED").ToString()) parsedAssemblyOutput += "test %data_received%";
                        else if (compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("RC50E.STACK_OVERFLOW").ToString()) parsedAssemblyOutput += "test %stack_overflow%";
                        else if (compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("RC50E.DATASTREAM_ACTIVE").ToString()) parsedAssemblyOutput += "test %datastream_active%";
                        logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                        compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                        parsedAssemblyOutput = null;
                        if (compiler.tokenTokensArray[currentTokenIndex + 1] == tokens.convertInt("grouper_closeparentheses").ToString()) goto TopOfParseTree;
                    }

                    // restart if statement generation
                    currentTokenIndex += 2;  goto TopOfIf;
                }
                goto TopOfParseTree;
            }
            #endregion

            #region Functions
            #region Exit Function
            if (currentTokenID == tokens.convertInt("func_exit").ToString())
            {
                // build function
                parsedAssemblyOutput += "exit";
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                // need to advance three tokens forward
                currentTokenIndex += 3;
                goto TopOfParseTree;
            }
            #endregion
            #region Else 'Function'
            if (currentTokenID == tokens.convertInt("identifier_else").ToString())
            {
                parsedAssemblyOutput += "%compiler%: else";
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion
            #region Output Function
            if (currentTokenID == tokens.convertInt("func_output").ToString())
            {
                parsedAssemblyOutput += "output";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;

                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;

                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #region Input Function
            if (currentTokenID == tokens.convertInt("func_input").ToString())
            {
                parsedAssemblyOutput += "input";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;

                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;

                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #region Jump Function
            if (currentTokenID == tokens.convertInt("func_jump").ToString())
            {
                parsedAssemblyOutput += "jump";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #region Sleep Function
            if (currentTokenID == tokens.convertInt("func_sleep").ToString())
            {
                int num = Convert.ToInt16(compiler.tokenValuesArray[currentTokenIndex += 2]);
                for (int i = 0; i < num; i++)
                {
                    parsedAssemblyOutput += "sleep";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    lineOfCodeGeneratedAssembly++;
                }
                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #region "Return" function
            if (currentTokenID == tokens.convertInt("func_return").ToString())
            {
                // --return with no variable--
                // pop stack value back to goto reg of running core
                // --return with variable --
                // write return variable address to internal output register of ALU
                // pop stack value back to goto reg of running core
                if (compiler.tokenTokensArray[currentTokenIndex += 1] == tokens.convertInt("variable").ToString()
                    || compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("data_int").ToString())
                { parsedAssemblyOutput += "%compiler%: rcall1 " + compiler.tokenValuesArray[currentTokenIndex]; currentTokenIndex++; /* placeholder for writing return variable address to internal output register of ALU*/ }
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null; lineOfCodeGeneratedAssembly++;

                parsedAssemblyOutput += "%compiler%: rcall2"; // pop stack value back to goto reg of running core
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null; 

                goto TopOfParseTree;
            }
            #endregion
            #endregion

            // RC40E extension functions
            #region RC40E Extension Functions
            #region GPU Encode Function
            if (currentTokenID == tokens.convertInt("RC40E.GPU_encode").ToString() && importer.ext_RC40E)
            {
                parsedAssemblyOutput += "gpuencode";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                lineOfCodeGeneratedAssembly++;

                parsedAssemblyOutput += "memw";
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                parsedAssemblyOutput += " ~temp~";
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;

                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #region GPU Clear Function
            if (currentTokenID == tokens.convertInt("RC40E.GPU_clear").ToString() && importer.ext_RC40E)
            {
                parsedAssemblyOutput += "gpuclr";
                currentTokenIndex += 3;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion
            #region GPU Draw Point Function
            if (currentTokenID == tokens.convertInt("RC40E.GPU_drawPoint").ToString() && importer.ext_RC40E)
            {
                parsedAssemblyOutput += "gpudraw";
                currentTokenIndex += 2;
                currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                if (currentTokenID == "4")
                {
                    parsedAssemblyOutput += " " + currentTokenValue;
                }
                if (currentTokenID == "2")
                {
                    parsedAssemblyOutput += " " + currentTokenValue;
                    currentTokenIndex++;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                }
                currentTokenIndex += 2;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion          
            #region GPU Erase Point Function
            if (currentTokenID == tokens.convertInt("RC40E.GPU_erasePoint").ToString() && importer.ext_RC40E)
            {
                parsedAssemblyOutput += "gpuerase";
                currentTokenIndex += 2;
                currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                if (currentTokenID == "4")
                {
                    parsedAssemblyOutput += " " + currentTokenValue;
                }
                if (currentTokenID == "2")
                {
                    parsedAssemblyOutput += " " + currentTokenValue;
                    currentTokenIndex++;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                }
                currentTokenIndex += 2;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion
            #region GPU Granular Update Function
            if (currentTokenID == tokens.convertInt("RC40E.GPU_granularUpdate").ToString() && importer.ext_RC40E)
            {
                parsedAssemblyOutput += "gpugu";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex += 2;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion

            #region Update Speed Function
            if (currentTokenID == tokens.convertInt("RC40E.updateSpeed").ToString() & importer.ext_RC40E)
            {
                parsedAssemblyOutput += "updspd";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #endregion

            // RC50E extension functions
            #region RC50E Extension Functions
            #region GPU Encode Function
            if (currentTokenID == tokens.convertInt("RC50E.GPU_encode").ToString() && importer.ext_RC50E)
            {
                parsedAssemblyOutput += "gpuencode";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                lineOfCodeGeneratedAssembly++;

                parsedAssemblyOutput += "memw";
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                parsedAssemblyOutput += " ~temp~";
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;

                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #region GPU Clear Function
            if (currentTokenID == tokens.convertInt("RC50E.GPU_clear").ToString() && importer.ext_RC50E)
            {
                parsedAssemblyOutput += "gpuclr";
                currentTokenIndex += 3;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion
            #region GPU Draw Point Function
            if (currentTokenID == tokens.convertInt("RC50E.GPU_drawPoint").ToString() && importer.ext_RC50E)
            {
                parsedAssemblyOutput += "gpudraw";
                currentTokenIndex += 2;
                currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                if (currentTokenID == "4")
                {
                    parsedAssemblyOutput += " " + currentTokenValue;
                }
                if (currentTokenID == "2")
                {
                    parsedAssemblyOutput += " " + currentTokenValue;
                    currentTokenIndex++;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                }
                currentTokenIndex += 2;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion          
            #region GPU Erase Point Function
            if (currentTokenID == tokens.convertInt("RC50E.GPU_erasePoint").ToString() && importer.ext_RC50E)
            {
                parsedAssemblyOutput += "gpuerase";
                currentTokenIndex += 2;
                currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                if (currentTokenID == "4")
                {
                    parsedAssemblyOutput += " " + currentTokenValue;
                }
                if (currentTokenID == "2")
                {
                    parsedAssemblyOutput += " " + currentTokenValue;
                    currentTokenIndex++;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                }
                currentTokenIndex += 2;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion
            #region GPU Granular Update Function
            if (currentTokenID == tokens.convertInt("RC50E.GPU_granularUpdate").ToString() && importer.ext_RC50E)
            {
                // create 4 different output instructions to %gex register
                // combine all arguments, convert to binary, and then convert to decimal and send those immediate values to %gex register
                string block1 = compiler.tokenValuesArray[currentTokenIndex += 2];
                string block2 = compiler.tokenValuesArray[currentTokenIndex += 1];
                string block3 = compiler.tokenValuesArray[currentTokenIndex += 1];
                string block4 = compiler.tokenValuesArray[currentTokenIndex += 1];
                string byte1 = block1; string byte2 = block2; string byte3 = block3; string byte4 = block4;

                // very quick check - may not be thorough enough though
                if (compiler.tokenTokensArray[currentTokenIndex] != tokens.convertInt("variable").ToString())
                {
                    // hex stuff to binary conversion
                    block1 = block1.Replace("0x", ""); block1 = functionLibrary.convertNumberToBinary(int.Parse(block1, System.Globalization.NumberStyles.HexNumber), false, 15);
                    block2 = block2.Replace("0x", ""); block2 = functionLibrary.convertNumberToBinary(int.Parse(block2, System.Globalization.NumberStyles.HexNumber), false, 15);
                    // convert blocks into four bytes to be written to granular update registers
                    byte1 = block1.Substring(0, 8);
                    byte2 = block1.Substring(8, 7) + block2.Substring(0, 1);
                    byte3 = block2.Substring(1, 8);
                    byte4 = block2.Substring(9, 6) + block3 + block4;
                    byte1 = Convert.ToInt64(byte1, 2).ToString(); byte2 = Convert.ToInt64(byte2, 2).ToString(); byte3 = Convert.ToInt64(byte3, 2).ToString(); byte4 = Convert.ToInt64(byte4, 2).ToString();
                }

                parsedAssemblyOutput += "output " + byte1 + " %gex";
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;

                parsedAssemblyOutput += "output " + byte2 + " %gex";
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;

                parsedAssemblyOutput += "output " + byte3 + " %gex";
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;

                parsedAssemblyOutput += "output " + byte4 + " %gex";
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;

                parsedAssemblyOutput += "gpugu";
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;

                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion

            #region Core Update Speed Function
            if (currentTokenID == tokens.convertInt("RC50E.updateSpeed").ToString() & importer.ext_RC50E)
            {
                parsedAssemblyOutput += "updspd";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #region Core Enable Function
            if (currentTokenID == tokens.convertInt("RC50E.core_enable").ToString() && importer.ext_RC50E)
            {
                parsedAssemblyOutput += "con";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex += 2;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion
            #region Core Disable Function
            if (currentTokenID == tokens.convertInt("RC50E.core_disable").ToString() && importer.ext_RC50E)
            {
                parsedAssemblyOutput += "coff";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex += 2;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion

            #region XOR Bitwise Function
            if (currentTokenID == tokens.convertInt("RC50E.xor").ToString() && importer.ext_RC50E)
            {
                parsedAssemblyOutput += "xor";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #region OR Bitwise Function
            if (currentTokenID == tokens.convertInt("RC50E.or").ToString() && importer.ext_RC50E)
            {
                parsedAssemblyOutput += "or";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #region AND Bitwise Function
            if (currentTokenID == tokens.convertInt("RC50E.and").ToString() && importer.ext_RC50E)
            {
                parsedAssemblyOutput += "and";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #region XNOR Bitwise Function
            if (currentTokenID == tokens.convertInt("RC50E.xnor").ToString() && importer.ext_RC50E)
            {
                parsedAssemblyOutput += "xnor";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #region NOR Bitwise Function
            if (currentTokenID == tokens.convertInt("RC50E.nor").ToString() && importer.ext_RC50E)
            {
                parsedAssemblyOutput += "nor";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #endregion

            #region Custom Functions
            /* 
            - how custom functions will work with ARCISS-3L
            - fcalls will just be a simple jmp to where falloc of function is located (make sure to merge falloc instruction with the next instruction)
            - before jumping to falloc location, we need to push current loc+1 onto the stack
            - [figure out how to implement function memory management and pushing function arguments onto stack as well]
                - for now, simply don't include scope LOL
            - if function has return;, simply pop out loc on the stack to the goto reg
            - if function has return [value], write [value] to internal output register of ALU, and then pop out loc on the stack to the goto reg
            */
            if (Convert.ToInt16(currentTokenID) > tokens.totalTokenCount 
                // restrict all internal functions - optimize this!!!
                && currentTokenID != tokens.convertInt("RC40E.GPU_encode").ToString()
                && currentTokenID != tokens.convertInt("RC40E.GPU_clear").ToString()
                && currentTokenID != tokens.convertInt("RC40E.GPU_drawPoint").ToString()
                && currentTokenID != tokens.convertInt("RC40E.GPU_erasePoint").ToString()
                && currentTokenID != tokens.convertInt("RC40E.GPU_granularUpdate").ToString()
                && currentTokenID != tokens.convertInt("RC40E.updateSpeed").ToString()
                && currentTokenID != tokens.convertInt("RC40E.RNG_ON").ToString()
                && currentTokenID != tokens.convertInt("RC50E.GPU_encode").ToString()
                && currentTokenID != tokens.convertInt("RC50E.GPU_clear").ToString()
                && currentTokenID != tokens.convertInt("RC50E.GPU_drawPoint").ToString()
                && currentTokenID != tokens.convertInt("RC50E.GPU_erasePoint").ToString()
                && currentTokenID != tokens.convertInt("RC50E.GPU_granularUpdate").ToString()
                && currentTokenID != tokens.convertInt("RC50E.core_enable").ToString()
                && currentTokenID != tokens.convertInt("RC50E.core_disable").ToString()
                && currentTokenID != tokens.convertInt("RC50E.xor").ToString()
                && currentTokenID != tokens.convertInt("RC50E.or").ToString()
                && currentTokenID != tokens.convertInt("RC50E.and").ToString()
                && currentTokenID != tokens.convertInt("RC50E.xnor").ToString()
                && currentTokenID != tokens.convertInt("RC50E.nor").ToString()
                && currentTokenID != tokens.convertInt("RC50E.DATA_RECEIVED").ToString()
                && currentTokenID != tokens.convertInt("RC50E.STACK_OVERFLOW").ToString()
                && currentTokenID != tokens.convertInt("RC50E.DATASTREAM_ACTIVE").ToString())
            {
                // for now, no need to manage function variables as we won't have scope (fix in future revisions of ARCISS most likely)
                // --function call--
                // read goto reg and add 1 to it
                // push ALU output to stack
                // fjump to loc (static) where function definition exists
                // see return function assembly definition for rpoint instructions

                logging.logDebug("----Function call assembly block start----", logging.loggingSenderID.assemblyParser);

                string funcDef = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += "%compiler%: fcall " + funcDef;
                // add any arguments if necessary
                // skip open parentheses
                currentTokenIndex += 2;
                // grab the amount of arguments it requires - do we need this?
                while (compiler.tokenTokensArray[currentTokenIndex] != tokens.convertInt("grouper_closeparentheses").ToString())
                { parsedAssemblyOutput += " " + compiler.tokenValuesArray[currentTokenIndex]; currentTokenIndex++; }
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: [compiler note]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                //compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                currentTokenIndex++; lineOfCodeGeneratedAssembly++;

                parsedAssemblyOutput += "%compiler%: fcall1"; // read goto reg and add one to it
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null; lineOfCodeGeneratedAssembly++;

                parsedAssemblyOutput += "%compiler%: fcall2"; // push ALU output value to stack
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null; lineOfCodeGeneratedAssembly++;

                parsedAssemblyOutput += "fjump " + funcDef; // funcdef fjump to static loc where funcdef exists
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;

                logging.logDebug("----Function call assembly block end", logging.loggingSenderID.assemblyParser);

                goto TopOfParseTree;
            }
            #endregion

            #region Variable Parsing
            // this section will contain the following parse checks:
            // changing variables using all operators 
            // (=, >> [repeat], << [repeat], ++, --, +=, -=, !!) 
            // var1 = var2, new var1 = var2, var1 = var2 + varX - varX ...
            if (currentTokenID == tokens.convertInt("variable").ToString())
            {
                currentTokenIndex++;
                currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                if (currentTokenID == tokens.convertInt("operator_equals").ToString())
                {
                    // MEMW SYNTAX: memw [location] [value]
                    // set operator to memw [mem write] and gather data
                    parsedAssemblyOutput += "memw";
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    currentTokenIndex += 2;
                    currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                    // check for # and variable
                    if (currentTokenID == tokens.convertInt("data_int").ToString())
                    {
                        currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                        parsedAssemblyOutput += " " + currentTokenValue;
                        logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                        compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                        parsedAssemblyOutput = null;
                        currentTokenIndex++;
                        goto TopOfParseTree;
                    }
                    // all cases for var = var
                    else if (currentTokenID == tokens.convertInt("variable").ToString())
                    {
                        if (compiler.tokenTokensArray[currentTokenIndex + 1] != tokens.convertInt("end_of_line").ToString()) goto FunctionGenerator;
                        // for copying values
                        else
                        {
                            // reset parsedassemblyoutput
                            parsedAssemblyOutput = null;
                            // memr, then memw
                            parsedAssemblyOutput += "memw";
                            currentTokenIndex -= 2;
                            currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                            parsedAssemblyOutput += " " + currentTokenValue;

                            parsedAssemblyOutput += " memr";
                            currentTokenIndex += 2;
                            currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                            parsedAssemblyOutput += " " + currentTokenValue;

                            logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;
                            currentTokenIndex++;
                            goto TopOfParseTree;
                        }
                    }
                    // all cases for var change from functions
                    else if (Convert.ToInt16(currentTokenID) > tokens.totalTokenCount)
                    {
                        logging.logDebug("hit var change from function", logging.loggingSenderID.assemblyParser);
                        // setup data
                        jumpAfterProcessLoc = 1;
                        jumpAfterProcessLocData = compiler.tokenValuesArray[currentTokenIndex - 2]; // before equal sign
                        currentTokenIndex--; lineOfCodeGeneratedAssembly--;
                        parsedAssemblyOutput = null; 
                        goto TopOfParseTree;
                    }
                }
                else if (currentTokenID == tokens.convertInt("operator_bitwise_rshift").ToString())
                {
                    int iterate = 1; // by default iterate once
                    // need to grab iteration amount
                    try { iterate = Convert.ToInt16(compiler.tokenValuesArray[currentTokenIndex + 1]); }
                    catch (FormatException) { /* do nothing */ }

                    // first iteration
                    parsedAssemblyOutput += "bsr";
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    lineOfCodeGeneratedAssembly++;

                    // 2+ iterations
                    if (iterate != 1)
                    {
                        for (int i = 1; i < iterate; i++)
                        {
                            parsedAssemblyOutput += "bsr ~temp~";
                            logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;
                            lineOfCodeGeneratedAssembly++;
                        }
                    }

                    parsedAssemblyOutput += "memw " + currentTokenValue;
                    parsedAssemblyOutput += " ~temp~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    currentTokenIndex += 2;
                    if (iterate != 1) currentTokenIndex++;
                    goto TopOfParseTree;
                }
                else if (currentTokenID == tokens.convertInt("operator_bitwise_lshift").ToString())
                {
                    int iterate = 1; // by default iterate once
                    // need to grab iteration amount
                    try { iterate = Convert.ToInt16(compiler.tokenValuesArray[currentTokenIndex + 1]); }
                    catch (FormatException) { /* do nothing */ }

                    // first iteration
                    parsedAssemblyOutput += "bsl";
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    lineOfCodeGeneratedAssembly++;

                    // 2+ iterations
                    if (iterate != 1)
                    {
                        for (int i = 1; i < iterate; i++)
                        {
                            parsedAssemblyOutput += "bsl ~temp~";
                            logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;
                            lineOfCodeGeneratedAssembly++;
                        }
                    }

                    parsedAssemblyOutput += "memw " + currentTokenValue;
                    parsedAssemblyOutput += " ~temp~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    currentTokenIndex += 2;
                    if (iterate != 1) currentTokenIndex++;
                    goto TopOfParseTree;
                }
                else if (currentTokenID == tokens.convertInt("operator_increment").ToString())
                {

                    parsedAssemblyOutput += "ad1";
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    lineOfCodeGeneratedAssembly++;

                    parsedAssemblyOutput += "memw " + currentTokenValue;
                    parsedAssemblyOutput += " ~temp~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    currentTokenIndex += 2;
                    goto TopOfParseTree;
                }
                else if (currentTokenID == tokens.convertInt("operator_decrement").ToString())
                {

                    parsedAssemblyOutput += "sb1";
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    lineOfCodeGeneratedAssembly++;

                    parsedAssemblyOutput += "memw " + currentTokenValue;
                    parsedAssemblyOutput += " ~temp~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    currentTokenIndex += 2;
                    goto TopOfParseTree;
                }
                else if (currentTokenID == tokens.convertInt("operator_bitwise_not").ToString())
                {
                    parsedAssemblyOutput += "not";
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    lineOfCodeGeneratedAssembly++;

                    parsedAssemblyOutput += "memw " + currentTokenValue;
                    parsedAssemblyOutput += " ~temp~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    currentTokenIndex += 2;
                    goto TopOfParseTree;
                }
                else if (currentTokenID == tokens.convertInt("operator_plusequals").ToString())
                {
                    parsedAssemblyOutput += "add";
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    currentTokenIndex += 2;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    lineOfCodeGeneratedAssembly++;

                    currentTokenIndex -= 2;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += "memw " + currentTokenValue;
                    parsedAssemblyOutput += " ~temp~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    currentTokenIndex += 3;
                    goto TopOfParseTree;
                }
                else if (currentTokenID == tokens.convertInt("operator_minusequals").ToString())
                {
                    parsedAssemblyOutput += "sub";
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    currentTokenIndex += 2;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    lineOfCodeGeneratedAssembly++;

                    currentTokenIndex -= 2;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += "memw " + currentTokenValue;
                    parsedAssemblyOutput += " ~temp~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    currentTokenIndex += 3;
                    goto TopOfParseTree;
                }
            }
        #endregion

        // ****TODO****
        // NEW FUNCTION GENERATOR BASED ON PEMDAS MODEL
        // NEW IF STATEMENT PARSER THAT INCLUDES LOPERATORS

        #region Function Generator

        FunctionGenerator:
            int funcCount = 0;
            string originalVar = compiler.tokenValuesArray[currentTokenIndex - 2];
            parsedAssemblyOutput = null; // reset
                                         // generate assembly lines of code one for each 2 vars
        TopOfFunction:
            if (funcCount != 0) lineOfCodeGeneratedAssembly++;
            funcCount++;
            if (compiler.tokenTokensArray[currentTokenIndex += 1] == tokens.convertInt("operator_plus").ToString())
            {
                if (funcCount == 1) parsedAssemblyOutput += "add " + compiler.tokenValuesArray[currentTokenIndex - 1] + " " + compiler.tokenValuesArray[currentTokenIndex += 1] + " ~temp~";
                else parsedAssemblyOutput += "add ~temp~ " + compiler.tokenValuesArray[currentTokenIndex += 1] + " ~temp~";
                logging.logDebug("f" + funcCount + " [" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfFunction;
            }
            else if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("operator_minus").ToString())
            {
                if (funcCount == 1) parsedAssemblyOutput += "sub " + compiler.tokenValuesArray[currentTokenIndex - 1] + " " + compiler.tokenValuesArray[currentTokenIndex += 1] + " ~temp~";
                else parsedAssemblyOutput += "sub ~temp~ " + compiler.tokenValuesArray[currentTokenIndex += 1] + " ~temp~";
                logging.logDebug("f" + funcCount + " [" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfFunction;
            }
            else if (compiler.tokenTokensArray[currentTokenIndex] == tokens.convertInt("end_of_line").ToString())
            {
                // generate writeback statement
                parsedAssemblyOutput += "memw " + originalVar + " ~temp~";
                logging.logDebug("wb [" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
        #endregion

        EndOfParsingStage1:
            logging.logDebug("hit end of parsing stage 1", logging.loggingSenderID.assemblyParser);
            // end of parsing stage 1!

            // ****OPTIMIZE FOR WHEN TO INSERT****
            // FOR THE MOMENT, ONLY FOR RC5.0!
            #region generate cmp instruction before test instruction to load registers with cmp data
            if (Program.compileTarget == "3")
            {
                logging.logDebug("Generating required cmp instructions for RC5.0 ...", logging.loggingSenderID.assemblyParser);
                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {
                    if (compiler.parsedAssemblyList[i].Contains("test ") // include space so that we don't accidentally hit variables or other things
                                                                         // file as bug report
                        && !compiler.parsedAssemblyList[i].Contains("%input%")
                        && !compiler.parsedAssemblyList[i].Contains("~temp1~")
                        && !compiler.parsedAssemblyList[i].Contains("~temp2~")
                        && !compiler.parsedAssemblyList[i].Contains("%rng_on%")
                        && !compiler.parsedAssemblyList[i].Contains("%data_received%")
                        && !compiler.parsedAssemblyList[i].Contains("%stack_overflow%")
                        && !compiler.parsedAssemblyList[i].Contains("%datastream_active%")) // don't know if we need the second temp variable here
                    {
                        // grab both blocks of data we're testing
                        string temp = null;
                        // grab blocks
                        if (compiler.parsedAssemblyList[i].Contains("> ") // spaces here VERY IMPORTANT! distinguishes this check from the other >= <= checks
                            || compiler.parsedAssemblyList[i].Contains("< ")) { temp = compiler.parsedAssemblyList[i].Substring(compiler.parsedAssemblyList[i].IndexOf("test") + 7); }
                        else if (compiler.parsedAssemblyList[i].Contains("==")
                            || compiler.parsedAssemblyList[i].Contains("!=")
                            || compiler.parsedAssemblyList[i].Contains(">=")
                            || compiler.parsedAssemblyList[i].Contains("<=")) { temp = compiler.parsedAssemblyList[i].Substring(compiler.parsedAssemblyList[i].IndexOf("test") + 8); }
                        else if (compiler.parsedAssemblyList[i].Contains("%so%")
                            || compiler.parsedAssemblyList[i].Contains("%su%")) { temp = compiler.parsedAssemblyList[i].Substring(compiler.parsedAssemblyList[i].IndexOf("test") + 9); }

                        string block1 = null; string block2 = null; int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (readLoc != temp.Length)
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        // removewhitespace code unnecessary?
                        block1 = functionLibrary.removeWhitespace(block1);
                        block2 = functionLibrary.removeWhitespace(block2);
                        parsedAssemblyOutput = null;
                        parsedAssemblyOutput += "cmp " + block1 + " " + block2;
                        compiler.parsedAssemblyList.Insert(i, parsedAssemblyOutput);
                        lineOfCodeGeneratedAssembly++; i++;
                    }
                }
            }
            #endregion

            // add 'EOF' to assembly
            parsedAssemblyOutput = null;
            parsedAssemblyOutput += "EOF";
            logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
            parsedAssemblyOutput = null;
            logging.logDebug("Stage 1 parsing complete", logging.loggingSenderID.assemblyParser, true);
        }

        public static void parseStage2()
        {
            // objectives to complete in stage 2 of parsing:
            // convert temporary memr and memw statements into respective ram and cache memory calls
            // add tjump and fjump statements to if statements
            // add jumps for else statements
            // add jump statements from last line of code inside block to outside block

            #region stage 2 - rewrite at some point?
            int blockID = 0;
            int eBlockID = 0;

            // fix bug where it crashes when we have two or more sblock / tblock / else / jalloc / jump statements
            // next to each other
            // to circumvent bug, can simply add any line of code inbetween
            // maybe mark it as syntax error? 

            #region converting temporary memory calls to appropriate memory calls
            logging.logDebug("converting temporary memory calls to appropriate memory calls...", logging.loggingSenderID.assemblyParser);
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("memw"))
                {
                    compiler.parsedAssemblyList[i] += " ";
                    if (Program.moreVerboseDebug) logging.logDebug("found candidate for conversion at line " + i.ToString(), logging.loggingSenderID.assemblyParser);
                    string temp = compiler.parsedAssemblyList[i].Remove(0, compiler.parsedAssemblyList[i].IndexOf("memw") + 5);
                    // grab next word
                    string block1 = null;
                    int readLoc = 0;
                    StringReader sr = new StringReader(temp);
                    char currentChar;
                    currentChar = (char)sr.Read();
                    block1 += currentChar;
                    readLoc++;
                    while (currentChar != ' ')
                    {
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                    }
                    if (Program.moreVerboseDebug) logging.logDebug("block is [" + block1 + "]", logging.loggingSenderID.assemblyParser);
                    if (Program.moreVerboseDebug) logging.logDebug("appropriate memory call is " + functionLibrary.generateMemoryCallFromVariable(functionLibrary.removeWhitespace(block1), true), logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("memw", functionLibrary.generateMemoryCallFromVariable(functionLibrary.removeWhitespace(block1), true));
                }
            }

            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("memr"))
                {
                    compiler.parsedAssemblyList[i] += " ";
                    if (Program.moreVerboseDebug) logging.logDebug("found candidate for conversion at line " + i.ToString(), logging.loggingSenderID.assemblyParser);
                    string temp = compiler.parsedAssemblyList[i].Remove(0, compiler.parsedAssemblyList[i].IndexOf("memr") + 5);
                    // grab next word
                    string block1 = null;
                    int readLoc = 0;
                    StringReader sr = new StringReader(temp);
                    char currentChar;
                    currentChar = (char)sr.Read();
                    block1 += currentChar;
                    readLoc++;
                    while (currentChar != ' ')
                    {
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                    }
                    if (Program.moreVerboseDebug) logging.logDebug("block is [" + block1 + "]", logging.loggingSenderID.assemblyParser);
                    if (Program.moreVerboseDebug) logging.logDebug("appropriate memory call is " + functionLibrary.generateMemoryCallFromVariable(functionLibrary.removeWhitespace(block1), false), logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("memr", functionLibrary.generateMemoryCallFromVariable(functionLibrary.removeWhitespace(block1), false));
                }
            }
            #endregion

            #region Merging sblock, tblock, else, jalloc, and jump statements / merging Loperators / instructions / removing whitespace from parsed assembly list
            // merge sblock, tblock, and else statements with lines of code
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("%compiler%: sblock"))
                {
                    // move this to the next instruction
                    compiler.parsedAssemblyList[i + 1] = compiler.parsedAssemblyList[i].Substring(12) + " " + compiler.parsedAssemblyList[i + 1];
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf('%'));
                }
                else if (compiler.parsedAssemblyList[i].Contains("%compiler%: tblock"))
                {
                    compiler.parsedAssemblyList[i - 1] = compiler.parsedAssemblyList[i].Substring(12) + " " + compiler.parsedAssemblyList[i - 1];
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf('%'));
                }
                else if (compiler.parsedAssemblyList[i].Contains("%compiler%: else") && !compiler.parsedAssemblyList[i + 1].Contains("%compiler%: sblock"))
                {
                    // move this to next instruction
                    compiler.parsedAssemblyList[i + 1] = compiler.parsedAssemblyList[i].Substring(12) + " " + compiler.parsedAssemblyList[i + 1];
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf('%'));
                }
                else if (compiler.parsedAssemblyList[i].Contains("%compiler%: else") && compiler.parsedAssemblyList[i + 1].Contains("%compiler%: sblock"))
                {
                    // move sblock AND else to instruction two lines down
                    // else
                    compiler.parsedAssemblyList[i + 2] = compiler.parsedAssemblyList[i].Substring(12) + " " + compiler.parsedAssemblyList[i + 2];
                    // sblock
                    compiler.parsedAssemblyList[i + 2] = compiler.parsedAssemblyList[i + 1].Substring(12) + " " + compiler.parsedAssemblyList[i + 2];
                    // remove both
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf('%'));
                    compiler.parsedAssemblyList[i + 1] = compiler.parsedAssemblyList[i + 1].Remove(compiler.parsedAssemblyList[i + 1].IndexOf('%'));
                }
                else if (compiler.parsedAssemblyList[i].Contains("%compiler%: jalloc"))
                {
                    // move this to the next instruction
                    compiler.parsedAssemblyList[i + 1] = compiler.parsedAssemblyList[i].Substring(compiler.parsedAssemblyList[i].IndexOf("jalloc")) + " " + compiler.parsedAssemblyList[i + 1];
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(0);
                }
                else if (compiler.parsedAssemblyList[i].Contains("jump"))
                {
                    // replace jump w/ fjump; to make it easier
                    // move this to previous instruction
                    compiler.parsedAssemblyList[i - 1] = compiler.parsedAssemblyList[i - 1] + " " + compiler.parsedAssemblyList[i];
                    compiler.parsedAssemblyList[i - 1] = compiler.parsedAssemblyList[i - 1].Replace("jump", "fjump");
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(0);
                }
                else if (compiler.parsedAssemblyList[i].Contains("%compiler%: lop1"))
                {
                    // move to beginning of previous instruction
                    compiler.parsedAssemblyList[i - 1] = compiler.parsedAssemblyList[i] + " " + compiler.parsedAssemblyList[i - 1];
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(0);
                }
                else if (compiler.parsedAssemblyList[i].Contains("%compiler%: lop2"))
                {
                    // move to beginning of previous instruction
                    compiler.parsedAssemblyList[i - 1] = compiler.parsedAssemblyList[i] + " " + compiler.parsedAssemblyList[i - 1];
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(0);
                }

            }

        TopOfWhiteSpaceRemove0:
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(compiler.parsedAssemblyList[i]))
                { compiler.parsedAssemblyList.RemoveAt(i); goto TopOfWhiteSpaceRemove0; }
            }

            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("%compiler%: falloc"))
                {
                    // move this to the next instruction
                    compiler.parsedAssemblyList[i + 1] = compiler.parsedAssemblyList[i].Substring(compiler.parsedAssemblyList[i].IndexOf("falloc")) + " " + compiler.parsedAssemblyList[i + 1];
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(0);
                }
            }

            TopOfWhiteSpaceRemove1:
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(compiler.parsedAssemblyList[i]))
                { compiler.parsedAssemblyList.RemoveAt(i); goto TopOfWhiteSpaceRemove1; }
            }

            // add else block ID
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("tblock"))
                {
                    //check if there is an else statement after this; if there is, also move this to next instruction!

                    if (compiler.parsedAssemblyList[i + 1].Contains("else"))
                    {
                        logging.logDebug("acquireblockid: " + compiler.parsedAssemblyList[i].Substring(6), logging.loggingSenderID.assemblyParser);
                        int IDIndexCounter = 1;
                        blockID = -1;
                        logging.logDebug(compiler.parsedAssemblyList[i].Substring(0, 6), logging.loggingSenderID.assemblyParser);
                        TopOfFunction:
                        try
                        {
                            Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(6, IDIndexCounter));
                            IDIndexCounter++;
                            goto TopOfFunction;
                        }
                        catch (FormatException)
                        {
                            blockID = Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(6, IDIndexCounter - 1));
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            blockID = Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(6, IDIndexCounter - 1));
                        }
                        logging.logDebug("blockid " + blockID.ToString(), logging.loggingSenderID.assemblyParser);
                        compiler.parsedAssemblyList[i + 1] = "eblock" + blockID.ToString() + " " + compiler.parsedAssemblyList[i + 1];
                    }
                }
            }

            // now check for cases of lone tblock statements; this will happen in the following scenarios:
            /*
                if(x)
                {
                    if(y)
                    {
                        something here
                    }
                }
            */
            // this function seems to be acting a bit weird, watch out!
            TopOfTBlockCheck:
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("tblock"))
                {
                    try
                    {
                        logging.logDebug(("tblock check: " + Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(6))).ToString(), logging.loggingSenderID.assemblyParser);
                        // if we can do the above following, then we only have a tblock[x]
                        // merge these with the line of code above it
                        compiler.parsedAssemblyList[i - 1] = compiler.parsedAssemblyList[i] + "" + compiler.parsedAssemblyList[i - 1];
                        compiler.parsedAssemblyList[i] = "";
                        goto TopOfTBlockCheck;
                    }
                    catch (FormatException) { /*do nothing*/ }
                }
            }
            // removing whitespace
            TopOfWhiteSpaceRemove2:
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(compiler.parsedAssemblyList[i]))
                { compiler.parsedAssemblyList.RemoveAt(i); goto TopOfWhiteSpaceRemove2; }
            }

            #endregion

            logging.logDebug("dump after merge", logging.loggingSenderID.assemblyParser);
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            { logging.logDebug("[" + (i + 1).ToString() + "]: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser); }

            #region generating tjump and fjump statements from if / else / Loperator instructions
            // adding tjump and fjump statements to test instructions
            logging.logDebug("Adding branch code part 1 ...", logging.loggingSenderID.assemblyParser);
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("test"))
                {
                    logging.logDebug("hit test instruction!: [ln" + (i + 1).ToString() + ", " + compiler.parsedAssemblyList[i] + "]", logging.loggingSenderID.assemblyParser);
                    // now we will gather tjump location and fjump location
                    if (compiler.parsedAssemblyList[i + 1].Contains("sblock"))
                    {
                        blockID = functionLibrary.acquireBlockID(compiler.parsedAssemblyList[i + 1], compiler.parsedAssemblyList[i + 1].IndexOf("sblock") + 6);
                        logging.logDebug("blockID: " + blockID.ToString(), logging.loggingSenderID.assemblyParser);
                        // tjump
                        compiler.parsedAssemblyList[i] += " tjump " + (i + 2).ToString();
                        // fjump
                        // set it to i because we only want it reading from the current location down, not from the beginning of the list
                        int fjumpIndex = i + 1; // VERY IMPORTANT!
                        logging.logDebug("fji before fjump find: " + fjumpIndex.ToString(), logging.loggingSenderID.assemblyParser);
                        while (!compiler.parsedAssemblyList[fjumpIndex - 1].Contains("tblock" + blockID.ToString())) // we do -1 b/c zero-based index
                        { fjumpIndex++; }
                        logging.logDebug("line of code found after fjump find: [" + fjumpIndex.ToString() + ", " + compiler.parsedAssemblyList[fjumpIndex - 1] + "]", logging.loggingSenderID.assemblyParser); // we do -1 on the array index b/c zero-based index

                        #region else statement fjump branch map info
                        /* else statement fjump branch map
                        This will be a basic map of the different types of fjump branches and stuff that this compiler will use and do in order to generate correct
                        branches. This is to help me visualize everything and not get ultra confused

                        *refer to physical copy*

                        */
                        #endregion

                        // this will be based on the else statement fjump branch map!

                        // check if we need to do else branching (if else exists)
                        // check for 'else' 1 line past the one fjump gen found
                        if (compiler.parsedAssemblyList[fjumpIndex].Contains("else"))
                        {
                            logging.logDebug("else statement detected as line of code to be set!: [" + (fjumpIndex + 1).ToString() + ", " + compiler.parsedAssemblyList[fjumpIndex] + "]", logging.loggingSenderID.assemblyParser);
                            // now check if eblock tied to else statement found matches blockID
                            eBlockID = functionLibrary.acquireBlockID(compiler.parsedAssemblyList[fjumpIndex], compiler.parsedAssemblyList[fjumpIndex].IndexOf("eblock") + 6);
                            if (eBlockID == blockID)
                            {
                                // eID = bID case
                                compiler.parsedAssemblyList[i] += " fjump " + (fjumpIndex + 1).ToString();
                                logging.logDebug("fji determined from function is [" + (fjumpIndex + 1).ToString() + "] - took eID = bID case", logging.loggingSenderID.assemblyParser);
                            }
                            // this case means that we have to jump outside of the nested else's in if statement!
                            else
                            {
                                logging.logDebug("eID != bID! Generating jump for outside of if statement!", logging.loggingSenderID.assemblyParser);
                                int blockIDTemp = blockID;
                                int lowestBlockID = blockIDTemp;
                                for (int j = fjumpIndex; j < compiler.parsedAssemblyList.Count; j++)
                                {
                                    if (compiler.parsedAssemblyList[j - 1].Contains("tblock"))
                                    {
                                        blockIDTemp = functionLibrary.acquireBlockID(compiler.parsedAssemblyList[j - 1], (compiler.parsedAssemblyList[j - 1].IndexOf("tblock")) + 6);
                                        if (blockIDTemp < lowestBlockID)
                                        {
                                            lowestBlockID = blockIDTemp;
                                            logging.logDebug("lowestblockid has been updated to " + lowestBlockID.ToString(), logging.loggingSenderID.assemblyParser);
                                        }
                                    }
                                }
                                logging.logDebug("lowestblockid generated is " + lowestBlockID.ToString(), logging.loggingSenderID.assemblyParser);
                                logging.logDebug("finding end of if block ...", logging.loggingSenderID.assemblyParser);
                                TopOfEndIfBlockFind:
                                while (!compiler.parsedAssemblyList[fjumpIndex - 1].Contains("tblock" + lowestBlockID.ToString()))
                                {
                                    fjumpIndex++;
                                    logging.logDebug("fji now " + fjumpIndex.ToString() + ", points to line of code " + fjumpIndex.ToString(), logging.loggingSenderID.assemblyParser);
                                }
                                if (compiler.parsedAssemblyList[fjumpIndex].Contains("else"))
                                {
                                    fjumpIndex++;
                                    logging.logDebug("else found; jumping to line of code " + fjumpIndex.ToString(), logging.loggingSenderID.assemblyParser);
                                    goto TopOfEndIfBlockFind;
                                }
                                else if (!compiler.parsedAssemblyList[fjumpIndex].Contains("else"))
                                {
                                    compiler.parsedAssemblyList[i] += " fjump " + (fjumpIndex + 1).ToString();
                                    logging.logDebug("fji determined from function is [" + (fjumpIndex + 1).ToString() + "] - took eID != bID case", logging.loggingSenderID.assemblyParser);
                                }
                            }
                        }
                        else
                        {
                            // default case; jumping outside of if statement block (no else statement)
                            compiler.parsedAssemblyList[i] += " fjump " + (fjumpIndex + 1).ToString();
                            logging.logDebug("fji determined from function is [" + (fjumpIndex + 1).ToString() + "] - took default case", logging.loggingSenderID.assemblyParser);
                        }
                    }
                }
            }
            // for Loperators
            // temporary, see if this works
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("test"))
                {
                    logging.logDebug("hit test instruction!: [ln" + (i + 1).ToString() + ", " + compiler.parsedAssemblyList[i] + "]", logging.loggingSenderID.assemblyParser);
                    // now we will gather tjump location and fjump location
                    // and Loperator
                    if (compiler.parsedAssemblyList[i].Contains("%compiler%: lop1"))
                    {
                        // tjump is next line of code
                        compiler.parsedAssemblyList[i] += " tjump " + (i + 2);
                        // fjump is fjump on the last test instruction 
                        int fJumpIndex = i + 1;
                        while (compiler.parsedAssemblyList[fJumpIndex].Contains("%compiler%: lop1") 
                            || compiler.parsedAssemblyList[fJumpIndex].Contains("%compiler%: lop2") 
                            || !compiler.parsedAssemblyList[fJumpIndex].Contains("test"))
                        { fJumpIndex++;  }
                        // now that we have the line of code w/ the final fjump line of code, extract that line of code
                        // see if this works - tentative solution
                        string fJumpLoc = compiler.parsedAssemblyList[fJumpIndex].Substring(compiler.parsedAssemblyList[fJumpIndex].IndexOf("fjump") + 6);
                        compiler.parsedAssemblyList[i] += " fjump " + fJumpLoc;
                    }
                    // or Loperator
                    else if (compiler.parsedAssemblyList[i].Contains("%compiler%: lop2"))
                    {
                        // tjump on lop2 is tjump loc from last test statement
                        int tJumpIndex = i + 1;
                        while (compiler.parsedAssemblyList[tJumpIndex].Contains("%compiler%: lop1") 
                            || compiler.parsedAssemblyList[tJumpIndex].Contains("%compiler%: lop2") 
                            || compiler.parsedAssemblyList[tJumpIndex].Contains("cmp ")) // specific for RC5.0 (for now)
                        { tJumpIndex++; }
                        string modItem = compiler.parsedAssemblyList[tJumpIndex].Remove(compiler.parsedAssemblyList[tJumpIndex].IndexOf("fjump"));
                        string tJumpLoc = modItem.Substring(modItem.IndexOf("tjump") + 6);
                        compiler.parsedAssemblyList[i] += " tjump " + tJumpLoc;
                        // fjump next line of code
                        compiler.parsedAssemblyList[i] += "fjump " + (i + 2);
                    }
                }
            }
            #endregion

            logging.logDebug("dump after adding branch code part 1", logging.loggingSenderID.assemblyParser);
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            { logging.logDebug("[" + (i + 1).ToString() + "]: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser); }

            #region generating fjump statements for end of blocks
            logging.logDebug("Adding branch code part 2 ...", logging.loggingSenderID.assemblyParser);
            // adding fjumps to end of blocks
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                int fjumpIndex = i;
                if (compiler.parsedAssemblyList[i].Contains("tblock") && !compiler.parsedAssemblyList[i].Contains("jump") && !compiler.parsedAssemblyList[i].Contains("exit")) // add the jump & exit check as well b/c they don't need fjump statement
                {
                    logging.logDebug("Line of code at end of block found!: [" + (i + 1).ToString() + ", " + compiler.parsedAssemblyList[i] + "]", logging.loggingSenderID.assemblyParser);
                    // check if next line of code contains else; if it does, jump past it until we don't hit else
                    if (compiler.parsedAssemblyList[i + 1].Contains("else"))
                    {
                        logging.logDebug("line of code to load has else in it! jumping out ...", logging.loggingSenderID.assemblyParser);
                        fjumpIndex++;
                        // find place where there is no else statement
                        TopOfEndBlockFind:
                        while (!compiler.parsedAssemblyList[fjumpIndex - 1].Contains("tblock"))
                        {
                            fjumpIndex++;
                            logging.logDebug("fji is now " + fjumpIndex, logging.loggingSenderID.assemblyParser);
                        }
                        if (compiler.parsedAssemblyList[fjumpIndex].Contains("else"))
                        {
                            fjumpIndex++;
                            goto TopOfEndBlockFind;
                        }
                        else
                        {
                            logging.logDebug("place with no else statment found! found else before", logging.loggingSenderID.assemblyParser);
                            logging.logDebug("[" + (fjumpIndex + 1).ToString() + ", " + compiler.parsedAssemblyList[fjumpIndex] + "]", logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList[i] += " fjump " + (fjumpIndex + 1).ToString();
                        }
                    }
                    else
                    {
                        logging.logDebug("place with no else statment found! didnt find else before", logging.loggingSenderID.assemblyParser);
                        logging.logDebug(compiler.parsedAssemblyList[fjumpIndex + 1], logging.loggingSenderID.assemblyParser);
                        compiler.parsedAssemblyList[i] += " fjump " + (fjumpIndex + 2).ToString();
                    }
                }
            }
            #endregion

            logging.logDebug("dump after adding branch code part 2", logging.loggingSenderID.assemblyParser);
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                logging.logDebug("[" + (i + 1).ToString() + "]: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser);
            }

            #region removing sblock, tblock, eblock, else, and Loperator compiler notes
            logging.logDebug("Updating and normalizing parsed assembly code ...", logging.loggingSenderID.assemblyParser);
            TopOfRemoveBlockCompilerNotes:
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("tblock"))
                {
                    // find length of tblock string WITH ID
                    int tempBlockID = functionLibrary.acquireBlockID(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("tblock") + 6);
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("tblock"), 7 + tempBlockID.ToString().Length); // 7 for tblock + whitespace
                    logging.logDebug("line of code " + (i + 1).ToString() + " updated!: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser);
                    goto TopOfRemoveBlockCompilerNotes;
                }
                if (compiler.parsedAssemblyList[i].Contains("sblock"))
                {
                    // find length of tblock string WITH ID
                    int tempBlockID = functionLibrary.acquireBlockID(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("sblock") + 6);
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("sblock"), 7 + tempBlockID.ToString().Length); // 7 for tblock + whitespace
                    logging.logDebug("line of code " + (i + 1).ToString() + " updated!: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser);
                    goto TopOfRemoveBlockCompilerNotes;
                }
                if (compiler.parsedAssemblyList[i].Contains("eblock"))
                {
                    // find length of tblock string WITH ID
                    int tempBlockID = functionLibrary.acquireBlockID(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("eblock") + 6);
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("eblock"), 7 + tempBlockID.ToString().Length); // 7 for tblock + whitespace
                    logging.logDebug("line of code " + (i + 1).ToString() + " updated!: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser);
                    goto TopOfRemoveBlockCompilerNotes;
                }
                if (compiler.parsedAssemblyList[i].Contains("else"))
                {
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("else"), 5); // 5 for else length + 1 (whitespace)
                    logging.logDebug("line of code " + (i + 1).ToString() + " updated!: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser);
                    goto TopOfRemoveBlockCompilerNotes;
                }
                if (compiler.parsedAssemblyList[i].Contains("%compiler%: lop1"))
                {
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("%compiler%: lop1"), 17); // +1 on count for whitespace
                    logging.logDebug("line of code " + (i + 1).ToString() + " updated!: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser);
                    goto TopOfRemoveBlockCompilerNotes;
                }
                if (compiler.parsedAssemblyList[i].Contains("%compiler%: lop2"))
                {
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("%compiler%: lop2"), 17); // +1 on count for whitespace
                    logging.logDebug("line of code " + (i + 1).ToString() + " updated!: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser);
                    goto TopOfRemoveBlockCompilerNotes;
                }
            }

            #endregion
            #endregion
        }
    }
}