﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DRCHLLC_Core_Compiler__ARCISS_v1._1_
{
    class machineCodeParser
    {
        /*
         * This class will contain all functions and information needed for parsing code from
         * ARCIS assembly to native machine code on specific targets.
         * 
         * Created 9/5/2016 by AGuyWhoIsBored
         * 
         */

        // machine code array
        public static List<string> machineCodeList = new List<string>();
        static int digits = -1;

        public static void generateMachineCode()
        {
            // WILL CHECK FOR FUNCTIONS / INSTRUCTIONS THAT HAVE NOT BEEN CREATED YET!
            // ramw / ramr - RAM write / RAM read
            // cachew / cacher - cache write / cache read

            logging.logDebug("Target computer generating for: " + Program.compileTarget, logging.loggingSenderID.machineCodeParser);
            logging.logDebug("Core generating for: " + Program.compileCoreTarget, logging.loggingSenderID.machineCodeParser);
            digits = Convert.ToInt16(Math.Floor(Math.Log10(compiler.parsedAssemblyList.Count) + 1));

            // this is where we will generate our machine code!
            // length of word on RC3.0 - 41 bits
            // length of word on RC4.0 - 84 bits
            // length of word on RC5.0 - 48 bits

            #region All Language Opcodes
            //ARCISS v1.1 COMPILER ALL PARSED GENERATED FUNCTIONS:

            //TEMP (created in stage 1 of parsing, removed in stage 2 of parsing):
            //memw
            //memr

            //GLOBAL:
            //ramw
            //ramr
            //cachew (RC4.0+ only)
            //cacher (RC4.0+ only)
            //malloc
            //jalloc [not actual opcode; generated and then deleted by compiler]
            //test
            //exit
            //flush
            //output
            //input
            //jump
            //bsr
            //bsl
            //ad1
            //sb1
            //not
            //add
            //sub

            // new in v1.1:
            // sleep

            //RC4.0 extension
            //gpuencode
            //gpuclr
            //gpudraw
            //gpuerase
            //gpugu
            //updspd

            //RC5.0 extension:
            //gpuencode
            //gpuclr
            //gpudraw
            //gpuerase
            //gpugu
            //con 
            //coff
            //xor
            //or
            //and
            //xnor
            //nor

            #endregion

            // ****UPDATE RC3.0 AND RC4.0 MACHINE CODE TO V1.1 SPEC****
            // ****IMPLEMENT CACHING INTO RC4.0****
            #region RC3.0 machine code generation
            if (Program.compileTarget == "1")
            {
                #region RC3.0 IS Bit Layouts
                // instruction set bit layout for RC3.0 core 1 (facing north)
                // [0 0 0 0 0 ] [0 0 0 0 0 ] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0  0 0 0 0] [0 0] [0 0 0] 
                // [1 2 4 8 16] [1 2 4 8 16] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [16 8 4 2 1] [1 2] [1 2 4]
                // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]

                // instruction set bit layout for RC3.0 core 2 (facing north)
                // [0 0 0 0 0 ] [0 0 0 0 0 ] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0 0 0 0 0 ] [0 0] [0 0 0]
                // [1 2 4 8 16] [1 2 4 8 16] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [1 2 4 8 16] [1 2] [1 2 4]
                // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]

                // instruction set bit layout for RC3.0 core 3 (facing north)
                // [0  0 0 0 0] [0  0 0 0 0] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0 0 0 0 0 ] [0 0] [0 0 0]
                // [16 8 4 2 1] [16 8 4 2 1] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [1 2 4 8 16] [1 2] [1 2 4]
                // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]

                // instruction set bit layout for RC3.0 core 4 (facing north)
                // [0  0 0 0 0] [0  0 0 0 0] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0 0 0 0 0 ] [0 0] [0 0 0]
                // [16 8 4 2 1] [16 8 4 2 1] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [1 2 4 8 16] [1 2] [1 2 4]
                // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]
                #endregion

                // var for incrementing line of code location if none provided (default)
                int fJumpDefault = 2; // on the first line of code, we jump to line 2
                bool generate = true; // if this is true, we will generated a line of code; if it's not (i.e. malloc for values returning 0), will not generate!

                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {

                    // init
                    #region Init vars
                    string currentInstructionWord = null;

                    // will break down machine code into different segments
                    string mImmediate = "00000000";
                    string mGFalse = "00000";
                    string mGTrue = "00000";
                    string mCond = "00000";
                    string mRamW = "0000";
                    string mRamR = "0000";
                    string mOpcode = "00000";
                    string mDualReadToggle = "00";
                    string mExtension = "000";
                    #endregion

                    // now find what function or thing we need to do
                    #region malloc opcode
                    if (compiler.parsedAssemblyList[i].Contains("%compiler%: malloc"))
                    {
                        // grab values
                        int memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("malloc") + 7);
                        int value = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].LastIndexOf("$") + 1);
                        // update machine code
                        if (generate == true)
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(memAddr, true, 4);
                                mImmediate = functionLibrary.convertNumberToBinary(value, false, 8);
                                // only jump that we should see with a malloc
                                if (compiler.parsedAssemblyList[i].Contains("fjump"))
                                { mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5), true, 5); }
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                mRamW = functionLibrary.convertNumberToBinary(memAddr, false, 4);
                                mImmediate = functionLibrary.convertNumberToBinary(value, true, 8);
                                // only jump that we should see with a malloc
                                if (compiler.parsedAssemblyList[i].Contains("fjump"))
                                { mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5), true, 5); }
                            }
                        }
                    }
                    #endregion

                    #region exit opcode
                    if (compiler.parsedAssemblyList[i].Contains("exit"))
                    {
                        // generate machine code; this opcode is easy!
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") // cores 1, 3 & 4's instruction set bit layouts are identical
                        { mOpcode = "01011"; }
                        else if (Program.compileCoreTarget == "2")
                        { mOpcode = "11010"; }
                    }
                    #endregion

                    #region flush opcode
                    if (compiler.parsedAssemblyList[i].Contains("flush"))
                    {
                        // grab flush reg
                        int regToFlush = acquireMemoryAddress(compiler.parsedAssemblyList[i], 5);
                        // convert regtoflush from value in code to value required in ext.
                        regToFlush++;
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00111"; }
                        else if (Program.compileCoreTarget == "2") { mOpcode = "11100"; }
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mExtension = functionLibrary.convertNumberToBinary(regToFlush, true, 3); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mExtension = functionLibrary.convertNumberToBinary(regToFlush, false, 3); }
                    }
                    #endregion

                    #region output opcode
                    // syntax one
                    if (compiler.parsedAssemblyList[i].Contains("output1"))
                    {
                        // grab var address
                        int memAddr = -1;
                        int extension = -1;
                        int immediate = -1;

                        if (compiler.parsedAssemblyList[i].Contains("$")) { memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")); }
                        else
                        {
                            // grab second number, will need to run interesting function here
                            int IDIndexCounter = 1;
                            TopOfFunction:
                            try
                            {
                                Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(7, IDIndexCounter));
                                IDIndexCounter++;
                                goto TopOfFunction;
                            }
                            catch (FormatException) { /*do nothing*/ }
                            int IDIndexCounter2 = 1;
                            TopOfFunction2:
                            try
                            {
                                Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(7 + IDIndexCounter, IDIndexCounter2));
                                IDIndexCounter2++;
                                goto TopOfFunction2;
                            }
                            catch (FormatException) { /*do nothing*/ }
                            immediate = acquireMemoryAddress(compiler.parsedAssemblyList[i], 7 + IDIndexCounter + (IDIndexCounter2 - IDIndexCounter - 1)); // will need to test this; quick tests show this works
                        }
                        extension = acquireMemoryAddress(compiler.parsedAssemblyList[i], 7);
                        if (Program.compileCoreTarget == "1")
                        {
                            if (memAddr == -1) { mRamR = "0000"; }
                            else { mRamR = functionLibrary.convertNumberToBinary(memAddr, true, 4); }
                            mOpcode = "01001";

                            if (immediate != -1) { mImmediate = functionLibrary.convertNumberToBinary(immediate, false, 8); }
                        }
                        else if (Program.compileCoreTarget == "2")
                        {
                            if (memAddr == -1) { mRamR = "0000"; }
                            else { mRamR = functionLibrary.convertNumberToBinary(memAddr, true, 4); }
                            mOpcode = "10010";

                            if (immediate != -1) { mImmediate = functionLibrary.convertNumberToBinary(immediate, false, 8); }
                        }
                        else if (Program.compileCoreTarget == "3" | Program.compileCoreTarget == "4")
                        {
                            if (memAddr == -1) { mRamR = "0000"; }
                            else { mRamR = functionLibrary.convertNumberToBinary(memAddr, true, 4); }
                            mOpcode = "01001";

                            if (immediate != -1) { mImmediate = functionLibrary.convertNumberToBinary(immediate, true, 8); }
                        }

                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mExtension = functionLibrary.convertNumberToBinary(extension + 1, true, 3); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mExtension = functionLibrary.convertNumberToBinary(extension + 1, false, 3); }
                    }

                    // syntax two
                    if (compiler.parsedAssemblyList[i].Contains("output2"))
                    {
                        mExtension = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], 7) + 1, true, 3);
                        if (Program.compileCoreTarget == "1") { mOpcode = "01001"; }
                        else if (Program.compileCoreTarget == "2" | Program.compileCoreTarget == "3" | Program.compileCoreTarget == "4") { mOpcode = "10010"; }
                    }
                    #endregion

                    #region input opcode
                    if (compiler.parsedAssemblyList[i].Contains("input"))
                    {
                        mOpcode = "01010";
                        // by default data will be pushed onto 
                        // gather data
                        int inputSocket = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("input") + 5);
                        int memAddr = 0;
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        { memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")); }
                        // modify input socket based on hardware on RC 3.0
                        if (inputSocket == 6) { inputSocket = 1; }
                        else if (inputSocket == 5) { inputSocket = 3; }
                        else if (inputSocket == 4) { inputSocket = 2; }

                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mExtension = functionLibrary.convertNumberToBinary(inputSocket, true, 3); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mExtension = functionLibrary.convertNumberToBinary(inputSocket, false, 3); }
                        if (memAddr != 0)
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamW = functionLibrary.convertNumberToBinary(memAddr, true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamW = functionLibrary.convertNumberToBinary(memAddr, false, 4); }
                        }
                    }

                    #endregion

                    #region sleep opcode
                    // literally don't do anything at all
                    if(compiler.parsedAssemblyList[i].Contains("sleep")) { /*do nothing*/ }
                    #endregion

                    #region bsr opcode
                    if (compiler.parsedAssemblyList[i].Contains("bsr"))
                    {
                        // generate opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00110"; }
                        else if (Program.compileCoreTarget == "2") { mOpcode = "01100"; }

                        // grab address to modify
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4); }
                    }
                    #endregion

                    #region bsl opcode
                    if (compiler.parsedAssemblyList[i].Contains("bsl"))
                    {
                        // generate opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00101"; }
                        else if (Program.compileCoreTarget == "2") { mOpcode = "10100"; }

                        // grab address to modify
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4); }
                    }
                    #endregion

                    #region ad1 opcode
                    if (compiler.parsedAssemblyList[i].Contains("ad1"))
                    {
                        // ad1 instruction
                        // opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00001"; }
                        else if (Program.compileCoreTarget == "2") { mOpcode = "10000"; }
                        // mem addr
                        // we have this statement here because ad1 temp[x] temp[x] instructions can be generated
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4); }
                        }
                        // immediate value
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mImmediate = "00000001"; }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mImmediate = "10000000"; }
                    }
                    #endregion

                    #region sb1 opcode
                    if (compiler.parsedAssemblyList[i].Contains("sb1"))
                    {
                        // opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00010"; }
                        else if (Program.compileCoreTarget == "2") { mOpcode = "01000"; }
                        // mem addr
                        // we have this statement here because sb1 temp[x] temp[x] instructions can be generated
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4); }
                        }
                        // immediate value
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mImmediate = "00000001"; }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mImmediate = "10000000"; }
                    }
                    #endregion

                    #region not opcode
                    if (compiler.parsedAssemblyList[i].Contains("not"))
                    {
                        // opcode
                        mOpcode = "00100";
                        // mem addr
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4); }
                    }
                    #endregion

                    #region add opcode
                    if (compiler.parsedAssemblyList[i].Contains("add"))
                    {
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("add"), 4);
                        string block1 = null; string block2 = null; string block3 = null; int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        { readLoc++; currentChar = (char)sr.Read(); block1 += currentChar; }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        { readLoc++; currentChar = (char)sr.Read(); block2 += currentChar; }
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block3 += currentChar;
                        while (readLoc != temp.Length)
                        { readLoc++; currentChar = (char)sr.Read(); block3 += currentChar; }

                        // opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00001"; }
                        if (Program.compileCoreTarget == "2") { mOpcode = "10000"; }

                        // actual add instruction
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mDualReadToggle = "01"; }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mDualReadToggle = "10"; }

                        // because of the RC3.0 architecture, we'll need to detect if either block is an
                        // immediate int. If it is, then we have to set that as immediate value, and have
                        // the other value become mramr
                        // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                        // checker already checks that and takes care of that for us (although the error produced 
                        // is a bit misleading)

                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8); }
                            // then set other block to mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4); }
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8); }
                            // then set other block to mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4); }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4); }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4); }
                        }

                    }
                    #endregion

                    #region subtract opcode
                    // pretty much pasted from add opcode, too lazy to do otherwise
                    if (compiler.parsedAssemblyList[i].Contains("sub"))
                    {
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("sub"), 4);
                        string block1 = null; string block2 = null; string block3 = null; int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        { readLoc++; currentChar = (char)sr.Read(); block1 += currentChar; }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        { readLoc++; currentChar = (char)sr.Read(); block2 += currentChar; }
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block3 += currentChar;
                        while (readLoc != temp.Length)
                        { readLoc++; currentChar = (char)sr.Read(); block3 += currentChar; }

                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00010"; }
                        if (Program.compileCoreTarget == "2") { mOpcode = "01000"; }

                        // actual sub instruction
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mDualReadToggle = "01"; }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mDualReadToggle = "10"; }

                        // because of the RC3.0 architecture, we'll need to detect if either block is an
                        // immediate int. If it is, then we have to set that as immediate value, and have
                        // the other value become mramr
                        // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                        // checker already checks that and takes care of that for us (although the error produced 
                        // is a bit misleading)

                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8); }
                            // then set other block to mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4); }
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8); }
                            // then set other block to mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4); }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4); }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4); }
                        }
                    }
                    #endregion

                    // ****IMPLEMENT NEW CONDITIONS INTO RC3.0 LEGACY MACHINE CODE - MAY HAVE TO CREATE MULTIPLE INSTRUCTIONS IN THE ASSEMBLYPARSER****
                    #region test instruction
                    if (compiler.parsedAssemblyList[i].Contains("test"))
                    {
                        // grab what we're testing
                        // list of things we can test:
                        // SO - shift overflow
                        // SU - shift underflow
                        // <, >, ==
                        // user input 1, 2, 3, 4
                        if (compiler.parsedAssemblyList[i].Contains("%so%"))
                        {
                            // test ID
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "10000"; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "00001"; }
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("%su%"))
                        {
                            // test ID
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "01000"; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "00010"; }
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("=="))
                        {
                            // test ID
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "11000"; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "00011"; }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mOpcode = "00011"; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "11000"; }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mDualReadToggle = "01"; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mDualReadToggle = "10"; }
                            // load two numbers in
                            // will have to grab blocks
                            string temp = compiler.parsedAssemblyList[i];
                            if (temp.IndexOf("test") != 0)
                            { /*remove text before*/ temp = temp.Remove(0, compiler.parsedAssemblyList[i].IndexOf("test")); }
                            temp = temp.Remove(temp.IndexOf("test"), 8);
                            string block1 = null; string block2 = null; int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            { readLoc++; currentChar = (char)sr.Read(); block1 += currentChar; }
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            { readLoc++; currentChar = (char)sr.Read(); block2 += currentChar; }

                            // because of the RC3.0 architecture, we'll need to detect if either block is an
                            // immediate int. If it is, then we have to set that as immediate value, and have
                            // the other value become mramr
                            // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                            // checker already checks that and takes care of that for us (although the error produced 
                            // is a bit misleading)

                            if (!block1.Contains("$"))
                            {
                                // block1 becomes immediate value
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8); }
                                // then set other block to mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4); }
                            }
                            else if (!block2.Contains("$"))
                            {
                                // block2 becomes immediate value
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8); }
                                // then set other block to mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4); }
                            }
                            else
                            {
                                // convert both blocks to mramr and mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4); }
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4); }
                            }
                        }
                        else if (compiler.parsedAssemblyList[i].Contains(">"))
                        {
                            // test ID
                            mCond = "00100";
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mOpcode = "00011"; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "11000"; }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mDualReadToggle = "01"; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mDualReadToggle = "10"; }

                            // because of the way the RC3.0 architecture works, we'll need to test both blocks
                            // and see if one of them is an immediate number - if it is, that block will be loaded
                            // into the immediate part of the instruction word (which on the RC3.0 is piped into cache b)
                            // we will also need to check if the immediate number is on the righthand side of our assembly code,
                            // because if it's not, we'll need to invert the sign (as we can't change where the immediate value
                            // will be piped into) to be able to get accurate results

                            // load two numbers in
                            // will have to grab blocks
                            // for temp string, will have to remove any test BEFORE test!
                            string temp = compiler.parsedAssemblyList[i];
                            if (temp.IndexOf("test") != 0)
                            { /*remove text before*/ temp = temp.Remove(0, compiler.parsedAssemblyList[i].IndexOf("test")); }
                            temp = temp.Remove(temp.IndexOf("test"), 7);
                            string block1 = null; string block2 = null; int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            { readLoc++; currentChar = (char)sr.Read(); block1 += currentChar; }
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            { readLoc++; currentChar = (char)sr.Read(); block2 += currentChar; }

                            if (!block1.Contains("$"))
                            {
                                // block1 becomes immediate value
                                // because block1 is on the lefthand side of our operator, switch operator
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8); }
                                compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace('>', '<');
                                // then set other block to mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4); }
                            }
                            else if (!block2.Contains("$"))
                            {
                                // block2 becomes immediate value
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8); }
                                // then set other block to mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4); }
                            }
                            else
                            {
                                // convert both blocks to mramr and mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4); }
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4); }
                            }
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("<"))
                        {
                            // test ID
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "10100"; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "00101"; }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mOpcode = "00011"; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "11000"; }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mDualReadToggle = "01"; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mDualReadToggle = "10"; }

                            // because of the way the RC3.0 architecture works, we'll need to test both blocks
                            // and see if one of them is an immediate number - if it is, that block will be loaded
                            // into the immediate part of the instruction word (which on the RC3.0 is piped into cache b)
                            // we will also need to check if the immediate number is on the righthand side of our assembly code,
                            // because if it's not, we'll need to invert the sign (as we can't change where the immediate value
                            // will be piped into) to be able to get accurate results

                            // load two numbers in
                            // will have to grab blocks
                            string temp = compiler.parsedAssemblyList[i];
                            if (temp.IndexOf("test") != 0)
                            { /*remove text before*/ temp = temp.Remove(0, compiler.parsedAssemblyList[i].IndexOf("test")); }
                            temp = temp.Remove(temp.IndexOf("test"), 7);
                            string block1 = null; string block2 = null; int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            { readLoc++; currentChar = (char)sr.Read(); block1 += currentChar; }
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            { readLoc++; currentChar = (char)sr.Read(); block2 += currentChar; }

                            if (!block1.Contains("$"))
                            {
                                // block1 becomes immediate value
                                // because block1 is on the lefthand side of our operator, switch operator
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8); }
                                compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace('<', '>');
                                // then set other block to mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4); }
                            }
                            else if (!block2.Contains("$"))
                            {
                                // block2 becomes immediate value
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8); }
                                // then set other block to mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4); }
                            }
                            else
                            {
                                // convert both blocks to mramr and mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 4); }
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 4); }
                            }
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("%input%"))
                        {
                            // collect which one it is
                            string temp = compiler.parsedAssemblyList[i].Substring(compiler.parsedAssemblyList[i].IndexOf("%input%") + 8);
                            string block1 = null; int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            { readLoc++; currentChar = (char)sr.Read(); block1 += currentChar; }
                            block1 = functionLibrary.removeWhitespace(block1);

                            if (block1 == "0")
                            {
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "01100"; }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "00110"; }
                            }
                            else if (block1 == "1")
                            {
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "11100"; }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "00111"; }
                            }
                            else if (block1 == "2")
                            {
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "00010"; }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "01000"; }
                            }
                            else if (block1 == "3")
                            {
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "10010"; }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "01001"; }
                            }
                        }
                    }
                    #endregion

                    #region ramw (ONLY) component
                    if (compiler.parsedAssemblyList[i].Contains("ramw") && !compiler.parsedAssemblyList[i].Contains("ramr"))
                    {
                        // will assume that [ramw $address$ temp] is only case of this, so we will keep it that way
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4); }
                    }
                    #endregion

                    #region ramr + ramw component (for copying values)
                    // can be optimized
                    if (compiler.parsedAssemblyList[i].Contains("ramr") && compiler.parsedAssemblyList[i].Contains("ramw"))
                    {
                        // grab two memory addresses needed
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4); }
                        int nextStartLoc = compiler.parsedAssemblyList[i].IndexOf("$");
                        nextStartLoc = compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1);
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1)), true, 4); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1)), false, 4); }
                    }
                    #endregion

                    #region fjump component
                    if (compiler.parsedAssemblyList[i].Contains("fjump"))
                    { /*all four cores share identical mgfalse bit layout!*/ mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5), true, 5); }
                    else if (mGFalse == "00000" && generate == true) { mGFalse = functionLibrary.convertNumberToBinary(fJumpDefault, true, 5); }
                    #endregion

                    #region tjump component
                    if (compiler.parsedAssemblyList[i].Contains("tjump"))
                    { /*all four cores share identical mgtrue bit layout!*/ mGTrue = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("tjump")) + 5), true, 5); }
                    #endregion

                    if (generate == true)
                    {
                        currentInstructionWord = mGFalse + " " + mGTrue + " " + mCond + " " + mImmediate + " " + mRamW + " " + mRamR + " " + mOpcode + " " + mDualReadToggle + " " + mExtension;
                        addToMachineCodeList(currentInstructionWord, i + 1);
                    }
                    fJumpDefault++;
                }
                // after machine code generation is complete, remove last line of machine code (as it's EOF and has all 0's written)
                machineCodeList.RemoveAt(machineCodeList.Count - 1);
                logging.logDebug("Machine code generation complete!", logging.loggingSenderID.machineCodeParser);
                if (Program.moreVerboseDebug) logging.logDebug("Dumping generated machine code ...", logging.loggingSenderID.machineCodeParser);
                // remove all whitespaces in machine code first!
                for (int i = 0; i < machineCodeList.Count; i++) machineCodeList[i] = functionLibrary.removeWhitespace(machineCodeList[i]);
                if (Program.moreVerboseDebug) for (int i = 0; i < machineCodeList.Count; i++) logging.logDebug("[" + (i + 1).ToString("D" + digits) + "]: " + machineCodeList[i] + " | len " + machineCodeList[i].Length.ToString(), logging.loggingSenderID.machineCodeParser);
            }
            #endregion
            #region RC4.0 machine code generation
            else if (Program.compileTarget == "2")
            {
                // instruction set bit layout for RC4.0 cores 1 - 4 (facing south)
                // [0  0  0  0  0  0  0 0 0 0 0 0 0 0 0 | 0 0 0 0 0 0 0 0 0  0  0  0  0  0  0] [0 0 0 0] [0 0 0 0  0  0] [0 0 0 0  0  0] [0 0 0 0] [0   0  0  0  0 0 0 0] [0 ] [0  0 0 0 0] [0  0 0 0 0] [0 0 0 0] [0] [0 0 0 0] [0 0 0 0] [0 0]
                // [15 14 13 12 11 10 9 8 7 6 5 4 3 2 1] [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15] [E 4 2 1] [1 2 4 8 16 32] [1 2 4 8 16 32] [1 2 4 8] [128 64 32 16 8 4 2 1] [E ] [16 8 4 2 1] [16 8 4 2 1] [1 2 4 8] [E] [1 2 4 8] [E 1 2 4] [2 1] (E = toggle bit, this bit will be used to toggle things)
                // [GPU GRANULAR X                     ] [GPU GRANULAR Y                     ] [CLOCKID] [GOTO TRUE    ] [GOTO FALSE   ] [TEST ID] [IMMEDIATE           ] [DR] [RAM READ  ] [RAM WRITE ] [OPCODE ] [M] [EXT.   ] [CACHE C] [ADR]
                // CLOCKID is ID of new clock speed if you want to change it; TEST ID is CONDITION; DR is dual-read toggle; M is mode toggle; CACHE C is cache control; ADR is cache address selector (0 based, so 0 is 1, 3 is 4)

                // var for incrementing line of code location if none provided (default)
                int fJumpDefault = 1; // on the first line of code, we jump to line 2
                bool generate = true; // if this is true, we will generated a line of code; if it's not (i.e. malloc for values returning 0), will not generate!

                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {
                    fJumpDefault++;

                    // init
                    #region Init vars
                    string currentInstructionWord = null;

                    // will break down machine code into different segments
                    string mGPUGX = "000000000000000";
                    string mGPUGY = "000000000000000";
                    string mClockID = "0000";
                    string mMode = "0";
                    string mCacheC = "0000";
                    string mCacheAddr = "00";
                    string mGFalse = "000000";
                    string mGTrue = "000000";
                    string mCond = "0000";
                    string mImmediate = "00000000";
                    string mRamW = "00000";
                    string mRamR = "00000";
                    string mOpcode = "0000";
                    string mDualReadToggle = "0";
                    string mExtension = "0000";
                    #endregion

                    // now find what function or thing we need to do
                    #region malloc opcode
                    if (compiler.parsedAssemblyList[i].Contains("%compiler%: malloc"))
                    {
                        // grab values
                        int memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("malloc") + 7);
                        int value = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].LastIndexOf("$") + 1);
                        // update machine code
                        if (generate == true)
                        {
                            mRamW = functionLibrary.convertNumberToBinary(memAddr, false, 5);
                            mImmediate = functionLibrary.convertNumberToBinary(value, false, 8);
                            // only jump that we should see with a malloc
                            if (compiler.parsedAssemblyList[i].Contains("fjump"))
                            {
                                // grab fjump location and convert it to binary
                                mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5), true, 6);
                            }
                        }
                    }
                    #endregion

                    #region lmalloc opcode
                    if (compiler.parsedAssemblyList[i].Contains("%compiler%: lmalloc"))
                    {
                        // grab values
                        int memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("lmalloc") + 9);
                        int value = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].LastIndexOf("$") + 1);
                        // update machine code
                        if (generate == true)
                        {
                            mRamW = functionLibrary.convertNumberToBinary(memAddr, false, 5);
                            mImmediate = functionLibrary.convertNumberToBinary(value, false, 8);
                            // only jump that we should see with a lmalloc
                            if (compiler.parsedAssemblyList[i].Contains("fjump"))
                            {
                                // grab fjump location and convert it to binary
                                mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5), true, 6);
                            }
                        }
                    }
                    #endregion

                    #region exit opcode
                    if (compiler.parsedAssemblyList[i].Contains("exit"))
                    {
                        // this opcode is easy!
                        mOpcode = "1001";
                    }
                    #endregion

                    #region flush opcode
                    if (compiler.parsedAssemblyList[i].Contains("flush"))
                    {
                        // grab flush reg
                        int regToFlush = acquireMemoryAddress(compiler.parsedAssemblyList[i], 5);
                        // convert regtoflush from value in code to value required in ext.
                        regToFlush += 2;

                        // build instruction word
                        mOpcode = "0011";

                        mExtension = functionLibrary.convertNumberToBinary(regToFlush, true, 4);
                    }
                    #endregion

                    #region output opcode
                    // syntax one
                    if (compiler.parsedAssemblyList[i].Contains("output1"))
                    {
                        // set opcode
                        mOpcode = "0101";

                        // grab var address
                        int memAddr = -1;
                        int extension = -1;
                        int immediate = -1;

                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$"));
                        }
                        else
                        {
                            // grab second number, will need to run interesting function here
                            int IDIndexCounter = 1;
                            TopOfFunction:
                            try
                            {
                                Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(7, IDIndexCounter));
                                IDIndexCounter++;
                                goto TopOfFunction;
                            }
                            catch (FormatException)
                            {
                                // do nothing
                            }
                            int IDIndexCounter2 = 1;
                            TopOfFunction2:
                            try
                            {
                                Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(7 + IDIndexCounter, IDIndexCounter2));
                                IDIndexCounter2++;
                                goto TopOfFunction2;
                            }
                            catch (FormatException)
                            {
                                // do nothing
                            }
                            immediate = acquireMemoryAddress(compiler.parsedAssemblyList[i], 7 + IDIndexCounter + (IDIndexCounter2 - IDIndexCounter - 1)); // will need to test this; quick tests show this works
                        }
                        extension = acquireMemoryAddress(compiler.parsedAssemblyList[i], 7);
                        if (memAddr == -1) { mRamR = "00000"; }
                        else { mRamR = functionLibrary.convertNumberToBinary(memAddr, false, 5); }

                        if (immediate != -1)
                        {
                            mImmediate = functionLibrary.convertNumberToBinary(immediate, false, 8);
                        }
                        mExtension = functionLibrary.convertNumberToBinary(extension + 1, true, 4);
                    }

                    // syntax two
                    if (compiler.parsedAssemblyList[i].Contains("output2"))
                    {
                        mOpcode = "0101";
                        mExtension = functionLibrary.convertNumberToBinary((acquireMemoryAddress(compiler.parsedAssemblyList[i], 7)) + 1, true, 4);
                    }
                    #endregion

                    #region input opcode
                    if (compiler.parsedAssemblyList[i].Contains("input"))
                    {
                        mOpcode = "1101";
                        // by default data will be pushed onto 
                        // gather data
                        int inputSocket = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("input") + 5);
                        int memAddr = 0;
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$"));
                        }

                        // will have to update inputsocket for this RC's IS (instruction set)
                        //logging.logDebug("inputsocket is " + inputSocket.ToString());
                        if (inputSocket == 6) { inputSocket = 5; }
                        else if (inputSocket == 5) { inputSocket = 7; }
                        else if (inputSocket == 4) { inputSocket = 6; }
                        mExtension = functionLibrary.convertNumberToBinary(inputSocket, true, 4);
                        if (memAddr != 0)
                        {
                            mRamW = functionLibrary.convertNumberToBinary(memAddr, false, 5);
                        }
                    }

                    #endregion

                    #region sleep opcode
                    // literally do nothing
                    if (compiler.parsedAssemblyList[i].Contains("sleep")) { /*do nothing*/ }
                    #endregion

                    #region bsr opcode
                    if (compiler.parsedAssemblyList[i].Contains("bsr"))
                    {
                        // generate opcode
                        mOpcode = "1010";
                        mDualReadToggle = "1";

                        // grab address to modify
                        // since this is bsr, and we have an architectual limit where we only can access bsr on cache b, change this to read cache b! (mramw)
                        mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                    }
                    #endregion

                    #region bsl opcode
                    if (compiler.parsedAssemblyList[i].Contains("bsl"))
                    {
                        // generate opcode
                        mOpcode = "0010";

                        // grab address to modify
                        mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("$"))), false, 5);
                    }
                    #endregion

                    #region ad1 opcode
                    if (compiler.parsedAssemblyList[i].Contains("ad1"))
                    {
                        // ad1 instruction
                        // opcode
                        mOpcode = "1000";

                        // mem addr
                        // we have this statement here because ad1 temp[x] temp[x] instructions can be generated
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                        }
                        // immediate value
                        mImmediate = "00000001";
                    }
                    #endregion

                    #region sb1 opcode
                    if (compiler.parsedAssemblyList[i].Contains("sb1"))
                    {
                        // opcode
                        mOpcode = "0100";
                        // mem addr
                        // we have this statement here because sb1 temp[x] temp[x] instructions can be generated
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                        }
                        // immediate value
                        mImmediate = "00000001";
                    }
                    #endregion

                    #region not opcode
                    if (compiler.parsedAssemblyList[i].Contains("not"))
                    {
                        // opcode
                        mOpcode = "1100";
                        // mem addr
                        mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                    }
                    #endregion

                    #region add opcode
                    if (compiler.parsedAssemblyList[i].Contains("add"))
                    {
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("add"), 4);
                        string block1 = null;
                        string block2 = null;
                        string block3 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block3 += currentChar;
                        while (readLoc != temp.Length)
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block3 += currentChar;
                        }
                        // WILL ALSO NEED TO GENERATE MACHINE CODE FOR WRITEBACK IF APPROPRIATE! [DEPRECATED!]
                        // only case where it would not be appropriate if a test instruction testing SO or SU is next

                        // opcode
                        mOpcode = "1000";

                        // actual add instruction
                        mDualReadToggle = "1";

                        // because of the RC4.0 architecture, we'll need to detect if either block is an
                        // immediate int. If it is, then we have to set that as immediate value, and have
                        // the other value become mramr
                        // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                        // checker already checks that and takes care of that for us (although the error produced 
                        // is a bit misleading)
                        // also b/c of the RC4.0 architecture and the way it handles immediate values
                        // (piped into cache b instead of cache a like the RC3.0), we will have to change this slightly
                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);

                            // then set other block to mramr
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                            // then set other block to mramr
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                            mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                        }

                    }
                    #endregion

                    #region subtract opcode
                    // pretty much pasted from add opcode, too lazy to do otherwise
                    if (compiler.parsedAssemblyList[i].Contains("sub"))
                    {
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("sub"), 4);
                        string block1 = null;
                        string block2 = null;
                        string block3 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block3 += currentChar;
                        while (readLoc != temp.Length)
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block3 += currentChar;
                        }
                        // WILL ALSO NEED TO GENERATE MACHINE CODE FOR WRITEBACK IF APPROPRIATE! [DEPRECATED!]
                        // only case where it would not be appropriate if a test instruction testing SO or SU is next

                        // opcode
                        mOpcode = "0100";

                        // actual add instruction
                        mDualReadToggle = "1";

                        // because of the RC4.0 architecture, we'll need to detect if either block is an
                        // immediate int. If it is, then we have to set that as immediate value, and have
                        // the other value become mramr
                        // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                        // checker already checks that and takes care of that for us (although the error produced 
                        // is a bit misleading)
                        // also b/c of the RC4.0 architecture and the way it handles immediate values
                        // (piped into cache b instead of cache a like the RC3.0), we will have to change this slightly
                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);

                            // then set other block to mramr
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                            // then set other block to mramr
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                            mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                        }

                        //mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, (block2.IndexOf("$"))), false, 5);
                        //mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, (block1.IndexOf("$"))), false, 5);

                    }
                    #endregion

                    #region RC4.0 specific functions

                    #region gpu encode opcode
                    if (compiler.parsedAssemblyList[i].Contains("gpuencode"))
                    {
                        // opcode
                        mOpcode = "0110";
                        mDualReadToggle = "1";

                        // load two numbers in
                        // will have to do it this way b/c we can have either $x$ #, # $x$, or $x$ $x$
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("gpuencode"), 10);
                        string block1 = null;
                        string block2 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (readLoc != temp.Length)
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        // block1 will be ramr, block2 will be ramw
                        if (block1.Contains("$"))
                        {
                            // grab address
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                        }
                        else
                        {
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);
                        }
                        if (block2.Contains("$"))
                        {
                            mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                        }
                        else
                        {
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                        }
                    }
                    #endregion

                    #region gpu clear opcode
                    if (compiler.parsedAssemblyList[i].Contains("gpuclr"))
                    {
                        // easy!
                        mOpcode = "1000";
                        mMode = "1";
                    }
                    #endregion

                    #region gpu draw opcode
                    if (compiler.parsedAssemblyList[i].Contains("gpudraw"))
                    {
                        mOpcode = "0100";
                        mMode = "1";
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            // we know it's a var syntax
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                        }
                        else
                        {
                            // we know it's 2 immediate numbers syntax
                            // load two numbers in
                            string num1 = null;
                            string num2 = null;
                            num1 = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], 7), true, 4);
                            int nextStartLoc = compiler.parsedAssemblyList[i].IndexOf(" ");
                            nextStartLoc = compiler.parsedAssemblyList[i].IndexOf(" ", nextStartLoc + 1);
                            num2 = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], nextStartLoc), true, 4);
                            mImmediate = num2 + num1;
                        }
                    }
                    #endregion

                    #region gpu erase opcode
                    if (compiler.parsedAssemblyList[i].Contains("gpuerase"))
                    {
                        mOpcode = "1100";
                        mMode = "1";
                        if (compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            // we know it's a var syntax
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                        }
                        else
                        {
                            // we know it's 2 immediate numbers syntax
                            // load two numbers in
                            string num1 = null;
                            string num2 = null;
                            num1 = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], 8), true, 4);
                            int nextStartLoc = compiler.parsedAssemblyList[i].IndexOf(" ");
                            nextStartLoc = compiler.parsedAssemblyList[i].IndexOf(" ", nextStartLoc + 1);
                            num2 = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], nextStartLoc), true, 4);
                            mImmediate = num2 + num1;
                        }
                    }
                    #endregion

                    #region gpu granular update opcode
                    if (compiler.parsedAssemblyList[i].Contains("gpugu"))
                    {
                        mOpcode = "0010";
                        mMode = "1";

                        // this will be interesting
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("gpugu"), 6);
                        string block1 = null;
                        string block2 = null;
                        string block3 = null;
                        string block4 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block3 += currentChar;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block3 += currentChar;
                        }
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block4 += currentChar;
                        while (readLoc != temp.Length)
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block4 += currentChar;
                        }
                        // hex stuff
                        block1 = block1.Replace("0x", "");
                        block2 = block2.Replace("0x", "");
                        int hexToInt = int.Parse(block1, System.Globalization.NumberStyles.HexNumber);
                        mGPUGX = functionLibrary.convertNumberToBinary(hexToInt, false, 15);
                        hexToInt = int.Parse(block2, System.Globalization.NumberStyles.HexNumber);
                        mGPUGY = functionLibrary.convertNumberToBinary(hexToInt, false, 15);
                        // bool stuff
                        string drawPart = "01";
                        string fillX = "0";
                        string fillY = "0";
                        if (block3.Contains("1")) { fillX = "1"; }
                        if (block4.Contains("1")) { fillY = "1"; }
                        mExtension = drawPart + fillX + fillY;

                    }
                    #endregion

                    #region update clock speed opcode
                    if (compiler.parsedAssemblyList[i].Contains("updspd"))
                    {
                        // don't even need an opcode!
                        // grab clock id
                        string clockID = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], 6), false, 3);
                        mClockID = "1" + clockID;
                    }
                    #endregion

                    #endregion

                    // WILL ALSO NEED TO ADD IN A=0 / B=0 TESTS!
                    #region test instruction
                    if (compiler.parsedAssemblyList[i].Contains("test"))
                    {
                        // grab what we're testing
                        // list of things we can test:
                        // SO - shift overflow
                        // SU - shift underflow
                        // <, >, ==
                        // user input 1, 2, 3, 4
                        if (compiler.parsedAssemblyList[i].Contains("%so%"))
                        {
                            // test ID
                            mCond = "1000";
                            // all we need to do! extremely easy! :D (hopefully)
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("%su%"))
                        {
                            // test ID
                            mCond = "0100";
                            // all we need to do! extremely easy! :D (hopefully)
                        }
                        // hardware conditions specific to RC4.0
                        else if (compiler.parsedAssemblyList[i].Contains("%rng_on%")) { mCond = "0011"; }
                        else if (compiler.parsedAssemblyList[i].Contains("=="))
                        {
                            // test ID
                            mCond = "1100";
                            mOpcode = "0001";
                            mDualReadToggle = "1";
                            // load two numbers in
                            // will have to grab blocks
                            string temp = compiler.parsedAssemblyList[i];
                            if (temp.IndexOf("test") != 0)
                            {
                                // remove text before
                                temp = temp.Remove(0, compiler.parsedAssemblyList[i].IndexOf("test"));
                            }
                            temp = temp.Remove(temp.IndexOf("test"), 8);
                            string block1 = null;
                            string block2 = null;
                            int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block1 += currentChar;
                            }
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block2 += currentChar;
                            }
                            // because of the RC4.0 architecture, we'll need to detect if either block is an
                            // immediate int. If it is, then we have to set that as immediate value, and have
                            // the other value become mramr
                            // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                            // checker already checks that and takes care of that for us (although the error produced 
                            // is a bit misleading)
                            // also b/c of the RC4.0 architecture and the way it handles immediate values
                            // (piped into cache a instead of cache b like the RC3.0), we will have to change this slightly
                            if (!block1.Contains("$"))
                            {
                                // block1 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);

                                // then set other block to mramr
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                            }
                            else if (!block2.Contains("$"))
                            {
                                // block2 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                                // then set other block to mramr
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                            }
                            else
                            {
                                // convert both blocks to mramr and mramw
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                            }

                        }
                        else if (compiler.parsedAssemblyList[i].Contains(">"))
                        {
                            // test ID
                            mCond = "0010";
                            mOpcode = "0001";
                            mDualReadToggle = "1";
                            // will perform same checks as ==, check that method for more info
                            // load two numbers in
                            // will have to grab blocks
                            string temp = compiler.parsedAssemblyList[i];
                            if (temp.IndexOf("test") != 0)
                            {
                                // remove text before
                                temp = temp.Remove(0, compiler.parsedAssemblyList[i].IndexOf("test"));
                            }
                            temp = temp.Remove(temp.IndexOf("test"), 7);
                            string block1 = null;
                            string block2 = null;
                            int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block1 += currentChar;
                            }
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block2 += currentChar;
                            }
                            // because of the RC4.0 architecture, we'll need to detect if either block is an
                            // immediate int. If it is, then we have to set that as immediate value, and have
                            // the other value become mramr
                            // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                            // checker already checks that and takes care of that for us (although the error produced 
                            // is a bit misleading)
                            // also b/c of the RC4.0 architecture and the way it handles immediate values
                            // (piped into cache a instead of cache b like the RC3.0), we will have to change this slightly
                            if (!block1.Contains("$"))
                            {
                                // block1 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);

                                // then set other block to mramr
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                            }
                            else if (!block2.Contains("$"))
                            {
                                // block2 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                                // then set other block to mramr
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                            }
                            else
                            {
                                // convert both blocks to mramr and mramw
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                            }
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("<"))
                        {
                            // test ID
                            mCond = "1010";
                            mOpcode = "0001";
                            mDualReadToggle = "1";
                            // will perform same checks as ==, check that method for more info

                            // load two numbers in
                            // will have to grab blocks
                            string temp = compiler.parsedAssemblyList[i];
                            if (temp.IndexOf("test") != 0)
                            {
                                // remove text before
                                temp = temp.Remove(0, compiler.parsedAssemblyList[i].IndexOf("test"));
                            }
                            temp = temp.Remove(temp.IndexOf("test"), 7);
                            string block1 = null;
                            string block2 = null;
                            int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block1 += currentChar;
                            }
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block2 += currentChar;
                            }
                            // because of the RC4.0 architecture, we'll need to detect if either block is an
                            // immediate int. If it is, then we have to set that as immediate value, and have
                            // the other value become mramr
                            // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                            // checker already checks that and takes care of that for us (although the error produced 
                            // is a bit misleading)
                            // also b/c of the RC4.0 architecture and the way it handles immediate values
                            // (piped into cache a instead of cache b like the RC3.0), we will have to change this slightly
                            if (!block1.Contains("$"))
                            {
                                // block1 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 8);

                                // then set other block to mramr
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                            }
                            else if (!block2.Contains("$"))
                            {
                                // block2 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 8);
                                // then set other block to mramr
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                            }
                            else
                            {
                                // convert both blocks to mramr and mramw
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, block1.IndexOf("$")), false, 5);
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, block2.IndexOf("$")), false, 5);
                            }
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("ui"))
                        {
                            // collect which one it is
                            if (compiler.parsedAssemblyList[i].Contains("ui0"))
                            {
                                mCond = "0001";
                                // easy! :D
                            }
                            else if (compiler.parsedAssemblyList[i].Contains("ui1"))
                            {
                                mCond = "1001";
                                // easy! :D
                            }
                            else if (compiler.parsedAssemblyList[i].Contains("ui2"))
                            {
                                mCond = "0101";
                                // easy! :D
                            }
                            else if (compiler.parsedAssemblyList[i].Contains("ui3"))
                            {
                                mCond = "1101";
                                // easy! :D
                            }
                        }
                    }
                    #endregion

                    #region ramw (ONLY) component
                    if (compiler.parsedAssemblyList[i].Contains("ramw") && !compiler.parsedAssemblyList[i].Contains("ramr"))
                    {
                        // will assume that [ramw $address$ temp] is only case of this, so we will keep it that way
                        mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                    }
                    #endregion

                    #region ramr + ramw component (for copying values)
                    // can be optimized
                    if (compiler.parsedAssemblyList[i].Contains("ramr") && compiler.parsedAssemblyList[i].Contains("ramw"))
                    {
                        // grab two memory addresses needed
                        mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                        int nextStartLoc = compiler.parsedAssemblyList[i].IndexOf("$");
                        nextStartLoc = compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1);
                        mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1)), false, 5);

                        // will also need to have an opcode to have value pass through ALU to get back to RAM; add will be sufficient
                        mOpcode = "1000";
                    }
                    #endregion

                    #region fjump component
                    if (compiler.parsedAssemblyList[i].Contains("fjump"))
                    {
                        mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("fjump")) + 5), true, 6);
                    }
                    else
                    {
                        if (mGFalse == "000000" && generate == true)
                        {
                            mGFalse = functionLibrary.convertNumberToBinary(fJumpDefault, true, 6);
                        }
                    }
                    #endregion

                    #region tjump component
                    if (compiler.parsedAssemblyList[i].Contains("tjump"))
                    {
                        mGTrue = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("tjump")) + 5), true, 6);
                    }
                    #endregion

                    if (generate == true)
                    {
                        currentInstructionWord = mGPUGX + " " + mGPUGY + " " + mClockID + " " + mGTrue + " " + mGFalse + " " + mCond + " " + mImmediate + " " + mDualReadToggle + " " + mRamR + " " + mRamW + " " + mOpcode + " " + mMode + " " + mExtension + " " + mCacheC + " " + mCacheAddr;
                        addToMachineCodeList(currentInstructionWord, i + 1);
                    }
                }
                // after machine code generation is complete, remove last line of machine code (as it's EOF and has all 0's written)
                machineCodeList.RemoveAt(machineCodeList.Count - 1);
                logging.logDebug("Machine code generation complete!", logging.loggingSenderID.machineCodeParser);
                if (Program.moreVerboseDebug) logging.logDebug("Dumping generated machine code ...", logging.loggingSenderID.machineCodeParser);
                // remove all whitespaces in machine code first!
                for (int i = 0; i < machineCodeList.Count; i++) machineCodeList[i] = functionLibrary.removeWhitespace(machineCodeList[i]);
                if (Program.moreVerboseDebug) for (int i = 0; i < machineCodeList.Count; i++) logging.logDebug("[" + (i + 1).ToString("D" + digits) + "]: " + machineCodeList[i] + " | len " + machineCodeList[i].Length.ToString(), logging.loggingSenderID.machineCodeParser);
            }
            #endregion
            #region RC5.0 machine code generation
            else if (Program.compileTarget == "3")
            {
                // instruction set bit layout for RC5.0 cores 1 - 4 (facing east)
                // [0 0 0 0 0  0  0 ] [0 0 0 0 0  0  0 ] [0 0 0 0] [0 0 0 0] [0 0 0 0 0 ] [0] [0 0 0 0 0 ] [0] [0 0 0 0 0 ] [0] [0 0 0 0 0  0  0  0  ]
                // [1 2 4 8 16 32 64] [1 2 4 8 16 32 64] [1 2 4 8] [1 2 4 8] [1 2 4 8 16] [1] [1 2 4 8 16] [1] [1 2 4 8 16] [1] [1 2 4 8 16 32 64 128]
                // [GOTO FALSE      ] [GOTO TRUE       ] [EXT.   ] [OPCODE ] [SEL. BITS ] [F] [RAM READ  ] [R] [RAM WRITE ] [W] [IMMEDIATE           ]
                // F is FORWRDING TOGGLE, R is READ RAM/CACHE TOGGLE, W is WRITE RAM/CACHE TOGGLE

                // var for incrementing line of code location if none provided (default)
                int fJumpDefault = 0; // on the first line of code, we jump to line 1
                bool generate = true; // if this is true, we will generated a line of code; if it's not (i.e. malloc for values returning 0), will not generate!

                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {
                    fJumpDefault++;

                    // init
                    #region Init vars
                    string currentInstructionWord = null;

                    // break down into separate segments
                    string mGFalse = "0000000";
                    string mGTrue = "0000000";
                    string mExtension = "0000";
                    string mOpcode = "0000";
                    string mSelectBit1 = "0"; string mSelectBit2 = "0"; string mSelectBit3 = "0"; string mSelectBit4 = "0"; string mSelectBit5 = "0";
                    string mToggleForward = "0";
                    string mRead = "00000";
                    string mReadToggle = "1"; // 0 is cache, 1 is RAM
                    string mWrite = "00000";
                    string mWriteToggle = "1"; // 0 is cache, 1 is RAM
                    string mImmediate = "00000000";
                    #endregion

                    // now find what function or thing we need to do
                    #region malloc opcode
                    if (compiler.parsedAssemblyList[i].Contains("%compiler%: malloc"))
                    {
                        // grab values
                        int memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("malloc") + 7);
                        int value = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].LastIndexOf("$") + 1);
                        // update machine code
                        if (generate == true)
                        {
                            mWrite = functionLibrary.convertNumberToBinary(memAddr, true, 5);
                            mImmediate = functionLibrary.convertNumberToBinary(value, true, 8);
                            // only jump that we should see with a malloc
                            if (compiler.parsedAssemblyList[i].Contains("fjump"))
                            {
                                // grab fjump location and convert it to binary
                                mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5), true, 7);
                            }
                        }
                    }
                    #endregion

                    #region lmalloc opcode
                    if (compiler.parsedAssemblyList[i].Contains("%compiler%: lmalloc"))
                    {
                        // grab values
                        int memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("lmalloc") + 9);
                        int value = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].LastIndexOf("$") + 1);
                        // update machine code
                        if (generate == true)
                        {
                            // for writing to cache, need leftmost bit set on mWrite + inverted mWrite address
                            mWrite = "1" + functionLibrary.convertNumberToBinary(memAddr, false, 4);
                            mImmediate = functionLibrary.convertNumberToBinary(value, true, 8);
                            mWriteToggle = "0"; // to denote caching
                            // only jump that we should see with a lmalloc
                            if (compiler.parsedAssemblyList[i].Contains("fjump"))
                            {
                                // grab fjump location and convert it to binary
                                mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5), true, 7);
                            }
                        }
                    }
                    #endregion

                    #region exit opcode
                    if (compiler.parsedAssemblyList[i].Contains("exit"))
                    {
                        // COFF w/ global off
                        mSelectBit1 = "1";
                        mOpcode = "1100";
                        mExtension = "1001";
                    }
                    #endregion

                    #region output opcode
                    if (compiler.parsedAssemblyList[i].Contains("output"))
                    {
                        // no opcode - memory based!
                        // acquire both blocks
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("output"), 7);
                        string block1 = null;
                        string block2 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        block1 = functionLibrary.removeWhitespace(block1);
                        block2 = functionLibrary.removeWhitespace(block2);

                        // target specific code
                        mSelectBit4 = "1";
                        if (block2 == "%dp") { mWrite = "11000"; }
                        else if (block2 == "%dec") { mWrite = "00100"; }
                        else if (block2 == "%raw") { mWrite = "10100"; }
                        else if (block2 == "%out") { mWrite = "01100"; }
                        else if (block2 == "%gt_") { mWrite = "01000"; }
                        else if (block2 == "%spd") { mWrite = "10000"; }
                        else if (block2 == "%stk") { mWrite = "10010"; }
                        else if (block2 == "%gex") { mWrite = "00010"; }

                        // global var
                        if (block1.Contains("$") && block1.Count(x => x == '$') == 2)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                        // local var
                        // if we have cache, need to have add or sub opcode to route cache data to output
                        else if (block1.Contains("$") && block1.Count(x => x == '$') == 4)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; mOpcode = "1000"; }
                        else if (!block1.Contains("$"))
                        { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8); }
                    }
                    #endregion

                    #region input opcode
                    if (compiler.parsedAssemblyList[i].Contains("input") && !compiler.parsedAssemblyList[i].Contains("test")) // test input syntax will be located under test opcode
                    {
                        // no opcode - memory based!
                        // acquire both blocks
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("input"), 6);
                        string block1 = null;
                        string block2 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        block1 = functionLibrary.removeWhitespace(block1);
                        block2 = functionLibrary.removeWhitespace(block2);

                        // target specific code
                        mSelectBit3 = "1";
                        mSelectBit5 = "1";
                        if (block1 == "%dp_") { mRead = "11000"; }
                        else if (block1 == "%ui_") { mRead = "00100"; }
                        else if (block1 == "%rng") { mRead = "10100"; }
                        else if (block1 == "%stk") { mRead = "01100"; }
                        else if (block1 == "%gt_") { mRead = "01000"; }
                        else if (block1 == "%spd") { mRead = "10000"; }

                        // global var
                        if (block2.Contains("$") && block2.Count(x => x == '$') == 2)
                        { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                        // local var
                        else if (block2.Contains("$") && block2.Count(x => x == '$') == 4)
                        { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), false, 4); mWriteToggle = "0"; }
                    }
                    #endregion

                    #region sleep opcode
                    // literally nothing
                    if (compiler.parsedAssemblyList[i].Contains("sleep")) { /*do nothing*/ }
                    #endregion

                    #region bsr opcode
                    if (compiler.parsedAssemblyList[i].Contains("bsr"))
                    {
                        mOpcode = "1010";
                        if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 2)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5); }
                        else if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 4)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), true, 4) + "1"; mReadToggle = "0"; }
                    }
                    #endregion

                    #region bsl opcode
                    if (compiler.parsedAssemblyList[i].Contains("bsl"))
                    {
                        mOpcode = "0010";
                        if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 2)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5); }
                        else if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 4)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), true, 4) + "1"; mReadToggle = "0"; }
                    }
                    #endregion

                    #region ad1 opcode
                    if (compiler.parsedAssemblyList[i].Contains("ad1"))
                    {
                        mImmediate = "10000000";
                        mOpcode = "1000";
                        // pipe variable into input A
                        if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 2)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5); }
                        else if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 4)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), true, 4) + "1"; mReadToggle = "0"; }
                        // pipe immediate value into input B
                        mWrite = "10000";
                    }
                    #endregion

                    #region sb1 opcode
                    if (compiler.parsedAssemblyList[i].Contains("sb1"))
                    {
                        mImmediate = "10000000";
                        mOpcode = "0100";
                        // pipe variable into input A
                        if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 2)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5); }
                        else if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 4)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), true, 4) + "1"; mReadToggle = "0"; }
                        // pipe immediate value into input B
                        mWrite = "10000";
                    }
                    #endregion

                    #region not opcode
                    if (compiler.parsedAssemblyList[i].Contains("not"))
                    {
                        mOpcode = "0110";
                        if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 2)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5); }
                        else if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 4)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), true, 4) + "1"; mReadToggle = "0"; }
                    }
                    #endregion

                    #region add opcode
                    if (compiler.parsedAssemblyList[i].Contains("add"))
                    {
                        // 3 possibilities: [add var var], [add var num], and [add num var]
                        // writeback can be: ram, cache, ~temp~ - currently ignored
                        mOpcode = "1000";
                        // grab 3 blocks
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("add"), 4);
                        string block1 = null;
                        string block2 = null;
                        string block3 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block3 += currentChar;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block3 += currentChar;
                        }
                        block1 = functionLibrary.removeWhitespace(block1);
                        block2 = functionLibrary.removeWhitespace(block2);
                        block3 = functionLibrary.removeWhitespace(block3); // currently ignored
                        if (!block1.Contains("$") && block1 != "~temp~")
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8);

                            // then set other block to mRead
                            if (block2.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8);
                            // then set other block to mramr
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            mSelectBit2 = "1";
                            if (block1 != "~temp~") // temp fix for this, most likely will need more comprehensive fix in the future
                            {
                                if (block1.Count(x => x == '$') == 2)
                                { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                                else if (block1.Count(x => x == '$') == 4)
                                { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                            }

                            if (block2.Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), false, 4); mWriteToggle = "0"; }
                        }
                    }
                    #endregion

                    #region subtract opcode
                    if (compiler.parsedAssemblyList[i].Contains("sub"))
                    {
                        // 3 possibilities: [sub var var], [sub var num], and [sub num var]
                        // writeback can be: ram, cache, ~temp~ - currently ignored
                        mOpcode = "0100";
                        // grab 3 blocks
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("sub"), 4);
                        string block1 = null;
                        string block2 = null;
                        string block3 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        readLoc++;
                        currentChar = (char)sr.Read();
                        block3 += currentChar;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block3 += currentChar;
                        }
                        block1 = functionLibrary.removeWhitespace(block1);
                        block2 = functionLibrary.removeWhitespace(block2);
                        block3 = functionLibrary.removeWhitespace(block3); // currently ignored
                        if (!block1.Contains("$") && block1 != "~temp~")
                        {
                            // pipe immediate into input A, var into input B
                            mSelectBit2 = "1"; // need dual-read to perform above task
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8); mRead = "00001"; mReadToggle = "0";

                            // then set other block to mWrite b/c dual-read is toggled
                            if (block2.Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), false, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), false, 4) + "0"; mWriteToggle = "0"; }
                        }
                        else if (!block2.Contains("$"))
                        {
                            // pipe immediate into input B, var into input A
                            // default inputs, so don't need to do anything fancy
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8); mWrite = "10000"; mWriteToggle = "0";
                            // then set other block to mramr
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "0"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            mSelectBit2 = "1";
                            if (block1 != "~temp~") // temp fix for this, most likely will need more comprehensive fix in the future
                            {
                                if (block1.Count(x => x == '$') == 2)
                                { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                                else if (block1.Count(x => x == '$') == 4)
                                { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                            }

                            if (block2.Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), false, 4); mWriteToggle = "0"; }
                        }

                    }
                    #endregion

                    #region RC5.0 specific functions

                    #region gpu encode opcode
                    if (compiler.parsedAssemblyList[i].Contains("gpuencode"))
                    {
                        mOpcode = "0011";
                        // similar to add opcode but only 2 blocks
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("gpuencode"), 10);
                        string block1 = null;
                        string block2 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        block1 = functionLibrary.removeWhitespace(block1);
                        block2 = functionLibrary.removeWhitespace(block2);
                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8);

                            // then set other block to mRead
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8);
                            // then set other block to mramr
                            if (block2.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            mSelectBit2 = "1";
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }

                            if (block2.Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), false, 4); mWriteToggle = "0"; }
                        }

                    }
                    #endregion

                    #region gpu clear opcode
                    if (compiler.parsedAssemblyList[i].Contains("gpuclr"))
                    { mOpcode = "0010"; mSelectBit1 = "1"; }
                    #endregion

                    #region gpu draw opcode
                    if (compiler.parsedAssemblyList[i].Contains("gpudraw"))
                    {
                        mOpcode = "1011";
                        // if two numbers, encode them on-the-fly and pipe into immediate
                        // if variable, write variable to gpu point register (inaccessible through ARCISS-HLL)
                        if (!compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            // similar to add opcode but only 2 blocks
                            string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("gpudraw"), 8);
                            string block1 = null;
                            string block2 = null;
                            int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block1 += currentChar;
                            }
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block2 += currentChar;
                            }
                            block1 = functionLibrary.removeWhitespace(block1);
                            block2 = functionLibrary.removeWhitespace(block2);
                            // switched b/c target specifically requires it
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 4) + functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 4);
                        }
                        else
                        {
                            if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5); }
                            else if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), true, 4) + "1"; mReadToggle = "0"; }
                        }

                        mWrite = "11100";
                        mSelectBit4 = "1";
                    }
                    #endregion

                    #region gpu erase opcode
                    if (compiler.parsedAssemblyList[i].Contains("gpuerase"))
                    {
                        // exactly the same as gpudraw except with different opcode
                        mOpcode = "0111";
                        // if two numbers, encode them on-the-fly and pipe into immediate
                        // if variable, write variable to gpu point register (inaccessible through ARCISS-HLL)
                        if (!compiler.parsedAssemblyList[i].Contains("$"))
                        {
                            // similar to add opcode but only 2 blocks
                            string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("gpuerase"), 9);
                            string block1 = null;
                            string block2 = null;
                            int readLoc = 0;
                            StringReader sr = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block1 += currentChar;
                            }
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr.Read();
                                block2 += currentChar;
                            }
                            block1 = functionLibrary.removeWhitespace(block1);
                            block2 = functionLibrary.removeWhitespace(block2);
                            // switched b/c target specifically requires it
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), false, 4) + functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), false, 4);
                        }
                        else
                        {
                            if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5); }
                            else if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), true, 4) + "1"; mReadToggle = "0"; }
                        }

                        mWrite = "11100";
                        mSelectBit4 = "1";
                    }
                    #endregion

                    #region gpu granular update opcode
                    // most of this functionality is handled via writing to %gex register AKA output opcode
                    // this is only for actually updating the display with the %gex register data
                    if (compiler.parsedAssemblyList[i].Contains("gpugu")) { mOpcode = "1111"; }
                    #endregion

                    #region core on opcode
                    if (compiler.parsedAssemblyList[i].Contains("con"))
                    {
                        mOpcode = "0100";
                        mSelectBit1 = "1";
                        mExtension = functionLibrary.convertNumberToBinary(Convert.ToInt16(functionLibrary.removeWhitespace(compiler.parsedAssemblyList[i].Substring(4))) + 1, true, 4);
                    }
                    #endregion

                    #region core off opcode
                    if (compiler.parsedAssemblyList[i].Contains("coff"))
                    {
                        mOpcode = "1100";
                        mSelectBit1 = "1";
                        mExtension = functionLibrary.convertNumberToBinary(Convert.ToInt16(functionLibrary.removeWhitespace(compiler.parsedAssemblyList[i].Substring(5))) + 5, true, 4);
                    }
                    #endregion

                    #region xor opcode
                    if (compiler.parsedAssemblyList[i].Contains("xor "))
                    {
                        mOpcode = "1110";
                        // grab two blocks
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("xor "), 4);
                        string block1 = null;
                        string block2 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        block1 = functionLibrary.removeWhitespace(block1);
                        block2 = functionLibrary.removeWhitespace(block2);

                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8);

                            // then set other block to mRead
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8);
                            // then set other block to mramr
                            if (block2.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            mSelectBit2 = "1";
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }

                            if (block2.Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), false, 4); mWriteToggle = "0"; }
                        }
                    }
                    #endregion

                    #region xnor opcode
                    else if (compiler.parsedAssemblyList[i].Contains("xnor "))
                    {
                        // xor [var] [var], xor [var] [num], xor [num] [var]
                        mOpcode = "0001";
                        // grab two blocks
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("xnor "), 5);
                        string block1 = null;
                        string block2 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        block1 = functionLibrary.removeWhitespace(block1);
                        block2 = functionLibrary.removeWhitespace(block2);
                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8);

                            // then set other block to mRead
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8);
                            // then set other block to mramr
                            if (block2.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            mSelectBit2 = "1";
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }

                            if (block2.Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), false, 4); mWriteToggle = "0"; }
                        }
                    }
                    #endregion

                    #region nor opcode
                    else if (compiler.parsedAssemblyList[i].Contains("nor "))
                    {
                        mOpcode = "1101";
                        // grab two blocks
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("nor "), 4);
                        string block1 = null;
                        string block2 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        block1 = functionLibrary.removeWhitespace(block1);
                        block2 = functionLibrary.removeWhitespace(block2);
                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8);

                            // then set other block to mRead
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8);
                            // then set other block to mramr
                            if (block2.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            mSelectBit2 = "1";
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                            if (block2.Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), false, 4); mWriteToggle = "0"; }
                        }
                    }
                    #endregion

                    #region or opcode
                    else if (compiler.parsedAssemblyList[i].Contains("or "))
                    {
                        mOpcode = "0101";
                        // grab two blocks
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("or "), 3);
                        string block1 = null;
                        string block2 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        block1 = functionLibrary.removeWhitespace(block1);
                        block2 = functionLibrary.removeWhitespace(block2);

                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8);

                            // then set other block to mRead
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8);
                            // then set other block to mramr
                            if (block2.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            mSelectBit2 = "1";
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                            if (block2.Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), false, 4); mWriteToggle = "0"; }
                        }
                    }
                    #endregion

                    #region and opcode
                    else if (compiler.parsedAssemblyList[i].Contains("and "))
                    {
                        mOpcode = "1001";
                        // grab two blocks
                        string temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("and "), 4);
                        string block1 = null;
                        string block2 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        block1 = functionLibrary.removeWhitespace(block1);
                        block2 = functionLibrary.removeWhitespace(block2);
                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8);

                            // then set other block to mRead
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8);
                            // then set other block to mramr
                            if (block2.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            mSelectBit2 = "1";
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                            if (block2.Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), false, 4); mWriteToggle = "0"; }
                        }
                    }
                    #endregion

                    #endregion

                    #region opcodes for function calls
                    #region fcall1 opcode
                    if (compiler.parsedAssemblyList[i].Contains("fcall1"))
                    {
                        // add opcode with read from %gt_ and immediate 1
                        mOpcode = "1000";
                        mSelectBit3 = "1";
                        mRead = "01000"; //mReadToggle = "1";
                        mWrite = "10000"; mWriteToggle = "0";
                        mImmediate = "10000000";
                    }
                    #endregion
                    #region fcall2 opcode
                    if (compiler.parsedAssemblyList[i].Contains("fcall2"))
                    {
                        // push ALU output to stack
                        mSelectBit4 = "1";
                        mWrite = "10010"; mWriteToggle = "1";
                        // also contains fjump component
                    }
                    #endregion
                    #region rcall1 opcode
                    if (compiler.parsedAssemblyList[i].Contains("rcall1"))
                    {
                        // read return variable and write it to ALU
                        // grab address to read
                        string addr = functionLibrary.removeWhitespace(compiler.parsedAssemblyList[i].Substring(19));
                        if (addr.Count(x => x == '$') == 2)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(addr, 0), true, 5); }
                        else if (addr.Count(x => x == '$') == 4)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(addr, 1), true, 4) + "1"; mReadToggle = "0"; }
                        // write to ALU output register via add opcode
                        mOpcode = "1000";
                    }
                    #endregion
                    #region rcall2 opcode
                    if (compiler.parsedAssemblyList[i].Contains("rcall2"))
                    {
                        // pop stack value back to goto of running core
                        // forwarding it all the way around the master bus, may take some time to be received by ALU
                        mSelectBit3 = "1"; mSelectBit5 = "1";
                        mOpcode = "1000";
                        mRead = "01100"; mReadToggle = "1";
                    }
                    #endregion
                    #endregion

                    #region cmp instruction
                    if (compiler.parsedAssemblyList[i].Contains("cmp "))
                    {
                        // potentially create optimization for cases where cmp instruction isn't needed because data being tested is already loaded into ALU registers
                        mOpcode = "1100";
                        string temp = compiler.parsedAssemblyList[i].Remove(0, 4);
                        // grab blocks

                        string block1 = null;
                        string block2 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        block1 = functionLibrary.removeWhitespace(block1);
                        block2 = functionLibrary.removeWhitespace(block2);

                        if (!block1.Contains("$"))
                        {
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block1), true, 8);

                            // then set other block to mRead
                            if (block2.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else if (!block2.Contains("$"))
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(block2), true, 8);
                            // then set other block to mramr
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            mSelectBit2 = "1";
                            if (block1.Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 0), true, 5); }
                            else if (block1.Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block1, 1), true, 4) + "1"; mReadToggle = "0"; }

                            if (block2.Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 0), true, 5); }
                            else if (block2.Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(block2, 1), false, 4); mWriteToggle = "0"; }
                        }
                    }
                    #endregion

                    // ****WILL ALSO NEED TO ADD IN A=0 / B=0 TESTS!****
                    // ****CANNOT HAVE COMBINED TEST INSTRUCTIONS WITH RC5.0!
                    #region test instruction
                    if (compiler.parsedAssemblyList[i].Contains("test"))
                    {
                        // cannot combine test instructions with RC5.0!
                        mSelectBit1 = "1";
                        mOpcode = "1000";
                        // set condition to test
                        // grab input number - only from 0 - 3 - add 1 b/c 0-based index - add 8 to that b/c start at 8 on extension bit
                        if (compiler.parsedAssemblyList[i].Contains("%input%"))
                        { mExtension = functionLibrary.convertNumberToBinary(Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(compiler.parsedAssemblyList[i].IndexOf("%input%") + 8, 1)) + 9, true, 4); }
                        else if (compiler.parsedAssemblyList[i].Contains(">"))
                        { mExtension = "0110"; }
                        else if (compiler.parsedAssemblyList[i].Contains("<"))
                        { mExtension = "1100"; }
                        else if (compiler.parsedAssemblyList[i].Contains("=="))
                        { mExtension = "0010"; }
                        // special case - flip tjump and fjump values as this cond check does not exist in hardware (== does)
                        else if (compiler.parsedAssemblyList[i].Contains("!="))
                        {
                            mExtension = "0010";
                            // swap fjump and tjump
                            compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("tjump", "tjumpr");
                            compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("fjump", "tjump");
                            compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("tjumpr", "fjump");
                        }
                        else if (compiler.parsedAssemblyList[i].Contains(">="))
                        { mExtension = "1110"; }
                        else if (compiler.parsedAssemblyList[i].Contains("<="))
                        { mExtension = "0001"; }
                        // cout positive
                        else if (compiler.parsedAssemblyList[i].Contains("%so%"))
                        { mExtension = "1000"; }
                        // cout negative - swap tjump and fjump
                        else if (compiler.parsedAssemblyList[i].Contains("%su%"))
                        {
                            mExtension = "1000";
                            compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("tjump", "tjumpr");
                            compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("fjump", "tjump");
                            compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("tjumpr", "fjump");
                        }

                        // hardware conditions specific to RC5.0
                        else if (compiler.parsedAssemblyList[i].Contains("%data_received%")) { mExtension = "1011"; }
                        else if (compiler.parsedAssemblyList[i].Contains("%stack_overflow%")) { mExtension = "0111"; }
                        else if (compiler.parsedAssemblyList[i].Contains("%datastream_active%")) { mExtension = "1111"; }
                    }
                    #endregion

                    #region ramw (ONLY) component
                    if (compiler.parsedAssemblyList[i].Contains("ramw") && !compiler.parsedAssemblyList[i].Contains("ramr") && !compiler.parsedAssemblyList[i].Contains("cacher"))
                    {
                        // two cases - [ramw $adr$ ~temp~] and [ramw $adr$ (num)]
                        mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 5);
                        if (!compiler.parsedAssemblyList[i].Contains("~"))
                        { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(functionLibrary.removeWhitespace(compiler.parsedAssemblyList[i].Substring(compiler.parsedAssemblyList[i].LastIndexOf("$") + 1))), true, 8); }
                    }
                    #endregion
                    #region cachew (ONLY) component
                    if (compiler.parsedAssemblyList[i].Contains("cachew") && !compiler.parsedAssemblyList[i].Contains("cacher") && !compiler.parsedAssemblyList[i].Contains("ramr"))
                    {
                        // two cases - [cachew $adr$ ~temp~] and [cachew $adr$ (num)]
                        mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), false, 4);
                        if (!compiler.parsedAssemblyList[i].Contains("~"))
                        { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(functionLibrary.removeWhitespace(compiler.parsedAssemblyList[i].Substring(compiler.parsedAssemblyList[i].LastIndexOf("$") + 1))), true, 8); }
                        // cache specific flag
                        mWriteToggle = "0";
                        // cachew specific flag - need to enable writing output to cache
                        mToggleForward = "1"; mSelectBit5 = "1";
                    }
                    #endregion

                    // these three types of memory calls are all interconnected
                    #region ramr + ramw component (for copying global values)
                    if (compiler.parsedAssemblyList[i].Contains("ramr") && compiler.parsedAssemblyList[i].Contains("ramw")
                        && !compiler.parsedAssemblyList[i].Contains("cacher") && !compiler.parsedAssemblyList[i].Contains("cachew")) // not sure if these checks are necessary
                    {
                        mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("ramw") + 5), true, 5);
                        mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("ramr") + 5), true, 5);
                    }
                    #endregion
                    #region cacher + cachew component (for copying local values)
                    else if (compiler.parsedAssemblyList[i].Contains("cachew") && compiler.parsedAssemblyList[i].Contains("cacher")
                        && !compiler.parsedAssemblyList[i].Contains("ramr") && !compiler.parsedAssemblyList[i].Contains("ramw")) // not sure if these checks are necessary
                    {
                        mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("cachew") + 8), false, 4);
                        mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("cacher") + 8), true, 4) + "1";
                        mWriteToggle = "0"; mReadToggle = "0";
                        // cachew specific flag - need to enable writing output to cache
                        mToggleForward = "1"; mSelectBit5 = "1";
                    }
                    #endregion
                    #region ramr/cacher + ramw/cachew component (for copying values across local and global boundaries)
                    else if ((compiler.parsedAssemblyList[i].Contains("ramr") || compiler.parsedAssemblyList[i].Contains("cacher"))
                        && (compiler.parsedAssemblyList[i].Contains("ramw") || compiler.parsedAssemblyList[i].Contains("cachew")))
                    {
                        if (compiler.parsedAssemblyList[i].Contains("ramr"))
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("ramr") + 5), true, 5); }
                        else if (compiler.parsedAssemblyList[i].Contains("cacher"))
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("cacher") + 8), true, 4) + "1"; mReadToggle = "0"; }
                        if (compiler.parsedAssemblyList[i].Contains("ramw"))
                        { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("ramw") + 5), true, 5); }
                        else if (compiler.parsedAssemblyList[i].Contains("cachew"))
                        {
                            mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("cachew") + 8), false, 4); mWriteToggle = "0";
                            // cachew specific flag - need to enable writing output to cache
                            mToggleForward = "1"; mSelectBit5 = "1";
                        }
                    }
                    #endregion

                    #region fjump component
                    if (compiler.parsedAssemblyList[i].Contains("fjump"))
                    { mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5) - 1 /*don't know if it will work or not*/, true, 7); }
                    else if (mGFalse == "0000000" && generate)
                    { mGFalse = functionLibrary.convertNumberToBinary(fJumpDefault, true, 7); }
                    #endregion
                    #region tjump component
                    if (compiler.parsedAssemblyList[i].Contains("tjump"))
                    { mGTrue = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("tjump") + 5) - 1 /*don't know if it will work or not*/ , true, 7); }
                    #endregion

                    if (generate)
                    {
                        currentInstructionWord = mGFalse + " " + mGTrue + " " + mExtension + " " + mOpcode + " " + mSelectBit1 + mSelectBit2 + mSelectBit3 + mSelectBit4 + mSelectBit5 + " " + mToggleForward + " " + mRead+ " " + mReadToggle + " " + mWrite + " " + mWriteToggle + " " + mImmediate;
                        addToMachineCodeList(currentInstructionWord, i + 1);
                    }
                }
                // after machine code generation is complete, remove last line of machine code (as it's EOF and has all 0's written)
                machineCodeList.RemoveAt(machineCodeList.Count - 1);
                logging.logDebug("Machine code generation complete!", logging.loggingSenderID.machineCodeParser);
                if (Program.moreVerboseDebug) logging.logDebug("Dumping generated machine code ...", logging.loggingSenderID.machineCodeParser);
                // remove all whitespaces in machine code first!
                for (int i = 0; i < machineCodeList.Count; i++) machineCodeList[i] = functionLibrary.removeWhitespace(machineCodeList[i]);
                if (Program.moreVerboseDebug) for (int i = 0; i < machineCodeList.Count; i++) logging.logDebug("[" + (i + 1).ToString("D" + digits) + "]: " + machineCodeList[i] + " | len " + machineCodeList[i].Length.ToString(), logging.loggingSenderID.machineCodeParser);
            }
            #endregion
        }

        // related functions
        private static int acquireMemoryAddress(string codeToAcquireFrom, int lengthOfStringBefore)
        {
            int IDIndexCounter = 1;
            int memAddr = -1;
        TopOfFunction:
            try
            {
                Convert.ToInt16(codeToAcquireFrom.Substring(lengthOfStringBefore + 1, IDIndexCounter));
                IDIndexCounter++;
                goto TopOfFunction;
            }
            catch (FormatException) { memAddr = Convert.ToInt16(codeToAcquireFrom.Substring(lengthOfStringBefore + 1, IDIndexCounter - 1)); }
            catch (ArgumentOutOfRangeException) { memAddr = Convert.ToInt16(codeToAcquireFrom.Substring(lengthOfStringBefore + 1, IDIndexCounter - 1)); }
            return memAddr;
        }
        private static void addToMachineCodeList(string currentInstructionWord, int lineOfCodeAt)
        {
            if (Program.compileTarget != "3") logging.logDebug("[" + (machineCodeList.Count + 1).ToString("D" + digits) + "]: " + currentInstructionWord + ", len " + currentInstructionWord.Length.ToString(), logging.loggingSenderID.machineCodeParser);
            // b/c RC5.0's instructions are based on a 0-based index
            else logging.logDebug("[" + machineCodeList.Count.ToString("D" + digits) + "]: " + currentInstructionWord + ", len " + currentInstructionWord.Length.ToString(), logging.loggingSenderID.machineCodeParser);
            machineCodeList.Add(currentInstructionWord);
        }
    }
}
