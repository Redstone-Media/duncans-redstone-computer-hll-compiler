﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace DRCHLLC_Core_Compiler__ARCISS_v1._1_
{
    class functionLibrary
    {
        /*
         * This class contains all miscellaneous functions needed to perform
         * compilations and other required tasks.
         * 
         * Created 12/28/2016 by AGuyWhoIsBored
         * 
         */

        public static bool checkForZero(string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return true;
            else return false;
        }
        public static string calculateMD5Hash(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++) { sb.Append(hash[i].ToString("X2")); }
            return sb.ToString();
        }
        public static string replaceFirst(string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0) { return text; }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }
        public static string[] removeElementFromArray(string[] IndicesArray, int RemoveAt)
        {
            string[] newIndicesArray = new string[IndicesArray.Length - 1];
            int i = 0;
            int j = 0;
            while (i < IndicesArray.Length)
            {
                if (i != RemoveAt)
                { newIndicesArray[j] = IndicesArray[i]; j++; }
                i++;
            }
            return newIndicesArray;
        }
        public static string removeWhitespace(string input) { return new string(input.ToCharArray().Where(c => !char.IsWhiteSpace(c)).ToArray()); }
        public static void addToTokenArrays(string tokenedValue, string bufferString, string tokenLineOfCodeValue, int sender)
        { 
            // sender 0 - compiler master code base token array
            // sender 1 - linker TEMP token arrays (per extension)
            // sender 2 - linker secondary code base token arrays
            if (sender == 0)
            {
                if (tokenedValue != "0")
                {
                    Array.Resize(ref compiler.tokenTokensArray, compiler.tokenTokensArray.Length + 1);
                    Array.Resize(ref compiler.tokenValuesArray, compiler.tokenValuesArray.Length + 1);
                    Array.Resize(ref compiler.tokenLineOfCodeValueArray, compiler.tokenLineOfCodeValueArray.Length + 1);
                    compiler.tokenTokensArray[compiler.tokenValueCount] = tokenedValue;
                    compiler.tokenValuesArray[compiler.tokenValueCount] = bufferString;
                    compiler.tokenLineOfCodeValueArray[compiler.tokenValueCount] = tokenLineOfCodeValue;
                    compiler.tokenValueCount++;
                }
                else { logging.logDebug("Invalid token detected before array insert - omitting token ...", logging.loggingSenderID.tokenizer); }
            }
            else if (sender == 1)
            {
                Array.Resize(ref linker.tempTokenListTokens, linker.tempTokenListTokens.Length + 1);
                Array.Resize(ref linker.tempTokenListValues, linker.tempTokenListValues.Length + 1);
                Array.Resize(ref linker.tempTokenListLOCode, linker.tempTokenListLOCode.Length + 1);
                linker.tempTokenListTokens[linker.tokenListCounter] = tokenedValue;
                linker.tempTokenListValues[linker.tokenListCounter] = bufferString;
                linker.tempTokenListLOCode[linker.tokenListCounter] = tokenLineOfCodeValue;
                linker.tokenListCounter++;
            }
            else if (sender == 2)
            {
                Array.Resize(ref compiler.linkTokenTokensArray, compiler.linkTokenTokensArray.Length + 1);
                Array.Resize(ref compiler.linkTokenValuesArray, compiler.linkTokenValuesArray.Length + 1);
                Array.Resize(ref compiler.linkTokenLOCodeArray, compiler.linkTokenLOCodeArray.Length + 1);
                compiler.linkTokenTokensArray[compiler.linkTokenValueCount] = tokenedValue;
                compiler.linkTokenValuesArray[compiler.linkTokenValueCount] = bufferString;
                compiler.linkTokenLOCodeArray[compiler.linkTokenValueCount] = tokenLineOfCodeValue;
                compiler.linkTokenValueCount++;
            }
        }
        public static int acquireBlockID(string codeToParse, int lengthOfStringBefore)
        {
            // this function needs to exist b/c of possibilities that block ID's can go higher than single digit in complex programs
            int IDIndexCounter = 1;
            int blockID = -1;
            TopOfFunction:
            try
            {
                Convert.ToInt16(codeToParse.Substring(lengthOfStringBefore, IDIndexCounter));
                IDIndexCounter++;
                goto TopOfFunction;
            }
            catch (FormatException)
            { blockID = Convert.ToInt16(codeToParse.Substring(lengthOfStringBefore, IDIndexCounter - 1)); }
            return blockID;
        }
        public static string convertNumberToBinary(int pieceToConvert, bool flipBits, int padLength)
        {
            // this function will also pad our binary numbers with 0's to make it easier to just plug in
            string output = null;
            output = Convert.ToString(pieceToConvert, 2);
            if (flipBits == true) { output = new string(output.ToCharArray().Reverse().ToArray()); }
            if (output.Length != padLength)
            {
                int increaseLength = padLength - (padLength - output.Length); // very important!
                if (flipBits == false)
                { output = output.PadLeft(padLength - output.Length + increaseLength, '0'); }
                else
                { output = output.PadRight(padLength - output.Length + increaseLength, '0'); }
            }
            return output;
        }
        public static void writeDebugLogOut()
        {
            if (Program.debug)
            {
                logging.logDebug("Writing debug log to file!", logging.loggingSenderID.compiler);
                logging.logDebug("Debug log located at '" + Directory.GetCurrentDirectory() + "\\debuglog.txt" + "'", logging.loggingSenderID.compiler);
                File.WriteAllLines(Directory.GetCurrentDirectory() + "\\debuglog.txt", new[] { logging.debugText });

            }
        }
        public static string generateMemoryCallFromVariable(string variable, bool write)
        {
            string output = null;
            string memoryalloctype = compiler.MemoryAllocationType[compiler.MATAddressPointers.ToList().IndexOf(variable)].ToString();
            // write 0 = read
            // write 1 = write
            // loop through MAT array and determine whether to issue RAM memory call or CACHE memory call
            if (memoryalloctype == "1") output += "ram";
            else if (memoryalloctype == "3") output += "cache";
            if (write) output += "w"; else output += "r";
            return output;
        }
    }
}
