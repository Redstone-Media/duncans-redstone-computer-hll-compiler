﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DRCHLLC_Core_Compiler__ARCISS_v1._1_
{
    class linker
    {
        /*
         * This class will contain all functions and information needed for linking
         * internal and external function calls (functions and external libraries) to 
         * the master program assembly to enable function call support.
         * 
         * Created 10/30/2018
         * 
         */

        // this class will include:
        // - symbol generation [deprecated]
        // - function call merging
        // - resolving all dependencies in all libraries
        // - resolve all target requests in libraries
        // - resolve all requires requests in libraries

        // ONLY local and temporary for each extension
        // master secondary tokenlist will be in main compiler class
        public static string[] tempTokenListTokens = new string[0];
        public static string[] tempTokenListValues = new string[0];
        public static string[] tempTokenListLOCode = new string[0];
        public static int tokenListCounter = 0;
        
        public static void linkExtension(List<string> originCodeBase, string extensionName)
        {
            logging.logDebug("Initializing link with extension [" + extensionName + "]", logging.loggingSenderID.linker);
            originCodeBase = makeUniqueCalls(originCodeBase, extensionName);
            // reset temporary arrays
            tempTokenListTokens = new string[0]; tempTokenListValues = new string[0]; tempTokenListLOCode = new string[0]; tokenListCounter = 0;
            string tempFunctionName = "";

            // lex and tokenize so that we can detect special keywords and values
            // will merge full linked token database with other master program token database
            logging.logDebug("Lexing data in code base buffer ...", logging.loggingSenderID.linker);
            for (int i = 0; i < originCodeBase.Count; i++) { originCodeBase[i] = originCodeBase[i].Trim(); originCodeBase[i] += " "; }
            for (int i = 0; i < originCodeBase.Count; i++)
            {
                logging.logDebug("Lexing line " + (i + 1).ToString() + " of code from code base buffer", logging.loggingSenderID.linker);
                compiler.LexData(originCodeBase[i], (i + 1).ToString(), 1);
            }

            // resolve all target keywords
            // if required target for function execution does not match compile target, remove entire function block from originCodeBase
            logging.logDebug("Resolving all function targets ...", logging.loggingSenderID.linker);
            int tokenTarget1 = 0; // token location of the 'func' definition of the function
            int tokenTarget2 = 0; // token location of the first '{' of the function definition
            int tokenTargetLoc = 0; // token location of 'target' token
            for (int i = 0; i < tempTokenListTokens.Length; i++)
            {
                if (tempTokenListTokens[i] == tokens.convertInt("identifier_func").ToString()) { tokenTarget1 = i; tempFunctionName = tempTokenListValues[i += 1];  }
                if (tempTokenListTokens[i] == tokens.convertInt("ext_target").ToString())
                {
                    tokenTargetLoc = i;
                    logging.logDebug("Target keyword hit at token " + tokenTargetLoc + ", resolving ...", logging.loggingSenderID.linker);
                    // grab all target IDs and put them in a list
                    List<string> targets = new List<string>(); i++;
                    while (tempTokenListTokens[i] == tokens.convertInt("data_int").ToString())
                    { targets.Add(tempTokenListValues[i]); i++; }
                    if (Program.moreVerboseDebug) logging.logDebug("Dumping target ID list", logging.loggingSenderID.linker);
                    if (Program.moreVerboseDebug) foreach (string tID in targets)
                    { logging.logDebug("[" + tID + "]", logging.loggingSenderID.linker); }
                    // now check if any values in list match compile target
                    bool matchTarget = false;
                    tokenTarget2 = i;
                    for (int j = 0; j < targets.Count; j++)
                    {
                        if (targets[j] == Program.compileTarget)
                        {
                            matchTarget = true; logging.logDebug("Target matched for function [" + tempFunctionName + "]", logging.loggingSenderID.linker);
                            // remove target keyword and targets
                            for (int k = tokenTargetLoc; k < tempTokenListTokens.Length; k++)
                            {
                                if (tempTokenListTokens[k] != tokens.convertInt("data_int").ToString() && k != tokenTargetLoc) break;
                                tempTokenListTokens[k] = "0";
                            }
                        }
                    }
                    if (!matchTarget)
                    {
                        // remove function from token list and from secondary codebase
                        logging.logDebug("Target not matched for function [" + tempFunctionName + "], removing function from secondary code base ...", logging.loggingSenderID.linker);
                        if (Program.moreVerboseDebug) logging.logDebug("targettokenremove is [" + tokenTarget2 + "] " + tokenTarget1 + " " + tempTokenListTokens[tokenTarget1] + " " + tempTokenListValues[tokenTarget1], logging.loggingSenderID.linker);
                        // remove 'func' definition part of function definition
                        // for "remove", mark array index with tokenID 0, and then cleanup all invalid tokens at the end of target resolve
                        for (int k = tokenTarget1; k < tokenTarget2 + 1; k++)
                        { tempTokenListTokens[k] = "0"; }
                        // remove codebase for function definition
                        int blockID = 1;
                        for (int k = tokenTarget1; k < tempTokenListTokens.Length; k++)
                        {
                            if (tempTokenListTokens[k] == tokens.convertInt("grouper_openbracket").ToString()) { blockID++; } if (tempTokenListTokens[k] == tokens.convertInt("grouper_closebracket").ToString()) { blockID--; }
                            tempTokenListTokens[k] = "0"; if (blockID == 0) break;
                        }
                        // add token to deletedToken list
                        tokens.removeImportedToken(tokens.convertInt(tempFunctionName).ToString());
                    }
                }
            }

            // cleanup invalid tokens
            #region copied from compiler class - cleaning up and finalizing token arrays
            // then output to debugger token array values before cleanup
            if (Program.moreVerboseDebug)
            {
                logging.logDebug("Dumping token arrays ...", logging.loggingSenderID.compiler);
                for (int i = 0; i < tokenListCounter; i++)
                { logging.logDebug("Location " + i.ToString() + " in token array is [" + tempTokenListLOCode[i] + "," + tempTokenListTokens[i] + "," + tempTokenListValues[i] + "]", logging.loggingSenderID.compiler); }
            }
            // clean up token arrays
            logging.logDebug("Detecting invalid tokens ... ", logging.loggingSenderID.compiler);
            int invalidTokenCount = 0;
            for (int i = 0; i < tokenListCounter; i++)
            { if (tempTokenListTokens[i] == "0") { invalidTokenCount++; if (Program.moreVerboseDebug) { logging.logDebug("Array location " + i.ToString() + " detected with invalid token", logging.loggingSenderID.compiler); } } }
            logging.logDebug(invalidTokenCount.ToString() + " invalid tokens detected", logging.loggingSenderID.compiler);
            logging.logDebug("Removing invalid tokens ...", logging.loggingSenderID.compiler);
            int removeOffset = 0; int newTokenValueCount = tokenListCounter - invalidTokenCount;
            while (invalidTokenCount != 0)
            {
                bool enableRemove = true;
                for (int i = 0; i < tokenListCounter - removeOffset; i++)
                {
                    if (tempTokenListTokens[i] == "0" && enableRemove == true)
                    {
                        // this enableRemove variable exists to be able to remove one at a time
                        enableRemove = false; removeOffset++; invalidTokenCount--;
                        tempTokenListTokens = functionLibrary.removeElementFromArray(tempTokenListTokens, i);
                        tempTokenListValues = functionLibrary.removeElementFromArray(tempTokenListValues, i);
                        tempTokenListLOCode = functionLibrary.removeElementFromArray(tempTokenListLOCode, i);
                    }
                }
            }
            logging.logDebug("Removed all invalid tokens from token arrays", logging.loggingSenderID.compiler);
            logging.logDebug(newTokenValueCount.ToString() + " tokens in token arrays after removal of invalid tokens", logging.loggingSenderID.compiler);
            logging.logDebug("Dumping updated token arrays after target resolution ...", logging.loggingSenderID.compiler);
            for (int i = 0; i < newTokenValueCount; i++)
            { logging.logDebug("Location " + i.ToString() + " in token array is [" + tempTokenListLOCode[i] + "," + tempTokenListTokens[i] + "," + tempTokenListValues[i] + "]", logging.loggingSenderID.compiler); }
            #endregion

            // resolve all requires keywords - this is where the linker may need to become recursive
            logging.logDebug("Resolving all function imports and requirements ...", logging.loggingSenderID.linker);
            for (int i = 0; i < tempTokenListTokens.Length; i++)
            {
                string resolveExtensionName = "";
                if (tempTokenListTokens[i] == tokens.convertInt("ext_requires").ToString())
                {
                    logging.logDebug("Hit function import requirement at token " + i + ", resolving ...", logging.loggingSenderID.linker);
                    resolveExtensionName = tempTokenListValues[i += 1];
                    logging.logDebug("Checking status of extension [" + resolveExtensionName + "]", logging.loggingSenderID.linker);
                    importer.checkExtensionStatus(resolveExtensionName);
                }
            }

            logging.logDebug("Merging imported token arrays into secondary code base ...", logging.loggingSenderID.linker);
            for (int i = 0; i < tempTokenListTokens.Length; i++)
            { functionLibrary.addToTokenArrays(tempTokenListTokens[i], tempTokenListValues[i], tempTokenListLOCode[i], 2); }
        }

        // this name is a bit deceiving
        public static List<string> makeUniqueCalls(List<string> codeBase, string extensionName)
        {
            logging.logDebug("Generating unique function calls for extension [" + extensionName + "]", logging.loggingSenderID.linker);
            // will comb through entire provided codebase and add defined functions to symbol table
            // will also generate unique identifiers for functions for later
            List<string> newCodeBase = codeBase;
            if (extensionName != "") extensionName += ".";
            for (int i = 0; i < newCodeBase.Count; i++)
            {
                if (newCodeBase[i].Contains("func "))
                {
                    // grab name of function through block
                    string temp = newCodeBase[i].Substring(newCodeBase[i].IndexOf("func") + 5);
                    StringReader sr = new StringReader(temp);
                    string block1 = null;
                    int readLoc = 0;
                    char currentChar;

                    currentChar = (char)sr.Read();
                    block1 += currentChar;
                    readLoc++;
                    while (currentChar != '(')
                    { readLoc++; currentChar = (char)sr.Read(); block1 += currentChar; }
                    block1 = block1.Remove(block1.Length - 1); block1 = functionLibrary.removeWhitespace(block1); // to get rid of (

                    // replace all instances of function block with extensionName.block1
                    for (int j = 0; j < newCodeBase.Count; j++)
                    { if (newCodeBase[j].Contains(block1) && !newCodeBase[j].Contains(extensionName + block1) && !newCodeBase[j].Contains("." + block1)) { newCodeBase[j] = newCodeBase[j].Replace(block1, extensionName + block1); } }
                    // update function name to reflect extensionName so that it will be tokenized correctly
                    // update tokenizer so that variables declared in function scopes are tokenized correctly
                    // update terminology for 'symbol table'
                }
            }
            return newCodeBase;
        }

    }
}
