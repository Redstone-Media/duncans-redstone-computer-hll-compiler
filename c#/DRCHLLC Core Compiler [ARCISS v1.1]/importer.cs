﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DRCHLLC_Core_Compiler__ARCISS_v1._1_
{
    class importer
    {
        /*
         * This class will contain all functions and information needed importing additional
         * information, resources, and extensions into the compiler and into the environment
         * 
         * Created 9/14/2016 by AGuyWhoIsBored
         * 
         */

        // resources will be loaded on program launch
        // resources (for now) will be located at program directory + /resources
        // in the ~\resources folder, there will be a ref.drchllc file that will contain the references 
        //      to loading all of the extensions
        // for the reference file, you only need the name of the file. Resources MUST be in the same folder as the
        // reference file!

        // resources will be loaded one file at a time
        // resource loading and order / queue management will be handled by other methods
        // how resources will be added and merged into compiler environment (step by step)
        // copy file to internal buffers > assign global runtime resource ID & ext. ID
        // > syntax check all function data / code > (if successful) > load in and merge function(s) syntax tables
        // > load in and merge functon token(s) >load in and merge function code > complete

        // INTERNAL: REDO THIS CLASS ALSO! 

        // FOR ARCISS v1.1 (and compiler v1.1), just merge all imported code into main codebase (make sure to check for no conflicts and remove target and requires keywords after proper handling)
        // we will then use built-in function support to handle the imported code!

        public static bool ext_RC40E = false;
        public static bool ext_RC50E = false;

        // resources to import and link that are available
        public static List<string> loadResourceList = new List<string>();
        
        // loaded resources
        public static Dictionary<int, string> loadedResourcesList = new Dictionary<int, string>(); // <globalResourceID, resourceName>
        public static int resourceIDCounter = 0; // 0-based index

        public static string[] resourceTTokens = new string[0];
        public static string[] resourceTValues = new string[0];
        public static string[] resourceTLineOfCodeValue = new string[0];
        public static int resourceTCount = 0;

        static void importExtension(string extLoad)
        {
            logging.logDebug("Loading extension [" + extLoad + "] ...", logging.loggingSenderID.importer, true);

            // actually begin importing process here
            // move all contents of file to buffer
            #region moving contents of file to buffer
            List<string> resourceFileList = new List<string>();
            StreamReader sr1 = new StreamReader(Program.resourceFolderLocation + "\\" + extLoad + ".drchllc");
            for (int b = 0; b < File.ReadAllLines(Program.resourceFolderLocation + "\\" + extLoad + ".drchllc").Length; b++)
            { resourceFileList.Add(sr1.ReadLine()); }
            sr1.Close();
            logging.logDebug("Moved resource file to buffer", logging.loggingSenderID.importer);
            logging.logDebug("Removing non-code objects from buffer ...", logging.loggingSenderID.importer);
            for (int b = 0; b < resourceFileList.Count; b++)
            { if (resourceFileList[b].Contains("//")) { resourceFileList[b] = resourceFileList[b].Remove(resourceFileList[b].IndexOf("//")); } }
            for (int b = 0; b < resourceFileList.Count; b++)
            {
                Predicate<string> p = new Predicate<string>(functionLibrary.checkForZero);
                if (resourceFileList[b] == string.Empty) { resourceFileList.RemoveAll(p); }
            }

            if (Program.moreVerboseDebug)
            {
                logging.logDebug("Dumping buffer ...", logging.loggingSenderID.importer);
                for (int b = 0; b < resourceFileList.Count; b++)
                { logging.logDebug(b.ToString() + ": [" + resourceFileList[b] + "]", logging.loggingSenderID.importer); }
            }
            #endregion

            // check for extensions on extension
            logging.logDebug("Checking for requested extension imports in extension [" + extLoad + "] ...", logging.loggingSenderID.importer);
            while (resourceFileList[1].Contains("import"))
            {
                string extRequest = resourceFileList[1].Remove(resourceFileList[1].IndexOf("import"), 7).Trim();
                checkExtensionStatus(extRequest); resourceFileList.RemoveAt(1);
            }

            // assigning global runtime resource ID and extension ID to resource
            #region assigning ID's
            loadedResourcesList.Add(resourceIDCounter, resourceFileList[0]);
            resourceIDCounter++;
            logging.logDebug("Resource named '" + resourceFileList[0] + "' assigned global resource identifier " + (resourceIDCounter - 1), logging.loggingSenderID.importer);
            resourceFileList.RemoveAt(0);
            #endregion

            // initialize link
            linker.linkExtension(resourceFileList, loadedResourcesList[resourceIDCounter - 1]);
        }

        public static void checkExtensionStatus(string extRequest)
        {
            logging.logDebug("Import of extension [" + extRequest + "] requested", logging.loggingSenderID.importer);
            if (loadedResourcesList.ContainsValue(extRequest)) logging.logDebug("Extension [" + extRequest + "] already imported! Ignoring ...", logging.loggingSenderID.importer);
            if (File.Exists(Program.resourceFolderLocation + "\\" + extRequest + ".drchllc"))
            {
                logging.logDebug("Extension exists in resource folder, importing ...", logging.loggingSenderID.importer);
                if (extRequest == "RC40E")
                {
                    if (ext_RC40E) { logging.logDebug("Internal Redstone Computer v4.0 Extension already loaded!", logging.loggingSenderID.importer); }
                    else
                    {
                        logging.logDebug("Enabling ARCISS Internal Redstone Computer v4.0 Extension", logging.loggingSenderID.importer); rc40e.enableExtension();
                        // check to make sure that the 'RCv4.0' compile target is selected; if not, throw a warning that unknown code may be executed when compiled
                        if (Program.compileTarget != "2") { logging.logWarning("Importing code library for the Redstone Computer v4.0 and the selected compile target is not the Redstone Computer v4.0! Unintended or unknown code may be executed on said compile target, and unknown errors may occur!", logging.loggingSenderID.compiler); }
                    }
                }
                else if (extRequest == "RC50E")
                {
                    if (ext_RC50E) { logging.logDebug("Internal Redstone Computer v5.0 Extension already loaded!", logging.loggingSenderID.importer); }
                    else
                    {
                        logging.logDebug("Enabling ARCISS Internal Redstone Computer v5.0 Extension", logging.loggingSenderID.importer); rc50e.enableExtension();
                        // check to make sure that the 'RCv5.0' compile target is selected; if not, throw a warning that unknown code may be executed when compiled
                        if (Program.compileTarget != "3") { logging.logWarning("Importing code library for the Redstone Computer v5.0 and the selected compile target is not the Redstone Computer v4.0! Unintended or unknown code may be executed on said compile target, and unknown errors may occur!", logging.loggingSenderID.compiler); }
                    }
                }
                else if (new FileInfo(Program.resourceFolderLocation + "\\" + extRequest + ".drchllc").Length == 0) { logging.logDebug("Resource [" + extRequest + ".drchllc] is empty! Skipping import ...", logging.loggingSenderID.importer); }
                else importExtension(extRequest);
            }
            else logging.logDebug("Extension [" + extRequest + "] does not exist in resource folder, ignoring ...", logging.loggingSenderID.importer);
        }
    }
}
