﻿using System;
using System.IO;
using System.Linq;
using System.Threading;

namespace DRCHLLC_Uploader
{
    class Program
    {
        /*Duncan's Redstone Computer HLL Compiler [DRCHLL-C] Uploader
         * -------------------------

          Created 6/21/2015.
          Copyright © AGuyWhoIsBored. All rights reserved.

          DRCHLLC Uploader
          Project started on 6/21/15
          Build v1.0a pushed 7/22/16
          Build v1.1a pushed 11/10/18

        * --------------------------

        // this program will contain the code to upload to certain targets!

        // this program will include things such as:
        // - parsing flags
        // - setting variables
        // - etc...
        // - uploading
        // args :
        // [program] -f "file" -t [targetID] -c [coreID] [-d (toggle debug)] -os [overrideStartCoordinates "x,y,z,yaw,pitch"] -o [optionCollectiveID] -cd [countdownInt] -cwi [commandWaitInterval] -csi [commandSendInterval]

        /* DRCHLLC Uploader changelog / to-do list

        v1.0a: initial release

        v1.1a
        - added arguments for uploader settings to attempt to mitigate some users having issues with the uploader program
        - added datapack uploading method - this is now the recommended method to upload programs

        */

        public static string uploaderVersion = "1.1a";             // version of uploader program
        public static string compilerLanguageVersion = "1.0, 1.1"; // version of ARCISS language compiler supports

        // arguments and settings
        public static string compileTarget = "-1";
        public static string overrideStartCoordinates = "";
        public static string compileCoreTarget = "-1";
        public static string sourceFileLocation = "";
        public static string optionCollectiveID = "-1";
        public static string countdownInt = "-1";
        public static bool generateDatapack = false;
        // need to find balance where it'll work on most computers (assuming slower computer -> higher intervals due to latency)
        public static int commandWaitInterval = 75; // interval inbetween sending commands
        public static int commandSendInterval = 110; // interval for sending components of commands to minecraft

        public static bool debug = false;
        private static string userCommand = null;

        static void Main(string[] args)
        {
            Console.Title = "DRCHLLC Uploader";
            Console.WriteLine("--------");
            Console.WriteLine("DRCHLL-C Machine Code Uploader v" + uploaderVersion);
            Console.WriteLine("Supports ARCISS spec v" + compilerLanguageVersion);
            Console.WriteLine("Written and developed by AGuyWhoIsBored");
            Console.WriteLine("--------");
            Console.WriteLine("");

            TopOfArgumentAsk:
            try
            {
                // load arguments
                if (args.Length == 0) { Console.WriteLine("Waiting for user input ..."); userCommand = Console.ReadLine(); }
                else
                {
                    Console.WriteLine("Prepassed arguments found!");
                    for (int i = 0; i < args.Length; i++)
                    {
                        // for formatting
                        if (i == 0) { userCommand += args[i]; }
                        else { userCommand += " " + args[i]; }
                    }
                }
                Console.WriteLine("Beginning argument parsing ...");
                #region argument / flag parsing
                int index = 0;
                int flagCollectID = 0;
                int quoteCount = 0;
                char currentChar;
                string bufferString = null;
                userCommand = userCommand + " ";
                StringReader sr = new StringReader(userCommand);
                while (index != userCommand.Length)
                {
                    currentChar = (char)sr.Read();
                    index++;
                    bufferString += currentChar;
                    switch (currentChar)
                    {
                        case ' ':
                            if (bufferString == "-f ") { flagCollectID = 1; }
                            else if (bufferString == "-t ") { flagCollectID = 2; }
                            else if (bufferString == "-o ") { flagCollectID = 3; }
                            else if (bufferString == "-c ") { flagCollectID = 4; }
                            else if (bufferString == "-d ") { debug = true; Console.WriteLine("debug mode enabled!"); }
                            else if(bufferString == "-os ") { flagCollectID = 5; }
                            else if(bufferString == "-cd ") { flagCollectID = 6; }
                            else if(bufferString == "-cwi ") { flagCollectID = 7; }
                            else if(bufferString == "-csi ") { flagCollectID = 8; }
                            else
                            {
                                if (flagCollectID == 2) { compileTarget = bufferString; compileTarget = functionLibrary.RemoveWhitespace(compileTarget); flagCollectID = 0; }
                                else if (flagCollectID == 3) { optionCollectiveID = bufferString; optionCollectiveID = functionLibrary.RemoveWhitespace(optionCollectiveID); flagCollectID = 0; }
                                else if (flagCollectID == 4) { compileCoreTarget = bufferString; compileCoreTarget = functionLibrary.RemoveWhitespace(compileCoreTarget); flagCollectID = 0; }
                                else if (flagCollectID == 6) { countdownInt = bufferString; countdownInt = functionLibrary.RemoveWhitespace(countdownInt); flagCollectID = 0; }
                                else if (flagCollectID == 7) { commandWaitInterval = Convert.ToInt16(bufferString); flagCollectID = 0; }
                                else if (flagCollectID == 8) { commandSendInterval = Convert.ToInt16(bufferString); flagCollectID = 0; }
                            }
                            if (quoteCount == 0) { bufferString = null; }
                            break;
                        case '"':
                            quoteCount++;
                            if (flagCollectID == 1 && quoteCount == 2) { sourceFileLocation = bufferString; sourceFileLocation = sourceFileLocation.Remove(sourceFileLocation.Length - 1); quoteCount = 0; flagCollectID = 0; }
                            if (flagCollectID == 5 && quoteCount == 2) { overrideStartCoordinates = bufferString; overrideStartCoordinates = overrideStartCoordinates.Remove(overrideStartCoordinates.Length - 1); quoteCount = 0; flagCollectID = 0; }
                            bufferString = null;
                            break;
                    }
                }
                #endregion

                // dumping parsed flag variables
                if (debug)
                {
                    Console.WriteLine("compileTarget: " + compileTarget.ToString());
                    Console.WriteLine("overrideStartCoordinates: " + overrideStartCoordinates);
                    Console.WriteLine("compileCoreTarget: " + compileCoreTarget.ToString());
                    Console.WriteLine("sourceFileLocation: " + sourceFileLocation);
                    Console.WriteLine("optionCollectiveID: " + optionCollectiveID);
                    Console.WriteLine("countdownInt: " + countdownInt);
                    Console.WriteLine("commandWaitInterval: " + commandWaitInterval.ToString());
                    Console.WriteLine("commandSendInterval: " + commandSendInterval.ToString());
                }

                // not going to check advanced argument syntax because we're going to assume that the user knows that they're doing
                // especially if they have a program like this
                if (compileTarget == "-1"
                    || compileCoreTarget == "-1"
                    || sourceFileLocation == "")
                { Console.WriteLine("Core arguments are incomplete! Please check your arguments and try again!"); goto TopOfArgumentAsk; }

                if (Path.GetExtension(sourceFileLocation).ToLower() != ".pb") { Console.WriteLine("This file isn't a valid program binary file to upload!"); Console.ReadLine(); Environment.Exit(0); }

                #region setting uploader override start coordinates if necessary
                // syntax: -os "x,y,z, yaw, pitch"
                if(overrideStartCoordinates != "")
                {
                    Console.WriteLine("Updating start coordinates to user-defined values!");

                    // break down values
                    string os1 = "", os2 = "", os3 = "", os4 = "", os5 = "";
                    int index1 = 0;
                    int count = 0;
                    char c;
                    string bs2 = null;
                    overrideStartCoordinates += ",";
                    StringReader sr1 = new StringReader(overrideStartCoordinates);
                    while (index1 != overrideStartCoordinates.Length)
                    {
                        c = (char)sr1.Read();
                        index1++;
                        bs2 += c;
                        switch (c)
                        {
                            case ',':
                                count++;
                                switch(count)
                                {
                                    case 1: os1 = bs2; os1 = os1.Remove(os1.Length - 1); break;
                                    case 2: os2 = bs2; os2 = os2.Remove(os2.Length - 1); break;
                                    case 3: os3 = bs2; os3 = os3.Remove(os3.Length - 1); break;
                                    case 4: os4 = bs2; os4 = os4.Remove(os4.Length - 1); break;
                                    case 5: os5 = bs2; os5 = os5.Remove(os5.Length - 1); break;
                                }
                                bs2 = null;
                                break;
                        }
                    }
                    try
                    {
                        if (compileTarget == "1")
                        {
                            switch (compileCoreTarget)
                            {
                                case "1": uploader.RC30C1 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "2": uploader.RC30C2 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "3": uploader.RC30C3 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "4": uploader.RC30C4 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                            }
                        }
                        else if (compileTarget == "2")
                        {
                            switch (compileCoreTarget)
                            {
                                case "1": uploader.RC40C1 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "2": uploader.RC40C2 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "3": uploader.RC40C3 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "4": uploader.RC40C4 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("An error occured: " + e.Message);
                        Console.ReadLine(); Environment.Exit(0);
                    }
                }
                #endregion

                #region updating machine code array 
                uploader.machineCodeList = File.ReadAllLines(sourceFileLocation).ToList();
                #endregion

                #region applying options 
                // [1234] < bits
                // 1: cleanFirstEnabled
                // 2: onlyClean
                // 3: fastUpload
                // 4: toggle datapack upload method
                if (optionCollectiveID != "-1")
                {
                    Console.WriteLine("Updating uploader settings ...");
                    try
                    {
                        optionCollectiveID = functionLibrary.convertNumberToBinary(Convert.ToInt16(optionCollectiveID), false, 4);
                        StringReader sr1 = new StringReader(optionCollectiveID);
                        char currentChar1;
                        int index1 = 0;
                        while (index1 != optionCollectiveID.Length)
                        {
                            currentChar1 = (char)sr1.Read();
                            index1++;
                            if (currentChar1 == '1' && index1 == 1) { uploader.cleanFirstEnabled = true; if (debug) Console.WriteLine("Clean first is enabled!"); }
                            else if (currentChar1 == '1' && index1 == 2) { uploader.onlyClean = true; if (debug) Console.WriteLine("Only clean is set!"); }
                            else if (currentChar1 == '1' && index1 == 3) { uploader.fastUpload = true; if (debug) Console.WriteLine("Fast upload is enabled!"); }
                            else if (currentChar1 == '1' && index1 == 4) { generateDatapack = true; if (debug) Console.WriteLine("Datapack uploading toggled!"); }
                        }
                    }
                    catch (Exception e)
                    { Console.WriteLine("An error occured: " + e.Message); }
                }

                #endregion

                // let users know about potential bug and how to fix it until bug is fixed (issue #2 in repo)
                if (!generateDatapack) Console.WriteLine("Due to a bug in the current state of the uploder program, unexpected things may occur. To avoid this, we recommend changing your inventory key to something abstract, such as a symbol, to ensure that this bug will not corrupt program uploading!");

                Console.WriteLine("Uploader ready to start!");
                if (debug) { Console.WriteLine("Press [enter] to begin upload!"); Console.ReadLine(); logging.startStopwatch(); }

                // check integrity of code
                #region validating integrity of code
                logging.logDebug("Validating integrity and compatibility of machine code on selected target ...");
                // validate integrity of machine code
                for (int i = 0; i < uploader.machineCodeList.Count; i++)
                {
                    if (uploader.machineCodeList[i].Length > 41 && compileTarget == "1")
                    {
                        logging.logDebug("***FATAL ERROR*** Program instruction length(s) exceed capacity of compile target!", true);
                        logging.logDebug("***FATAL ERROR*** This program WILL NOT be able to be uploaded to the compile target!", true);
                        logging.logDebug("***FATAL ERROR*** This was most likely caused by the program being too big!", true);
                        if (debug) Console.ReadLine();
                        Environment.Exit(0);
                    }
                    if (uploader.machineCodeList[i].Length > 84 && compileTarget == "2")
                    {
                        logging.logDebug("// ***FATAL ERROR*** Program instruction length(s) exceed capacity of compile target!", true);
                        logging.logDebug("// ***FATAL ERROR*** This program WILL NOT be able to be uploaded to the compile target!", true);
                        logging.logDebug("// ***FATAL ERROR*** This was most likely caused by the program being too big!", true);
                        if (debug) Console.ReadLine();
                        Environment.Exit(0);
                    }
                    if (uploader.machineCodeList[i].Length > 48 && compileTarget == "3")
                    {
                        logging.logDebug("// ***FATAL ERROR*** Program instruction length(s) exceed capacity of compile target!", true);
                        logging.logDebug("// ***FATAL ERROR*** This program WILL NOT be able to be uploaded to the compile target!", true);
                        logging.logDebug("// ***FATAL ERROR*** This was most likely caused by the program being too big!", true);
                        if (debug) Console.ReadLine();
                        Environment.Exit(0);
                    }
                }
                if (uploader.machineCodeList.Count > 20 && uploader.machineCodeList.Count <= 31 && compileTarget == "1")
                {
                    logging.logDebug("// ***ERROR*** Program length is higher than default capacity of compile target!", true);
                    logging.logDebug("// ***ERROR*** Unless you have modified the hardware to allow high capacity, this program WILL NOT be able to be uploaded to the compile target!", true);
                    if (debug) Console.ReadLine();
                    Environment.Exit(0);
                }
                if (uploader.machineCodeList.Count > 63 && compileTarget == "2")
                {
                    logging.logDebug("// ***FATAL ERROR*** Program instruction length(s) exceed maximum capacity of compile target!", true);
                    logging.logDebug("// ***FATAL ERROR*** This program WILL NOT be able to be uploaded to the compile target!", true);
                    logging.logDebug("// ***FATAL ERROR*** This was most likely caused by the program being too big!", true);
                    if (debug) Console.ReadLine();
                    Environment.Exit(0);
                }
                if (uploader.machineCodeList.Count > 128 && compileTarget == "3")
                {
                    logging.logDebug("// ***FATAL ERROR*** Program instruction length(s) exceed maximum capacity of compile target!", true);
                    logging.logDebug("// ***FATAL ERROR*** This program WILL NOT be able to be uploaded to the compile target!", true);
                    logging.logDebug("// ***FATAL ERROR*** This was most likely caused by the program being too big!", true);
                    if (debug) Console.ReadLine();
                    Environment.Exit(0);
                }
                #endregion

                // begin main upload thread
                if (generateDatapack) { datapackuploader.generateDatapack();  }
                else
                {
                    //countdownint
                    try
                    {
                        if (countdownInt == "-1") countdownInt = "10";
                        Console.WriteLine("Starting upload in " + countdownInt + " seconds!");
                        Thread.Sleep(Convert.ToInt16(countdownInt) * 1000);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("An error occured: " + e.Message);
                        if (countdownInt != "-1") { Console.WriteLine("Are you sure you the countdown number is valid?"); }
                        Console.ReadLine(); Environment.Exit(0);
                    }
                    uploader.uploadMachineCodeToCompileTarget();
                }
                if (debug) { Console.ReadLine(); }
            }
            catch (Exception e)
            { Console.WriteLine("An error occured: " + e.Message); Console.ReadLine(); }
        }
    }
}
