﻿using System;
using System.Linq;
namespace DRCHLLC_Uploader
{
    class functionLibrary
    {
        /*
         * This class contains all miscellaneous functions needed to perform
         * uploading and other required tasks.
         * 
         * Created 12/28/2016 by AGuyWhoIsBored
         * 
         */

        public static string RemoveWhitespace(string input)
        { return new string(input.ToCharArray().Where(c => !char.IsWhiteSpace(c)).ToArray()); }
        public static string convertNumberToBinary(int pieceToConvert, bool flipBits, int padLength)
        {
            // this function will also pad our binary numbers with 0's to make it easier to just plug in
            string output = null;
            output = Convert.ToString(pieceToConvert, 2);
            if (flipBits == true) { output = new string(output.ToCharArray().Reverse().ToArray()); }
            if (output.Length != padLength)
            {
                int increaseLength = padLength - (padLength - output.Length); // very important!
                if (flipBits == false) { output = output.PadLeft(padLength - output.Length + increaseLength, '0'); }
                else { output = output.PadRight(padLength - output.Length + increaseLength, '0'); }
            }
            return output;
        }
    }
}
