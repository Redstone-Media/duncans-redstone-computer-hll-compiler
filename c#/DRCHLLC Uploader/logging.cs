﻿using System;
using System.Diagnostics;

namespace DRCHLLC_Uploader
{
    class logging
    {
        /*
         * This class will contain all functions and information needed to enable
         * versatile logging methods and capabilities with the uploader and debug 
         * information provided by the uploader
         * 
         * Created 8/20/2016 by AGuyWhoIsBored
         * 
         */
        public static string debugText = "";
        private static Stopwatch compileTimeMS = new Stopwatch();

        ///<summary>
        ///<para>this will push info to our debugger window / array</para>
        ///will also push to main output log if needed
        /// </summary>
        public static void logDebug(string info, bool showInMain = false)
        { UpdateDebugger("[Uploader]: " + info); if (showInMain) { Console.WriteLine("[Info] " + "[Uploader]: " + info); } }
        private static void UpdateDebugger(string text)
        {
            if (Program.debug) { Console.WriteLine("[" + compileTimeMS.ElapsedMilliseconds.ToString() + "]: " + text); }
            debugText += Environment.NewLine + "[" + compileTimeMS.ElapsedMilliseconds.ToString() + "]: " + text;
        }
        public static void startStopwatch() { compileTimeMS.Start(); }
        public static void resetStopwatch() { compileTimeMS.Reset(); }
        public static long getStopwatchTimeInMS() { return compileTimeMS.ElapsedMilliseconds; }
    }
}
